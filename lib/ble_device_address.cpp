#include "ble_device_address.h"
#include <iostream>
#include <sstream>
#include <iomanip>
#include <algorithm>

namespace Neuro::Ble
{
    static auto split_address_string(const std::string &address_string)
    {
        std::array<std::string, BleDeviceAddress::AddressSize> splitAddressString;

        size_t findStartPosition = 0;
        size_t nextDelimiterPosition = 0;
        for (auto &octetString : splitAddressString)
        {
            if (nextDelimiterPosition == std::string::npos)
            {
                throw std::invalid_argument
                    (
                        "The address string is in an incorrect format: invalid delimiter or wrong address size"
                    );
            }
            nextDelimiterPosition = address_string.find(':', findStartPosition);
            auto count =
                nextDelimiterPosition != std::string::npos ?
                    nextDelimiterPosition - findStartPosition
                    : splitAddressString.size() - findStartPosition;
            octetString = address_string.substr(findStartPosition, count);
            findStartPosition = nextDelimiterPosition + 1;
        }

        return splitAddressString;
    }

    static auto octet_string_to_byte(const std::string &octet_string)
    {
        if (octet_string.length() != 2)
        {
            throw std::invalid_argument("Wrong octet string length: " + std::to_string(octet_string.length()));
        }

        const auto octetValue = std::stoul(octet_string, nullptr, 16);
        if (octetValue > std::numeric_limits<unsigned char>::max())
        {
            throw std::overflow_error("Wrong octet value being parsed: " + std::to_string(octetValue));
        }

        return std::byte(octetValue);
    }

    static auto parse_address_string(const std::string &address_string)
    {
        try
        {
            const auto addressOctetStrings = split_address_string(address_string);
            std::array<std::byte, BleDeviceAddress::AddressSize> addressArray{};
            std::transform(addressOctetStrings.begin(),
                           addressOctetStrings.end(),
                           addressArray.begin(),
                           [](const auto &octet_string)
                           { return octet_string_to_byte(octet_string); });
            return addressArray;
        }
        catch (std::exception &e)
        {
            throw std::invalid_argument("Unable to parse address string: " + std::string(e.what()));
        }
        catch (...)
        {
            throw std::invalid_argument("Unable to parse address string");
        }
    }

    BleDeviceAddress::BleDeviceAddress(const std::string &address_string)
        :
        mValue(parse_address_string(address_string))
    {}

    bool operator==(const BleDeviceAddress &lhs, const BleDeviceAddress &rhs)
    {
        return lhs[0] == rhs[0]
            && lhs[1] == rhs[1]
            && lhs[2] == rhs[2]
            && lhs[3] == rhs[3]
            && lhs[4] == rhs[4]
            && lhs[5] == rhs[5];
    }

    std::ostream &operator<<(std::ostream &out_stream, const BleDeviceAddress &address)
    {
        std::ios_base::fmtflags previousFormat(out_stream.flags());
        
        out_stream << std::hex << std::uppercase;
        const auto last_element_index = address.size() - 1;

        for (size_t i = 0; i < last_element_index; i++) {
            out_stream << std::setfill('0') << std::setw(2) << static_cast<unsigned short>(address[i]) << ":";
        }
        out_stream << std::setfill('0') << std::setw(2)
                    << static_cast<unsigned short>(address[last_element_index]);
        out_stream.flags(previousFormat);
        return out_stream;
    }

    std::string to_string(const BleDeviceAddress &address)
    {
        std::stringstream resultStream;
        resultStream << address;
        return resultStream.str();
    }

    BleDeviceAddress BleDeviceAddress::fromLSByteFirstUint64(uint64_t uint_address)
    {
		std::array<value_type, AddressSize> addressArray
		{
			static_cast<value_type>((uint_address & 0x0000000000FF0000) >> 40),
			static_cast<value_type>((uint_address & 0x00000000FF000000) >> 32),
			static_cast<value_type>((uint_address & 0x000000FF00000000) >> 24),
			static_cast<value_type>((uint_address & 0x0000FF0000000000) >> 16),
			static_cast<value_type>((uint_address & 0x00FF000000000000) >> 8),
			static_cast<value_type>((uint_address & 0xFF00000000000000))
		};
		return BleDeviceAddress{ addressArray };
    }
	
    BleDeviceAddress BleDeviceAddress::fromMSByteFirstUint64(uint64_t uint_address)
    {
		std::array<value_type, AddressSize> addressArray
		{
			static_cast<value_type>((uint_address & 0x0000FF0000000000) >> 40),
			static_cast<value_type>((uint_address & 0x000000FF00000000) >> 32),
			static_cast<value_type>((uint_address & 0x00000000FF000000) >> 24),
			static_cast<value_type>((uint_address & 0x0000000000FF0000) >> 16),
			static_cast<value_type>((uint_address & 0x000000000000FF00) >> 8),
			static_cast<value_type>((uint_address & 0x00000000000000FF))
		};
		return BleDeviceAddress{ addressArray };
    }
	
    BleDeviceAddress BleDeviceAddress::fromUint64(uint64_t uint_address)
    {
		return fromMSByteFirstUint64(uint_address);
    }

	uint64_t to_LSByteFirst_uint64(const BleDeviceAddress &address)
    {
        return static_cast<uint64_t>(address[0])
            | static_cast<uint64_t>(address[1]) << 8u
            | static_cast<uint64_t>(address[2]) << 16u
            | static_cast<uint64_t>(address[3]) << 24u
            | static_cast<uint64_t>(address[4]) << 32u
            | static_cast<uint64_t>(address[5]) << 40u;
    }

	uint64_t to_MSByteFirst_uint64(const BleDeviceAddress &address)
    {
        return static_cast<uint64_t>(address[5])
            | static_cast<uint64_t>(address[4]) << 8u
            | static_cast<uint64_t>(address[3]) << 16u
            | static_cast<uint64_t>(address[2]) << 24u
            | static_cast<uint64_t>(address[1]) << 32u
            | static_cast<uint64_t>(address[0]) << 40u;
    }

	uint64_t to_uint64(const BleDeviceAddress &address)
    {
        return to_MSByteFirst_uint64(address);
    }
}

size_t std::hash<Neuro::Ble::BleDeviceAddress>::operator()(const Neuro::Ble::BleDeviceAddress &address) const noexcept
{
    const auto longAddress = to_uint64(address);
    using std::hash;
    return hash<uint64_t>()(longAddress);
}
