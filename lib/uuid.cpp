#include "uuid.h"
#include <algorithm>
#include <sstream>

#include <iostream>
#include <iomanip>


namespace Neuro::Ble
{
	static std::array<std::byte, 16> uuid_array_from_string(const std::string& uuid_string)
	{
		std::array<std::byte, 16> uuidArray{};
		auto valuesSet = sscanf
		(
			uuid_string.c_str(),
			"%2hhx%2hhx%2hhx%2hhx-%2hhx%2hhx-%2hhx%2hhx-%2hhx%2hhx-%2hhx%2hhx%2hhx%2hhx%2hhx%2hhx",
			&uuidArray[0], &uuidArray[1], &uuidArray[2], &uuidArray[3],
			&uuidArray[4], &uuidArray[5],
			&uuidArray[6], &uuidArray[7],
			&uuidArray[8], &uuidArray[9],
			&uuidArray[10], &uuidArray[11], &uuidArray[12], &uuidArray[13], &uuidArray[14], &uuidArray[15]
		);

		if (valuesSet != 16)
		{
			throw std::invalid_argument("The UUID string is in an incorrect format");
		}

		return uuidArray;
	}

	UUID::UUID(const std::string& uuid_string)
		: UUID(uuid_array_from_string(uuid_string))
	{}

	template<size_t SectionSize>
	std::ostream& output_section(std::ostream& out_stream, const std::array<std::byte, SectionSize>& section)
	{
		std::for_each(section.begin(), section.end(), [&out_stream](const auto& value)
			{
				const auto hex = static_cast<unsigned short>(value);
				out_stream << std::setfill('0') << std::setw(2) << hex;
			});
		return out_stream;
	}

	std::ostream& operator<<(std::ostream& out_stream, const UUID& uuid)
	{
		std::ios_base::fmtflags previousFormat(out_stream.flags());
		out_stream << std::hex << std::uppercase;
		output_section(out_stream, uuid.low()) << "-";
		output_section(out_stream, uuid.mid()) << "-";
		output_section(out_stream, uuid.high()) << "-";
		output_section(out_stream, uuid.clock()) << "-";
		output_section(out_stream, uuid.node());
		out_stream.flags(previousFormat);
		return out_stream;
	}

	std::string to_string(const UUID& uuid)
	{
		std::stringstream resultStream;
		resultStream << uuid;
		return resultStream.str();
	}

	template<size_t SectionSize>
	static bool
		sections_equal(const std::array<std::byte, SectionSize>& lhs, const std::array<std::byte, SectionSize>& rhs)
	{
		return std::equal(lhs.begin(), lhs.end(), rhs.begin(), rhs.end());
	}

	bool operator==(const UUID& lhs, const UUID& rhs)
	{
		return sections_equal(lhs.low(), rhs.low())
			&& sections_equal(lhs.mid(), rhs.mid())
			&& sections_equal(lhs.high(), rhs.high())
			&& sections_equal(lhs.clock(), rhs.clock())
			&& sections_equal(lhs.node(), rhs.node());
	}
}

size_t std::hash<Neuro::Ble::UUID>::operator()(const Neuro::Ble::UUID& uuid) const noexcept
{
	const auto longUuidFirst = 
		static_cast<uint64_t>(uuid.low()[0])		+ static_cast<uint64_t>(uuid.low()[1])  *   2	+ static_cast<uint64_t>(uuid.low()[2]) * 4 + static_cast<uint64_t>(uuid.low()[3]) * 8
	  + static_cast<uint64_t>(uuid.mid()[0])  * 16	+ static_cast<uint64_t>(uuid.mid()[1])  *  32
	  + static_cast<uint64_t>(uuid.high()[0]) * 64	+ static_cast<uint64_t>(uuid.high()[1]) * 128;
	const uint64_t longUuidSecond = 
		static_cast<uint64_t>(uuid.clock()[0])		+ static_cast<uint64_t>(uuid.clock()[1]) *   2
	  + static_cast<uint64_t>(uuid.node()[0]) *  4	+ static_cast<uint64_t>(uuid.node()[1])  *   8	+ static_cast<uint64_t>(uuid.node()[2]) *  16
	  + static_cast<uint64_t>(uuid.node()[3]) * 32	+ static_cast<uint64_t>(uuid.node()[4])  *  64	+ static_cast<uint64_t>(uuid.node()[5]) * 128;
	using std::hash;
	return hash<uint64_t>()(longUuidFirst ^ longUuidSecond);
}

