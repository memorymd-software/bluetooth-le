#include <CoreBluetooth/CoreBluetooth.h>
#include "ble/mac/macos_ble_enumerator.h"
#include "ble/ios/ble_delegate.h"
#include "ble/advertisement_data.h"
#include "event_notifier.h"
#include "string_utils.h"
#include "logger.h"
#include <unordered_set>

namespace Neuro {
struct MacOsBleEnumerator::Impl final {
	static constexpr const char *class_name = "MacOSBleEnumerator";
	std::unordered_set<std::string> mFilters;
	Notifier<void, const AdvertisementData &> mAdvertiseNotifier;
	std::mutex mStopScanMutex;
	std::condition_variable mStopScanCondition;
    ScannerDelegate* mScanResultDelegate;
	CBCentralManager* mCentralManager;
    

	explicit Impl(const std::vector<std::string> &name_filters) :
		mFilters(name_filters.begin(), name_filters.end()),
        mScanResultDelegate([[ScannerDelegate alloc] initWithDeviceFoundCallback:^(CBPeripheral *peripheral, NSUUID *rssi, NSData *manufacturerData){
            onAdvertisementReceived(peripheral, rssi, manufacturerData);
        }]),
		mCentralManager([[CBCentralManager alloc] initWithDelegate:mScanResultDelegate queue:dispatch_queue_create("central_queue", 0)]) {
	}

	Impl(const Impl &) = delete;
	Impl& operator=(const Impl &) = delete;
	Impl(Impl &&) = delete;
	Impl& operator=(Impl &&) = delete;

	~Impl() {
        if ([mCentralManager isScanning]){
            [mCentralManager stopScan];
        }
	}

private:
	void onAdvertisementReceived(CBPeripheral *peripheral, NSUUID *rssi, NSData *manufacturerData) {
        if([peripheral name].length == 0) return;
        auto name = std::string([[peripheral name] UTF8String]);
        if (mFilters.find(name) == mFilters.end())
			return;

		AdvertisementData advertisementData;
		advertisementData.Name = name;
		advertisementData.Address = [[[peripheral identifier] UUIDString] cStringUsingEncoding: NSUTF8StringEncoding];
        
        advertisementData.RSSI = rssi.UUIDString.hash;
		advertisementData.ServicesUUIDs = std::vector<std::string>();
        advertisementData.TimeStamp = std::chrono::system_clock::now();
        advertisementData.SerialNumber = getDeviceSerial(manufacturerData);
        
		mAdvertiseNotifier.notifyAll(advertisementData);
	}
 
    uint64_t getDeviceSerial(NSData *manufacturerData){
        if([manufacturerData length] != 3)
            return 0;
        
        auto first = static_cast<const Byte *>([manufacturerData bytes]);
        auto last = first + [manufacturerData length];
        std::vector<Byte> manufacturerDataSections(first, last);
        auto serial = manufacturerDataSections[0] | manufacturerDataSections[1] << 8 | manufacturerDataSections[2] << 16;
        return serial < 0xFFFFFFul ? serial : 0;
    }
};

MacOsBleEnumerator::MacOsBleEnumerator(const std::vector<std::string> &name_filters) :
	mImpl(std::make_unique<Impl>(name_filters)){}

MacOsBleEnumerator::MacOsBleEnumerator(MacOsBleEnumerator&&) noexcept = default;

MacOsBleEnumerator& MacOsBleEnumerator::operator=(MacOsBleEnumerator&&) noexcept = default;

void MacOsBleEnumerator::swap(MacOsBleEnumerator &other) noexcept {
	using std::swap;
	swap(mImpl, other.mImpl);
}

MacOsBleEnumerator::~MacOsBleEnumerator() = default;

ListenerPtr<void, const AdvertisementData&> 
MacOsBleEnumerator::subscribeAdvertisementReceived(const std::function<void(const AdvertisementData&)>& callback){
	return mImpl->mAdvertiseNotifier.addListener(callback);
}

MacOsBleEnumerator make_ble_enumerator(const std::vector<std::string> &uuid_filters) {
	return MacOsBleEnumerator(uuid_filters);
}

void swap(MacOsBleEnumerator& lhs, MacOsBleEnumerator& rhs) noexcept {
	lhs.swap(rhs);
}


}
