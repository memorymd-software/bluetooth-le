/*
 * Copyright 2016 - 2017 Neurotech MRC. http://neuromd.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ble_delegate_h
#define ble_delegate_h

#include <CoreBluetooth/CoreBluetooth.h>
#include "ble/ble_device_info.h"


@interface ScannerDelegate:NSObject <CBCentralManagerDelegate>

-(id) initWithDeviceFoundCallback:(void (^)(CBPeripheral*, NSUUID*, NSData *))deviceFoundCallback;

@end

@interface ConnectionDelegate : NSObject <CBCentralManagerDelegate>

-(id) initWithConnectionCallbacks:(void(^)(CBPeripheral *))connectedCallback disconnectedCallback:(void(^)(CBPeripheral *))disconnectedCallback errorCallback:(void(^)(CBPeripheral *))errorCallback;

@end

@interface DeviceDelegate:NSObject <CBPeripheralDelegate>

-(id)initWithCallbacks:(void(^)())servicesDiscoveredCallback
    characteristicsDiscoveredCallback:(void(^)(CBService *))characteristicsDiscoveredCallback
    characteristicChangedCallback:(void(^)(CBCharacteristic *))characteristicChangedCallback;

@end

#endif /* ble_delegate_h */
