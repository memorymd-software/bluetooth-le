//
//  Central.m
//  bluetoothle
//
//  Created by admin on 24/10/2019.
//
#include "CentralManager.h"
#include "CentralImpl.h"


namespace Neuro::Ble {

    Central::Central(std::unique_ptr<Impl> &&impl_) : impl(std::move(impl_)) {
        
    }
    Central::~Central() = default;
    bool Central::isScanning() const noexcept {
        return impl->raw().isScanning == YES;
    }
    bool Central::isAvailable() const noexcept {
        return impl->raw().state == CBManagerStatePoweredOn;
    }

    void Central::connectPeripheral( PeripheralPtr peripheral, const std::map<std::string, int>& options) {
        if ( isAvailable() ) {
            [impl->raw() connectPeripheral:peripheral->impl->raw() options:nil];
        } else {
            throw std::runtime_error("connect failed");
        }
    }

    void Central::disconnectPeripheral( PeripheralPtr peripheral ) {
        if ( isAvailable() && peripheral->isConnected() ) {
            [impl->raw() cancelPeripheralConnection:peripheral->impl->raw()];
        } else {
            throw std::runtime_error("disconnect failed");
        }
    }
    void Central::scanForPeripheralsWithServices(const std::vector<Neuro::Ble::UUID> &identifiers, const std::map<std::string, int>& options) {
        if ( isAvailable() ) {
            [impl->raw() scanForPeripheralsWithServices:nil options:nil];
        } else {
            throw std::runtime_error("cbcentral unavalible");
        }
    }

    void Central::stopScan() {
        if ( isAvailable() ) {
            [impl->raw() stopScan];
        } else {
            throw std::runtime_error("cbcentral unavalible");
        }
    }

    std::vector<PeripheralPtr> Central::retrievePeripheralsWithIdentifiers(const std::vector<Neuro::Ble::UUID> &identifiers) {
        NSMutableArray *objIdentifierArray = [NSMutableArray arrayWithCapacity:identifiers.size()];
        for ( auto identifier : identifiers) {
            NSUUID *uuidIdentifier = [[NSUUID alloc] initWithUUIDString:@(to_string(identifier).data())];
            [objIdentifierArray addObject:  uuidIdentifier];
        }
        if ( isAvailable() ) {
            auto retrieveArray = [impl->raw() retrievePeripheralsWithIdentifiers: objIdentifierArray];
            std::vector<PeripheralPtr> result;
            if ( retrieveArray.count > 0) {
                for (CBPeripheral * peripheral : retrieveArray) {
                        result.push_back(
                                std::make_shared<Peripheral>(
                                                             std::make_unique<Peripheral::Impl>(peripheral)));
                }
                return result;
            }
        }
        return {};
    }
}
