//
//  CentralManager.h
//  bluetoothle
//
//  Created by admin on 18/09/2019.
//

#ifndef CentralManager_h
#define CentralManager_h

#include "advertisement_data.h"
#include "event_notifier.h"


#include <string>
#include <functional>
#include <map>
#include <vector>
#include <mutex>


namespace Neuro::Ble {   
    namespace Debug { class Error; }
    
    class nocopymove {
        nocopymove(const nocopymove&) = delete;
        nocopymove(nocopymove&&) = delete;
        nocopymove &operator=(const nocopymove&) = delete;
        nocopymove &operator=(nocopymove&&) = delete;
    public:
        nocopymove() = default;
        ~nocopymove() = default;
    };
    
    
    class Characteristic;
    class Service;
    class Peripheral;
    class Central;
    class CentralManager;
    
    using CentralPtr           = std::shared_ptr<Central>;
    using PeripheralPtr        = std::shared_ptr<Peripheral>;
    using ServicePtr           = std::shared_ptr<Service>;
    using CharacteristicPtr    = std::shared_ptr<Characteristic>;
    using CentralManagerPtr    = std::shared_ptr<CentralManager>;
    
    using ServiceWeakPtr       = std::weak_ptr<Service>;
    using PeripheralWeakPtr    = std::weak_ptr<Peripheral>;
    using RSSIInteger          = int;
    
    
    class Characteristic {
    public:
        enum class Properties : unsigned char
        {
            None   = 0b00000000,
            Read   = 0b00000001,
            Write  = 0b00000010,
            Notify = 0b00000100,
        };
        class Impl;
        std::unique_ptr<Characteristic::Impl> impl;
        
        Characteristic() = delete;
        Characteristic(std::unique_ptr<Characteristic::Impl> &&);
        ~Characteristic();
        
        [[nodiscard]] UUID uuid() const noexcept;
        [[nodiscard]] ServicePtr service() const noexcept;
        [[nodiscard]] std::vector<std::byte> data() const noexcept;
        
        
        void write(const std::vector<std::byte>& data);
        Utilities::EventNotifier<const std::vector<std::byte>&>& valueChanged();
        
        Properties properties();
        void setupServiceOwner(ServicePtr) noexcept;
    };
    
    
    class Service {
    public:
        class Impl;
        std::unique_ptr<Service::Impl> impl;
        
        Service() = delete;
        Service(std::unique_ptr<Service::Impl> &&);
        
        [[nodiscard]] bool isPrimary() const noexcept;
        [[nodiscard]] PeripheralPtr peripheral() const noexcept;
        
        CharacteristicPtr characteristicByUuid(const UUID& uuid) noexcept;
        std::vector<CharacteristicPtr> characteristics() noexcept;
        
        [[nodiscard]] UUID uuid() const noexcept;
        
        void setupPeripheralOwner(PeripheralPtr) noexcept;
        void updateCharacteristics() noexcept;
    };
    
    class Peripheral {
    public:
        friend class CentralManager;
        class Impl;
        Peripheral() = delete;
        Peripheral(std::unique_ptr<Impl> &&);
        ~Peripheral();
        
        
        
        [[nodiscard]] UUID uuid() const noexcept;
        
        [[nodiscard]] std::string name() const noexcept;
        [[nodiscard]] bool isConnected() const noexcept;
        
        
        void discoverServices(const std::vector<UUID>& = {});
        void discoverCharacteristicsFor(ServicePtr service, const std::vector<UUID> &characteristicUUID = {});

        ServicePtr serviceByUuid(const UUID& uuid) noexcept;
        std::vector<ServicePtr> services() noexcept;
        
        void updateServices();
        
        std::unique_ptr<Impl> impl;
    };
    
    class Central: private nocopymove {
    public:
        class Impl;
        Central() = delete;
        Central(std::unique_ptr<Impl> &&);
        ~Central();
        bool isScanning() const noexcept;
        
        // Available if bluetooth is in CBManagerPowerOn
        bool isAvailable() const noexcept;
        
        [[nodiscard]] std::vector<PeripheralPtr> retrievePeripheralsWithIdentifiers(const std::vector<UUID> &identifiers);
        [[nodiscard]] std::vector<PeripheralPtr> retrieveConnectedPeripheralsWithServices(const std::vector<UUID> &serviceUUIDs);
        
        void scanForPeripheralsWithServices (const std::vector<UUID> &identifiers = {}, const std::map<std::string, int>& options = {});
        void stopScan();
        
        void connectPeripheral( PeripheralPtr peripheral, const std::map<std::string, int>& options = {});
        void disconnectPeripheral( PeripheralPtr peripheral );
        
        void cancelPeripheralConnection( PeripheralPtr peripheral);
    private:
        std::unique_ptr<Impl> impl;
    };

    class CentralManager: private nocopymove {
    private:
        class Impl;
        std::unique_ptr<Impl>                   impl;
        CentralPtr                          mCentral;
        std::mutex                      mSyncCentral;
        
    public:
        CentralManager();
        ~CentralManager() noexcept;
        
        void make() noexcept;
        
        void startScan() noexcept;
        void stopScan()  noexcept;
        
        Utilities::EventNotifier<> &DidUpdateState();
        Utilities::EventNotifier<const AdvertisementData&, const RSSIInteger> &DidDiscoverPeripheral();
        
        Utilities::EventNotifier<const UUID&> &DidConnectPeripheral();
        Utilities::EventNotifier<const UUID&> &DidDisconnectPeripheral();
        
        
        Utilities::EventNotifier<const std::vector<UUID>&, const Debug::Error &> &DidDiscoverServices(PeripheralPtr);
        
        Utilities::EventNotifier<const std::vector<UUID>&, const Debug::Error &> &DidDiscoverCharacteristicsForService(PeripheralPtr);
        
        
        
        
        Utilities::EventNotifier<const UUID&, const std::vector<std::byte>&, const Debug::Error &> &DidUpdateValueForCharacteristic(PeripheralPtr);
        
        [[nodiscard]] CentralPtr central() noexcept;
    };
     
    namespace Debug {
        class Error {
            int mCode;
            std::string mMessage;
        public:
            Error() = default;
            Error(const int code, const std::string& message) : mCode{code}, mMessage{message} {}
            
            const int &code() const noexcept            {return mCode; }
            const std::string &message() const noexcept {return mMessage; }
        };
    }
}


#endif /* CentralManager_h */
