//
//  CentralImpl.h
//  bluetoothle
//
//  Created by admin on 24/10/2019.
//

#ifndef CentralImpl_h
#define CentralImpl_h

#include <CoreBluetooth/CoreBluetooth.h>

#include "CentralManager.h"

@interface CentralDelegate:NSObject <CBCentralManagerDelegate>

@property std::shared_ptr<Neuro::Utilities::EventNotifier<>>                     mUpdateStateNotifier;

@property std::shared_ptr<Neuro::Utilities::EventNotifier<
  const Neuro::Ble::AdvertisementData&, const Neuro::Ble::RSSIInteger >>        mDidDiscoverPeripheral;

@property std::shared_ptr<Neuro::Utilities::EventNotifier<const Neuro::Ble::UUID&>>                     mDidConnectPeripheral;
@property std::shared_ptr<Neuro::Utilities::EventNotifier<const Neuro::Ble::UUID&>>                     mDidDisconnectPeripheral;
@end

@interface AppleBleDeviceDelegate: NSObject<CBPeripheralDelegate>

@property std::shared_ptr<Neuro::Utilities::EventNotifier<const std::vector<Neuro::Ble::UUID>&, const Neuro::Ble::Debug::Error &>>                     mDidDiscoverServices;
@property std::shared_ptr<Neuro::Utilities::EventNotifier<const std::vector<Neuro::Ble::UUID>&, const Neuro::Ble::Debug::Error &>>                     mDidDiscoverCharacteristicsForService;
@property std::shared_ptr<Neuro::Utilities::EventNotifier<const Neuro::Ble::UUID&, const std::vector<std::byte>&, const Neuro::Ble::Debug::Error &>>                     mDidUpdateValueForCharacteristic;
@end

namespace Neuro::Ble {
    namespace helper {
        std::array<std::byte, 16> nsuuid_to_array(NSUUID *uuid);
        std::array<std::byte, 16> cbuuid_to_array(CBUUID *uuid);
        NSUUID *to_nsuuid(const std::string& uuidStr);
    }
    
    class Characteristic::Impl {
        CBCharacteristic    *mCharacteristic;
        ServiceWeakPtr              mService;
        Utilities::EventNotifier<const std::vector<std::byte>&> mValueChanged;
    public:
        Impl() = delete;
        Impl(CBCharacteristic *characteristic);
        
        CBCharacteristic              *raw() noexcept;
        ServiceWeakPtr             service() const noexcept;
        
        void setupServiceOwner(ServicePtr service) noexcept;
        void setupNotify() noexcept;
        Utilities::EventNotifier<const std::vector<std::byte>&>& valueChanged();
    };
    
    class Service::Impl {
        CBService                                              *mService;
        PeripheralWeakPtr                                    mPeripheral;
        std::unordered_map<UUID, CharacteristicPtr>  mCharacteristicsMap;
        void addCharacteristic(CBCharacteristic *characteristic);

    public:
        Impl() = delete;
        Impl(CBService *service);
        
        bool                   isPrimary() const noexcept;
        CBService                   *raw() const noexcept;
        PeripheralWeakPtr peripheralWeak() const noexcept;
        
        [[nodiscard]] CharacteristicPtr              characteristicByUuid( const UUID &uuid) const noexcept;
        [[nodiscard]] std::vector<CharacteristicPtr> characteristics() const noexcept;
        
        void setupPeripheralOwner(PeripheralPtr p);
        void updateCharacteristics() noexcept;
    };
    
    class Peripheral::Impl {
        friend class Peripheral;
        friend class Central;
        AppleBleDeviceDelegate                  *mDelegate;
        CBPeripheral                          *mPeripheral;
        std::unordered_map<UUID, ServicePtr>  mServicesMap;
    public:
        Impl() = delete;
        Impl(CBPeripheral *raw_);
        ~Impl();
        
        void addService(CBService* service);
        void addDidDiscoverServicesCallback(
                                    const std::function <void (UUID peripheral, const Debug::Error&)> &callback);
        void addDidDiscoverCharacteristicsCallback(
                                    const std::function <void (UUID service,       const Debug::Error&)> &callback);
        void addDidUpdateValueForCharacteristic(
                                    const std::function <void (UUID characteristic,
                                                               const Debug::Error&)> &callback);
        
        [[nodiscard]] ServicePtr              serviceByUuid( const UUID &uuid) const noexcept;
        [[nodiscard]] std::vector<ServicePtr> services() const noexcept;
        
        void setupDelegate()  noexcept;
        void updateServices() noexcept;
        
        CBPeripheral *raw() noexcept;
        AppleBleDeviceDelegate *delegate() noexcept;
    };
    
    class Central::Impl: private nocopymove {
        friend class Central;
        CBCentralManager                       *mCentral;
    public:
        Impl() = delete;
        Impl(CBCentralManager *central);
        ~Impl();
        
        CBCentralManager *raw() noexcept;
    };
    class CentralManager::Impl: private nocopymove {
    private:
        friend class CentralManager;
        CentralDelegate                 *mCentralDelegate;
        dispatch_queue_t                mCentralQueue;
        CBCentralManager                 *mCentralManager;
    public:
        Impl();
        ~Impl() = default;
    };
}

#endif /* CentralImpl_h */
