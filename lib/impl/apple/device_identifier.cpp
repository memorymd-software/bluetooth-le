#include "device_identifier.h"

namespace Neuro::Ble
{

	bool operator==(const DeviceIdentifier& lhs, const DeviceIdentifier& rhs)
	{
		return lhs.Uuid == rhs.Uuid;
	}

	std::ostream& operator<<(std::ostream& out_stream, const DeviceIdentifier& identifier)
	{
		return out_stream << identifier.Uuid;
	}

	std::string to_string(const DeviceIdentifier &identifier)
	{
		return to_string(identifier.Uuid);
	}
}

size_t std::hash<Neuro::Ble::DeviceIdentifier>::operator()(const Neuro::Ble::DeviceIdentifier& identifier) const noexcept
{
	using std::hash;
	return hash<Neuro::Ble::UUID>()(identifier.Uuid);
}