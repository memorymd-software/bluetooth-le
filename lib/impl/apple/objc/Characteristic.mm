//
//  Characteristic.m
//  bluetoothle
//
//  Created by admin on 29/10/2019.
//

#include "CentralManager.h"
#include "CentralImpl.h"



namespace Neuro::Ble {
    
    Characteristic::Impl::Impl(CBCharacteristic *characteristic) : mCharacteristic(characteristic) {}
    
    CBCharacteristic *Characteristic::Impl::raw() noexcept { return mCharacteristic; }
    
    void Characteristic::Impl::setupServiceOwner(ServicePtr service) noexcept {
        mService = service;
        setupNotify();
    }
    
    void Characteristic::Impl::setupNotify() noexcept {
        if (auto service = mService.lock(); service) {
            auto peripheral = service->peripheral();
            if (peripheral->isConnected()) {
                [peripheral->impl->raw() setNotifyValue:YES forCharacteristic: mCharacteristic];
                [peripheral->impl->raw() readValueForCharacteristic: mCharacteristic];
            } else {
                throw std::runtime_error("characteristic setup notify error");
            }
        }
        
    }
    
    ServiceWeakPtr Characteristic::Impl::service() const noexcept {
        return mService;
    }
    
    
    Utilities::EventNotifier<const std::vector<std::byte>&>& Characteristic::Impl::valueChanged() {
        return mValueChanged;
    }
    
    
    Characteristic::Characteristic(std::unique_ptr<Characteristic::Impl> &&impl_) : impl{std::move(impl_)}{}
    Characteristic::~Characteristic() = default;
    
    
    void Characteristic::setupServiceOwner(ServicePtr service) noexcept {
        impl->setupServiceOwner(service);
    }
    [[nodiscard]] UUID Characteristic::uuid() const noexcept {
        return UUID(helper::cbuuid_to_array( impl->raw().UUID));
    }


    ServicePtr Characteristic::service() const noexcept {
        auto  weak = impl->service();
        if (!weak.expired())
            return weak.lock();
        return ServicePtr();
    }
    
    std::vector<std::byte> Characteristic::data() const noexcept {
        auto characteristic = impl->raw();
        auto first = static_cast<const std::byte *>((characteristic.value).bytes);
        auto last = first + (characteristic.value).length;
        std::vector<std::byte> data(first, last);
        return data;
    }

    void Characteristic::write(const std::vector<std::byte>& data) {
        auto weak = impl->service();
        if (!weak.expired()) {
            auto service = weak.lock();
            auto perepheril = service->peripheral();
            
            if ( !perepheril->isConnected()) {
                throw std::runtime_error("write characteristic error");
            }

            NSData *nsData = [NSData dataWithBytes:data.data() length:data.size()];
            
            [perepheril->impl->raw() writeValue:nsData forCharacteristic: impl->raw() type:CBCharacteristicWriteWithResponse];
        }
    }
    
    
    
    Utilities::EventNotifier<const std::vector<std::byte>&>& Characteristic::valueChanged() {
        return impl->valueChanged();
    }
    

    Characteristic::Properties Characteristic::properties() {
        /*
         CBCharacteristicPropertyBroadcast                                                = 0x01,
         CBCharacteristicPropertyRead                                                    = 0x02,
         CBCharacteristicPropertyWriteWithoutResponse                                    = 0x04,
         CBCharacteristicPropertyWrite                                                    = 0x08,
         CBCharacteristicPropertyNotify                                                    = 0x10,
         CBCharacteristicPropertyIndicate                                                = 0x20,
         CBCharacteristicPropertyAuthenticatedSignedWrites                                = 0x40,
         CBCharacteristicPropertyExtendedProperties                                        = 0x80,
         CBCharacteristicPropertyNotifyEncryptionRequired NS_ENUM_AVAILABLE(10_9, 6_0)    = 0x100,
         */
        auto properties = impl->raw().properties;
        size_t result{};
        if( (properties & CBCharacteristicPropertyRead) ) {
            result |= static_cast<size_t>(Characteristic::Properties::Read);
        }
        if( (properties & CBCharacteristicPropertyWrite) ) {
            result |= static_cast<size_t>(Characteristic::Properties::Write);
        }
        if( (properties & CBCharacteristicPropertyNotify) ) {
            result |= static_cast<size_t>(Characteristic::Properties::Notify);
        }
        
        return static_cast<Characteristic::Properties>(result);
    }
    
}
