//
//  CentralManage+Delegate.m
//  bluetoothle
//
//  Created by admin on 18/09/2019.
//

#include "CentralManager.h"
#include "CentralImpl.h"

#include <map>
#include <string>
#include <functional>

#include <CoreBluetooth/CoreBluetooth.h>


namespace Neuro::Ble {
    
    namespace helper {
        std::array<std::byte, 16> nsuuid_to_array(NSUUID *uuid) {
            uuid_t uuid_value;
            [uuid getUUIDBytes:uuid_value];
            const auto uuidData = [NSData dataWithBytes:uuid_value length:16];
            std::array<std::byte, 16> dataRaw;
            [uuidData getBytes:dataRaw.data() length:16];
            return dataRaw;
        }
        std::array<std::byte, 16> cbuuid_to_array(CBUUID *uuid) {
            
            const auto uuidData = uuid.data;
            // fix it later
            if (uuidData.length != 16) {

                std::array<std::byte, 2> firstBytes;
                [uuidData getBytes:firstBytes.data() length:2];
                std::array<std::byte, 16> dataRaw;
                //0000180A-0000-1000-8000-00805F9B34FB
                size_t index{0};
                dataRaw[index++] = static_cast<std::byte>(0x0);
                dataRaw[index++] = static_cast<std::byte>(0x0);
                dataRaw[index++] = static_cast<std::byte>(firstBytes[0]);
                dataRaw[index++] = static_cast<std::byte>(firstBytes[1]);
                
                dataRaw[index++] = static_cast<std::byte>(0x0);
                dataRaw[index++] = static_cast<std::byte>(0x0);
                
                dataRaw[index++] = static_cast<std::byte>(0x10);
                dataRaw[index++] = static_cast<std::byte>(0x0);
                
                dataRaw[index++] = static_cast<std::byte>(0x80);
                dataRaw[index++] = static_cast<std::byte>(0x0);
                
                
                dataRaw[index++] = static_cast<std::byte>(0x0);
                dataRaw[index++] = static_cast<std::byte>(0x80);
                dataRaw[index++] = static_cast<std::byte>(0x5F);
                dataRaw[index++] = static_cast<std::byte>(0x9B);
                dataRaw[index++] = static_cast<std::byte>(0x34);
                dataRaw[index++] = static_cast<std::byte>(0xFB);
                
                return dataRaw;
            }
            
            
            std::array<std::byte, 16> dataRaw;
            [uuidData getBytes:dataRaw.data() length:16];
            return dataRaw;
        }
        
        NSUUID *to_nsuuid(const std::string& uuidStr) {
            return [[NSUUID alloc] initWithUUIDString:@(uuidStr.data())];
        }

        struct AdvertisementStatus {
            bool servicesUUIDsStatus{false};
            
            const bool ready() const noexcept{
                return servicesUUIDsStatus;
            }
        };
    }
    


    
    
    Central::Impl::Impl(CBCentralManager *central) : mCentral{central} {}
    Central::Impl::~Impl() {
        if( (mCentral.state == CBManagerStatePoweredOn) && (mCentral.isScanning == YES)) {
            [mCentral stopScan];
        }
    }
    CBCentralManager *Central::Impl::raw() noexcept { return mCentral; }
    
    
    CentralManager::Impl::Impl() :
        mCentralDelegate{[[CentralDelegate alloc] init]},
        mCentralQueue(dispatch_queue_create("BluetoothCentralQueue", nullptr))
    {
        
    }
    
    CentralManager::CentralManager() : impl{std::make_unique<CentralManager::Impl>()} {
        
    }
    
    CentralManager::~CentralManager() noexcept {
    }
    
    void CentralManager::make() noexcept {
        impl->mCentralManager = [[CBCentralManager alloc] initWithDelegate: impl->mCentralDelegate queue:impl->mCentralQueue];

        mCentral = std::make_shared<Neuro::Ble::Central>(
                        std::make_unique<Neuro::Ble::Central::Impl>(impl->mCentralManager));
        
    }
    
    CentralPtr CentralManager::central() noexcept {
        return mCentral;
    }
    
    
    void CentralManager::startScan() noexcept {
        std::lock_guard<std::mutex> lc(mSyncCentral);
        std::weak_ptr<Neuro::Ble::Central> weakCentral = central();
        if ( auto centralManagerVal = weakCentral.lock()) {
            if (!centralManagerVal->isScanning()) {
                centralManagerVal->scanForPeripheralsWithServices();
            }
        }
    }
    
    void CentralManager::stopScan() noexcept {
        std::lock_guard<std::mutex> lc(mSyncCentral);
        std::weak_ptr<Neuro::Ble::Central> weakCentral = central();
        if ( auto centralManagerVal = weakCentral.lock()) {
            if (centralManagerVal->isScanning()) {
                centralManagerVal->stopScan();
            }
        }
    }

    Utilities::EventNotifier<> &CentralManager::DidUpdateState() {
        return *impl->mCentralDelegate.mUpdateStateNotifier;
    }
    Utilities::EventNotifier<const AdvertisementData&, const RSSIInteger > &CentralManager::DidDiscoverPeripheral() {
        return *impl->mCentralDelegate.mDidDiscoverPeripheral;
    }
    

    Utilities::EventNotifier<const UUID&> &CentralManager::DidConnectPeripheral() {
        return *impl->mCentralDelegate.mDidConnectPeripheral;
    }
    
    Utilities::EventNotifier<const UUID&> &CentralManager::DidDisconnectPeripheral() {
        return *impl->mCentralDelegate.mDidDisconnectPeripheral;
    }
    
    Utilities::EventNotifier<const std::vector<Neuro::Ble::UUID>&, const Debug::Error &> &CentralManager::DidDiscoverServices(PeripheralPtr peripheral) {
        return *peripheral->impl->delegate().mDidDiscoverServices;
    }
    Utilities::EventNotifier<const std::vector<UUID>&, const Debug::Error &> &CentralManager::DidDiscoverCharacteristicsForService(PeripheralPtr peripheral) {
        return *peripheral->impl->delegate().mDidDiscoverCharacteristicsForService;
    }
    
    Utilities::EventNotifier<const UUID&, const std::vector<std::byte>&, const Debug::Error &> &CentralManager::DidUpdateValueForCharacteristic(PeripheralPtr peripheral) {
        return *peripheral->impl->delegate().mDidUpdateValueForCharacteristic;
    }
}

@implementation CentralDelegate {
    std::unordered_map<Neuro::Ble::UUID, Neuro::Ble::AdvertisementData> advertisementDataMap;
    std::unordered_map<Neuro::Ble::UUID, Neuro::Ble::helper::AdvertisementStatus> advertisementDataStatusMap;
    std::unordered_map<Neuro::Ble::UUID, CBPeripheral*> advertisementPeripheralMap;
    
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.mDidDiscoverPeripheral    = std::make_shared<Neuro::Utilities::EventNotifier<const Neuro::Ble::AdvertisementData&, const Neuro::Ble::RSSIInteger>>();
        self.mUpdateStateNotifier      = std::make_shared<Neuro::Utilities::EventNotifier<>>();
        self.mDidConnectPeripheral     = std::make_shared<Neuro::Utilities::EventNotifier<const Neuro::Ble::UUID&>>();
        self.mDidDisconnectPeripheral  = std::make_shared<Neuro::Utilities::EventNotifier<const Neuro::Ble::UUID&>>();

    }
    return self;
}

- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    if ( central.state == CBManagerStateUnknown) {
        return;
    }
    self.mUpdateStateNotifier->notify();
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary<NSString *, id> *)advertisementData RSSI:(NSNumber *)RSSI
{
    using namespace Neuro::Ble;

    NSUUID *uuid = peripheral.identifier;
    UUID ble_uuid = UUID{helper::nsuuid_to_array(uuid)};
    
    AdvertisementData advertisement{};
    helper::AdvertisementStatus statusAdvertisement{};
    
    if ( auto statusExist = advertisementDataStatusMap.find(ble_uuid); statusExist != std::end(advertisementDataStatusMap)) {
        statusAdvertisement = statusExist->second;
    }
    
    if ( auto existAdverisment = advertisementDataMap.find(ble_uuid); existAdverisment != std::end(advertisementDataMap)) {
        advertisement = existAdverisment->second;
    }
    
    if( NSString *name = peripheral.name) {
        advertisement.Name = name.UTF8String;
        advertisement.Identifier = DeviceIdentifier{ble_uuid};
        advertisement.RSSI = RSSI.intValue;
        
        if (NSData *manufacturerData = advertisementData[CBAdvertisementDataManufacturerDataKey]; manufacturerData != nullptr) {
            if(manufacturerData.length == 0)
                return;
            NSData * data = advertisementData[ CBAdvertisementDataManufacturerDataKey ];
            auto first = static_cast<const std::byte *>(data.bytes) + 2;
            auto last = first + 3;//[manufacturerData length];
            std::vector<std::byte> manufacturerDataSections(first, last);
            advertisement.ManufacturerData.insert_or_assign(0xFFFF, manufacturerDataSections);
        }
    }
    
    NSArray<CBUUID*> *services = advertisementData[CBAdvertisementDataServiceUUIDsKey];
    if( services.count != 0 ) {
        std::vector<UUID> services_{};
        for ( CBUUID *s in services) {
            auto uuid = UUID{helper::cbuuid_to_array(s)};
            services_.push_back(uuid);
        }
        advertisement.ServicesUUIDs = std::move(services_);
        statusAdvertisement.servicesUUIDsStatus = true;
    }
    
    
    // time of last update
    advertisement.TimeStamp = std::chrono::steady_clock::now();
    
    advertisementDataMap.insert_or_assign(ble_uuid, advertisement);
    advertisementDataStatusMap.insert_or_assign(ble_uuid, statusAdvertisement);
    advertisementPeripheralMap.insert_or_assign(ble_uuid, peripheral);
    if( statusAdvertisement.ready() ) {
        self.mDidDiscoverPeripheral->notify(advertisement, RSSI.intValue);
    }
}
- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral {
    
    using namespace Neuro::Ble;
    auto uuid = Neuro::Ble::UUID{helper::nsuuid_to_array(peripheral.identifier)};
    self.mDidConnectPeripheral->notify(uuid);
}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    using namespace Neuro::Ble;
    auto uuid = Neuro::Ble::UUID{Neuro::Ble::helper::nsuuid_to_array(peripheral.identifier)};
    self.mDidDisconnectPeripheral->notify(uuid);
}
- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
}
@end

@implementation AppleBleDeviceDelegate

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.mDidDiscoverServices                  = std::make_shared<Neuro::Utilities::EventNotifier<const std::vector<Neuro::Ble::UUID>&, const Neuro::Ble::Debug::Error &>>();
        self.mDidDiscoverCharacteristicsForService = std::make_shared<Neuro::Utilities::EventNotifier<const std::vector<Neuro::Ble::UUID>&, const Neuro::Ble::Debug::Error &>>();
        self.mDidUpdateValueForCharacteristic      = std::make_shared<Neuro::Utilities::EventNotifier<const Neuro::Ble::UUID&, const std::vector<std::byte>&, const Neuro::Ble::Debug::Error &>>();
    }
    return self;
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(nullable NSError *)error {
    using namespace Neuro::Ble;
    std::vector<UUID> uuids;
    // macOS: issue
    // if service list is empty
    // try request any service
    if( peripheral.services.count == 0) {
        [peripheral discoverServices:nil];
        return;
    }

    for ( CBService *service in peripheral.services) {
        const auto uuid = UUID(Neuro::Ble::helper::cbuuid_to_array(service.UUID));
        uuids.emplace_back(uuid);
    }
    
    
    self.mDidDiscoverServices->notify(uuids, Neuro::Ble::Debug::Error{});
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(nullable NSError *)error
{
        using namespace Neuro::Ble;
        std::vector<UUID> uuids;
        // macOS: issue
        // if characteristic list is empty
        // try request any characteristics
        if( service.characteristics.count == 0) {
            [peripheral discoverCharacteristics:nil forService:service];
            return;
        }
    
        for ( CBCharacteristic *characteristic in service.characteristics) {
            const auto uuid = UUID(Neuro::Ble::helper::cbuuid_to_array(characteristic.UUID));
            uuids.push_back(uuid);
        }
        self.mDidDiscoverCharacteristicsForService->notify(uuids, Neuro::Ble::Debug::Error{});
}
- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(nullable NSError *)error
{
    using namespace Neuro::Ble;
    const auto uuid = UUID(Neuro::Ble::helper::cbuuid_to_array(characteristic.UUID));
    
    auto first = static_cast<const std::byte *>((characteristic.value).bytes);
    auto last = first + (characteristic.value).length;
    std::vector<std::byte> data(first, last);
    
    self.mDidUpdateValueForCharacteristic->notify(uuid, data, Neuro::Ble::Debug::Error{});
}
/*
 - (void)peripheralDidUpdateName:(CBPeripheral *)peripheral NS_AVAILABLE(10_9, 6_0);
 - (void)peripheral:(CBPeripheral *)peripheral didModifyServices:(NSArray<CBService *> *)invalidatedServices
 - (void)peripheralDidUpdateRSSI:(CBPeripheral *)peripheral error:(nullable NSError *)error
 - (void)peripheral:(CBPeripheral *)peripheral didReadRSSI:(NSNumber *)RSSI error:(nullable NSError *)error
 - (void)peripheral:(CBPeripheral *)peripheral didDiscoverIncludedServicesForService:(CBService *)service error:(nullable NSError *)error;
 - (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(nullable NSError *)error;
 - (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(nullable NSError *)error;
 - (void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(nullable NSError *)error;
 - (void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(nullable NSError *)error;
 - (void)peripheral:(CBPeripheral *)peripheral didDiscoverDescriptorsForCharacteristic:(CBCharacteristic *)characteristic error:(nullable NSError *)error;
 - (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForDescriptor:(CBDescriptor *)descriptor error:(nullable NSError *)error;
 - (void)peripheral:(CBPeripheral *)peripheral didWriteValueForDescriptor:(CBDescriptor *)descriptor error:(nullable NSError *)error;
 - (void)peripheralIsReadyToSendWriteWithoutResponse:(CBPeripheral *)peripheral;
 - (void)peripheral:(CBPeripheral *)peripheral didOpenL2CAPChannel:(nullable CBL2CAPChannel *)channel error:(nullable NSError *)error;
 */
@end
