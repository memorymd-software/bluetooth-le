//
//  Service.m
//  bluetoothle
//
//  Created by admin on 24/10/2019.
//


#include "CentralManager.h"
#include "CentralImpl.h"

namespace Neuro::Ble {
    
    Service::Impl::Impl(CBService *service)  :
        mService(service)
//        mPeripheral{std::make_shared<Peripheral>(std::make_unique<Peripheral::Impl>(service.peripheral))}
    {
    }  
    bool Service::Impl::isPrimary() const noexcept {
        return mService.isPrimary == YES;
    }
    
    void Service::Impl::setupPeripheralOwner(PeripheralPtr p) {
        mPeripheral = p;
    }

    void Service::Impl::updateCharacteristics() noexcept {
        for ( CBCharacteristic *characteristic in mService.characteristics ) {
            addCharacteristic(characteristic);
        }
    }

    void Service::Impl::addCharacteristic(CBCharacteristic *characteristic) {
        const auto uuid = UUID(helper::cbuuid_to_array(characteristic.UUID));
        
        auto characteristicImpl = std::make_shared<Characteristic>(std::make_unique<Characteristic::Impl>(characteristic));
        mCharacteristicsMap.insert_or_assign(uuid, characteristicImpl);
    }
    
    CBService *Service::Impl::raw() const noexcept { return mService; }
    
    PeripheralWeakPtr Service::Impl::peripheralWeak() const noexcept {
        return mPeripheral;
    }
    
    [[nodiscard]] CharacteristicPtr Service::Impl::characteristicByUuid( const UUID &uuid) const noexcept {
        if ( auto search = mCharacteristicsMap.find(uuid); search != std::end(mCharacteristicsMap) ) {
            return search->second;
        }
        return CharacteristicPtr();
    }
    [[nodiscard]] std::vector<CharacteristicPtr> Service::Impl::characteristics() const noexcept {
        std::vector<CharacteristicPtr> services;
        for(auto [key,service] : mCharacteristicsMap) {
            services.push_back(service);
        }
        
        return services;
    }
    
    
    Service::Service(std::unique_ptr<Service::Impl> &&impl) : impl{std::move(impl)} {
        
    }
    
    bool Service::isPrimary() const noexcept {
        return impl->isPrimary();
    }
    
    PeripheralPtr Service::peripheral() const noexcept {
        auto  weak = impl->peripheralWeak();
        if (!weak.expired())
            return weak.lock();
        return PeripheralPtr();
    }
    
    CharacteristicPtr Service::characteristicByUuid(const UUID& uuid) noexcept {
        return impl->characteristicByUuid(uuid);
    }
    std::vector<CharacteristicPtr> Service::characteristics() noexcept {
        return impl->characteristics();
    }
    
    [[nodiscard]] UUID Service::uuid() const noexcept {
        return UUID(helper::cbuuid_to_array(impl->raw().UUID));
    }
    
    void Service::setupPeripheralOwner(PeripheralPtr p) noexcept {
        impl->setupPeripheralOwner(p);
    }
    
    void Service::updateCharacteristics() noexcept {
        impl->updateCharacteristics();
    }
}
