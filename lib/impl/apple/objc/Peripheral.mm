//
//  Peripheral.m
//  bluetoothle
//
//  Created by admin on 24/10/2019.
//

#include "CentralManager.h"
#include "CentralImpl.h"


namespace Neuro::Ble {
    
    
    Peripheral::Impl::Impl(CBPeripheral *raw_) :
        mDelegate{ [[AppleBleDeviceDelegate alloc] init]},
        mPeripheral{raw_}
    {
        setupDelegate();
    }
    Peripheral::Impl::~Impl() = default;

    AppleBleDeviceDelegate *Peripheral::Impl::delegate() noexcept {
        return mDelegate;
    }

    ServicePtr Peripheral::Impl::serviceByUuid( const UUID &uuid) const noexcept {
        if ( auto search = mServicesMap.find(uuid); search != std::end(mServicesMap) ) {
            return search->second;
        }
        return ServicePtr();
    }
    
    std::vector<ServicePtr> Peripheral::Impl::services() const noexcept {
        std::vector<ServicePtr> services;
        for(auto [key,service] : mServicesMap) {
            services.push_back(service);
        }
        return services;
    }
    
    void Peripheral::Impl::addService(CBService* service) {
        const auto uuid = UUID(helper::cbuuid_to_array(service.UUID));
        
        auto serviceImpl = std::make_shared<Service>(std::make_unique<Service::Impl>(service));
        mServicesMap.insert_or_assign(uuid, serviceImpl);
    }

    void Peripheral::Impl::setupDelegate() noexcept {
        mPeripheral.delegate = mDelegate;
    }
    
    void Peripheral::Impl::updateServices() noexcept {
        for ( CBService *service in mPeripheral.services ) {
            addService(service);
        }
    }
    
    CBPeripheral *Peripheral::Impl::raw() noexcept { return mPeripheral; }
    
    
    Peripheral::Peripheral(std::unique_ptr<Impl> &&impl_) {
        impl = std::move(impl_);
        impl->setupDelegate();
    }
    
    Peripheral::~Peripheral() = default;
    
    UUID Peripheral::uuid() const noexcept {
        NSUUID *uuid = impl->raw().identifier;
        UUID ble_uuid = UUID{helper::nsuuid_to_array(uuid)};
        return ble_uuid;
    }

    std::string Peripheral::name() const noexcept {
        return impl->raw().name.UTF8String;
    }
    
    [[nodiscard]] bool Peripheral::isConnected() const noexcept {
        return impl->raw().state == CBPeripheralStateConnected;
    }
    
    ServicePtr Peripheral::serviceByUuid(const UUID& uuid) noexcept {
        return impl->serviceByUuid(uuid);
    }
    std::vector<ServicePtr> Peripheral::services() noexcept {
        return impl->services();
    }
    
    
    
    void Peripheral::discoverServices(const std::vector<UUID>& serviceUIIDs) {
        impl->setupDelegate();
        if ( isConnected()) {
            if (serviceUIIDs.size() != 0) {
                NSMutableArray *array = [NSMutableArray arrayWithCapacity:serviceUIIDs.size()];
                for (auto uuid : serviceUIIDs) {
                    CBUUID *cbuuid = [CBUUID UUIDWithString:  @(to_string(uuid).data())];
                    [array addObject:cbuuid];
                }
                [impl->raw() discoverServices:array];
            } else {
                [impl->raw() discoverServices:nil];
            }
        } else {
            throw std::runtime_error("discover services failed");
        }
    }
    void Peripheral::discoverCharacteristicsFor(ServicePtr service, const std::vector<UUID> &characteristicUUIDs) {
        impl->setupDelegate();
        if ( isConnected()) {
            if (characteristicUUIDs.size() != 0) {
                NSMutableArray *array = [NSMutableArray arrayWithCapacity:characteristicUUIDs.size()];
                for (auto uuid : characteristicUUIDs) {
                    CBUUID *cbuuid = [CBUUID UUIDWithString:  @(to_string(uuid).data())];
                    [array addObject:cbuuid];
                }
                [impl->raw() discoverCharacteristics:array forService: service->impl->raw()];
            } else {
                [impl->raw() discoverCharacteristics:nil forService: service->impl->raw()];
            }
        } else {
            throw std::runtime_error("discover characteristics failed");
        }
    }

    void Peripheral::updateServices() {
        impl->updateServices();
    }
}


