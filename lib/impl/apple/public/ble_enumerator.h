#ifndef BLE_ENUMERATOR_H
#define BLE_ENUMERATOR_H

#include "ble_enumerator_interface.h"
#include "scan_filter.h"
#include <vector>

namespace Neuro::Ble
{
	std::unique_ptr<IBleEnumerator> create_ble_enumerator();	
	std::unique_ptr<IBleEnumerator> create_ble_enumerator(const std::vector<ScanFilter>& scan_filters);
}

#endif // BLE_ENUMERATOR_H