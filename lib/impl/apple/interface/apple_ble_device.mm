#include "apple_ble_device.h"
#include "apple_ble_service.h"

#include "CentralImpl.h"


#include <mutex>
#include <future>
#include <sstream>

namespace Neuro::Ble
{
    class AppleBleDevice::Impl {
        using Services = std::vector<std::shared_ptr<IBleService>>;
        
        using ServicePromise = std::promise<Services>;
        
        friend class                                                 AppleBleDevice;
        DeviceIdentifier                                                mIdentifier;
        CentralManagerPtr                                     mSharedCentralManager;
        AdvertisementData                                         mAdvertismentData;
        std::string                                                           mName;
        PeripheralPtr                                                   mPeripheral;
        std::unordered_map<UUID, std::shared_ptr<IBleService>>            mServices;
        
        std::promise<std::vector<std::shared_ptr<IBleService>>>     mServicePromise;
        
        Utilities::EventNotifier<ConnectionState>   mConnectionStateChangedNotifier;
        ConnectionState                                            mConnectionState;
        
        dispatch_semaphore_t connection = dispatch_semaphore_create(0);

        Utilities::EventListener<const UUID&>                    mDidConnectPeripheralListener{[&](const UUID&){
            mSharedCentralManager->stopScan();
            
            mConnectionState = ConnectionState::InRage;
            mConnectionStateChangedNotifier.notify(mConnectionState);
            updateServices();
            dispatch_semaphore_signal(connection);
        }};
        Utilities::EventListener<const UUID&>                    mDidDisconnectPeripheralListener{[&](const UUID&){
            sendDisconnected();
        }};
        
        Utilities::EventListener<const std::vector<Neuro::Ble::UUID>&, const Debug::Error &>     mDidDiscoverServicesListener{[this] (const std::vector<Neuro::Ble::UUID>& uuids, const Debug::Error &){
            mPeripheral->updateServices();
            for (auto uuid : uuids) {
                auto service = mPeripheral->serviceByUuid(uuid);
                service->setupPeripheralOwner(mPeripheral);
                if (auto existService = mServices.find(uuid); existService != std::end(mServices)) {
                    auto rawBleService = (AppleBleService*)(existService->second.get());
                    rawBleService->resetService(service);
                }
            }
        }};
        Utilities::EventListener<>  mDidUpdateState {[this](){
            if ( mConnectionState == ConnectionState::InRage && !mPeripheral->isConnected() ) {
                mConnectionState = ConnectionState::OutOfRange;
                mConnectionStateChangedNotifier.notify(mConnectionState);
            }
        }};
        
        Utilities::EventListener<const Neuro::Ble::AdvertisementData&, const Neuro::Ble::RSSIInteger >  mDidDiscoverPeripheral {[this](const Neuro::Ble::AdvertisementData& adverisement, const Neuro::Ble::RSSIInteger ){
            if ( adverisement.Identifier == mIdentifier) {
                connectDevice();
            }
        }};

        void initDevice() {
            createDevice();
            // only peripheral callback
            registrateCallbacks();
            if(mSharedCentralManager->central()->isAvailable()) {
                connectDevice();
            } else {
                throw std::runtime_error("CBCentralManager is not ready :(");
            }
        }
        
        void createDevice() {
            auto central = mSharedCentralManager->central();
            if ( central->isAvailable()) {
                auto peripherals = central->retrievePeripheralsWithIdentifiers({mIdentifier.Uuid});
                if (!peripherals.empty()) {
                    mPeripheral = peripherals[0];
                    mName = mAdvertismentData.Name;
                    return;
                } else {
                    throw std::runtime_error("retrieve peripherals number is zero");
                }
            }
        }
        
        void registrateCallbacks() {
        }
        void sendDisconnected() {
            mConnectionState = ConnectionState::OutOfRange;
            mConnectionStateChangedNotifier.notify(mConnectionState);
        }
        void connectDevice() {
            auto central = mSharedCentralManager->central();
            if(central->isAvailable() && !mPeripheral->isConnected()) {
                central->connectPeripheral(mPeripheral);
                auto status =
                    dispatch_semaphore_wait(connection, dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC));
                if ( status != 0 ) {
                    throw std::runtime_error("connecting failure after 5 seconds");
                }
            }
        }
        
        void updateServices() {
        }
        
    public:
        Impl() = delete;
        Impl(const DeviceIdentifier &identifier_, CentralManagerPtr centralManager, const AdvertisementData &advertismant) : mIdentifier{identifier_},
            mSharedCentralManager{centralManager},
            mAdvertismentData{advertismant}{
                
                mSharedCentralManager->DidUpdateState()
                    .registerListener(mDidUpdateState);
                
                mSharedCentralManager->DidConnectPeripheral()
                    .registerListener(mDidConnectPeripheralListener);
                
                mSharedCentralManager->DidDisconnectPeripheral()
                    .registerListener(mDidDisconnectPeripheralListener);
                
                mSharedCentralManager->DidDiscoverPeripheral().registerListener(mDidDiscoverPeripheral);
                
                
                initDevice();
        }
        
        std::future<std::vector<std::shared_ptr<IBleService>>> getServicesByUuid(const std::vector<UUID>& uuids_) {
            ServicePromise promise;
            Utilities::EventListener<const std::vector<Neuro::Ble::UUID>&, const Debug::Error &> listener{[&] (const std::vector<Neuro::Ble::UUID>& uuids, const Debug::Error &){
                mPeripheral->updateServices();
                for (auto uuid : uuids_) {
                    auto service = mPeripheral->serviceByUuid(uuid);
                    if (service) {
                        service->setupPeripheralOwner(mPeripheral);
                    } else {
                        break;
                    }
                    if (auto existService = mServices.find(uuid); existService != std::end(mServices)) {
                        auto rawBleService = (AppleBleService*)(existService->second.get());
                        rawBleService->resetService(service);
                    } else {
                        mServices.insert_or_assign(service->uuid(), std::make_unique<AppleBleService>(service, mSharedCentralManager));
                    }
                }
                std::vector<std::shared_ptr<IBleService>> services;

                for (auto key : uuids_) {
                    if(auto data = mServices.find(key); data != std::end(mServices)){
                        services.push_back(data->second);
                    }
                }
                
                promise.set_value(std::move(services));
            }};
            
            mSharedCentralManager->DidDiscoverServices(mPeripheral)
                            .registerListener(listener);
            if(mPeripheral->isConnected()) {
                mPeripheral->discoverServices(uuids_);
            } else {
                sendDisconnected();
            }
            
            auto future = promise.get_future();
            auto status = future.wait_for(std::chrono::seconds(5));
            if (status == std::future_status::timeout) {
                std::stringstream str;
                str << "service uuid(" << to_string(uuids_[0]) << ") not found :(\n";
                throw std::runtime_error(str.str());
            }
            return future;
        }
        
        ~Impl();
    };
   
    AppleBleService::~AppleBleService() = default;
    
    AppleBleDevice::AppleBleDevice(const DeviceIdentifier &identifier_, CentralManagerPtr centralManager, const AdvertisementData &advertisment) :
     impl{std::make_unique<AppleBleDevice::Impl>(identifier_, centralManager, advertisment)}
    {
    }
    AppleBleDevice::Impl::~Impl() {
        try {
            mSharedCentralManager->central()->disconnectPeripheral(mPeripheral);
        } catch (const std::exception& e) {
            NSLog( @"Exception with: %s", e.what());
        }
    }
    AppleBleDevice::~AppleBleDevice() = default;

    
    const DeviceIdentifier& AppleBleDevice::identifier() const noexcept {
        return impl->mIdentifier;
    }
    const std::string& AppleBleDevice::name() const noexcept {
        return impl->mName;
    }
    
    
    Utilities::EventNotifier<ConnectionState>& AppleBleDevice::connectionStateChanged() noexcept {
        return impl->mConnectionStateChangedNotifier;
    }
    
    ConnectionState AppleBleDevice::connectionState() const noexcept {
        return impl->mConnectionState;
    }
    
    std::future<std::vector<std::shared_ptr<IBleService>>> AppleBleDevice::getServicesByUuid(const std::vector<UUID>& services_uuids) const {
        return impl->getServicesByUuid(services_uuids);
    }

}

