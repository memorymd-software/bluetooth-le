//
//  apple_ble_device.h
//  bluetoothle
//
//  Created by admin on 18/09/2019.
//

#ifndef apple_ble_device_h
#define apple_ble_device_h

#include "ble_device.h"

#include "CentralManager.h"


namespace Neuro::Ble {
    
class AppleBleDevice final : public IBleDevice
{
    class Impl;
    std::unique_ptr<Impl> impl;
public:
    AppleBleDevice() = delete;
    AppleBleDevice(const DeviceIdentifier &indentifier_, CentralManagerPtr centralManager, const AdvertisementData&);
    ~AppleBleDevice();
    [[nodiscard]] const DeviceIdentifier& identifier() const noexcept override;
    
    [[nodiscard]] const std::string& name() const noexcept override;
    
    
    [[nodiscard]] virtual ConnectionState connectionState() const noexcept override;
    Utilities::EventNotifier<ConnectionState>& connectionStateChanged() noexcept override;
    std::future<std::vector<std::shared_ptr<IBleService>>> getServicesByUuid(const std::vector<UUID>& services_uuids) const override;
};
    
}
#endif /* apple_ble_device_h */
