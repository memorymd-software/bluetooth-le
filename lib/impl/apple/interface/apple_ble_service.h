//
//  apple_ble_service.h
//  bluetoothle
//
//  Created by admin on 23/09/2019.
//

#ifndef apple_ble_service_h
#define apple_ble_service_h


#include "ble_device.h"
#include "event_notifier.h"

#include "CentralManager.h"

namespace Neuro::Ble {
    class AppleBleService;
    class AppleBleCharacteristic;
    
    using AppleBleCharacteristicPtr = std::unique_ptr<AppleBleCharacteristic>;
    using AppleBleServicePtr = std::unique_ptr<AppleBleService>;
    
    class AppleBleCharacteristic : public IBleCharacteristic {
        CentralManagerPtr                                      mSharedCentralManager;
        CharacteristicPtr                                            mCharacteristic;
        Utilities::EventNotifier<const std::vector<std::byte>&>      mValueChanged;
        
        Utilities::EventListener<const Neuro::Ble::UUID&, const std::vector<std::byte>&, const Neuro::Ble::Debug::Error &>      mUpdateCharacteristic;
        
        
    public:
        AppleBleCharacteristic() = delete;
        AppleBleCharacteristic(CharacteristicPtr, CentralManagerPtr);
        
        CharacteristicProperties properties() const noexcept override;
        std::vector<std::byte> value() const override;
        Utilities::EventNotifier<const std::vector<std::byte>&>& valueChanged() override;
        void write(const std::vector<std::byte>&) override;
        void resetCharacteristic(CharacteristicPtr characteristic);
        ~AppleBleCharacteristic() override;
    };
        
    class AppleBleService : public IBleService {       
        class Impl;
        std::unique_ptr<Impl> impl;
    public:
        AppleBleService() = delete;
        
        AppleBleService(ServicePtr service, CentralManagerPtr);
        
        ~AppleBleService() override;
        
        void resetService(ServicePtr service);
        
        std::future<std::vector<std::shared_ptr<IBleCharacteristic>>> getCharacteristicsByUuid(const std::vector<UUID>& char_uuids) const override;
    };
    
    class AppleBleService::Impl {
        using CharacteristicPromise = std::promise<std::vector<std::shared_ptr<IBleCharacteristic>>>;
        ServicePtr                                                                                             mService;
        std::unordered_map<UUID, std::shared_ptr<IBleCharacteristic>>                                                  mCharacteristicMap;
        CentralManagerPtr                                                                         mSharedCentralManager;
                
        std::vector<Neuro::Ble::UUID> characteristic_uuids;
        Utilities::EventListener<const UUID&> mDidConnectPeripheralListener;
    	Utilities::EventListener
            <const std::vector<Neuro::Ble::UUID>&, const Debug::Error&>     mDidDiscoverCharacteristicsListener;
//        std::promise<std::vector<std::shared_ptr<IBleCharacteristic>>>                          mCharacteristicsPromise;
        void registrateCallbacks() noexcept;
        
    public:
        Impl(ServicePtr service, CentralManagerPtr centralManager);
        ServicePtr    service() const noexcept;
        PeripheralPtr peripheral() const noexcept;
        
        void resetService(ServicePtr service);
        
        std::future<std::vector<std::shared_ptr<IBleCharacteristic>>> getCharacteristicsByUuid(
                                                                                               const std::vector<UUID>& uuids);
        ~Impl() = default;
    };
}

#endif /* apple_ble_service_h */
