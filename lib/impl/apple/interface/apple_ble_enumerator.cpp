#include "advertising_devices_registry.h"
#include "apple_ble_device.h"
#include "ble_enumerator.h"

#include "CentralManager.h"
#include <iostream>


namespace Neuro::Ble
{
    class AppleBleEnumerator final : public IBleEnumerator
    {
    private:
        std::vector<ScanFilter>                                          mFilters;
        Utilities::EventNotifier<const std::vector<AdvertisementData> &> mAdvertisementListEventNotifier;
        std::vector<AdvertisementData> mAdvertisingDevices;
        std::unordered_map<UUID, AdvertisementData> mAdvertisingDevicesMap;
        CentralManagerPtr mSharedCentralManager;
        Impl::AdvertisingDevicesRegistry mRegistry;
        
        Utilities::EventListener<const std::vector<AdvertisementData> &> mAdvertisementListener
        {
            [=](const auto& advertisement_data)
            {
                mRegistry.onAdvertisementReceived(advertisement_data);
            }
        };
        
        Utilities::EventListener<> mUpdateListener { [this](){
            std::lock_guard<std::mutex> lock{create_enumerator_mutex_};
                if (mSharedCentralManager->central()->isAvailable()) {
                    mSharedCentralManager->startScan();
                }
            }
        };
        Utilities::EventListener<const AdvertisementData &, const RSSIInteger> mDiscoverPeripheral { [this](const AdvertisementData &advertisement, const RSSIInteger rssi){
                if ( mFilters.empty()) {
                    std::vector<AdvertisementData> adverties;
                    adverties.push_back(advertisement);
                    mAdvertisingDevicesMap.insert_or_assign(advertisement.Identifier.Uuid, advertisement);
                    mAdvertisementListEventNotifier.notify(adverties);
                    return;
                }
                for(auto value: mFilters) {
                    if ( value.MainServiceUuid.has_value()) {
                        auto uuid = *value.MainServiceUuid;
                        if(auto found = std::find(std::begin(advertisement.ServicesUUIDs), std::end(advertisement.ServicesUUIDs), uuid); found != std::end(advertisement.ServicesUUIDs)) {
                            std::vector<AdvertisementData> adverties;
                            adverties.push_back(advertisement);
                            mAdvertisingDevicesMap.insert_or_assign(advertisement.Identifier.Uuid, advertisement);
                            mAdvertisementListEventNotifier.notify(adverties);
                            break;
                        }
                    }
                }
            }
       };
        
        void registrateCallbacks() {
            mSharedCentralManager->DidUpdateState().registerListener(mUpdateListener);
            mSharedCentralManager->DidDiscoverPeripheral().registerListener(mDiscoverPeripheral);
        }
    private:
        std::mutex create_enumerator_mutex_;
    public:
        
        AppleBleEnumerator() :
        mFilters{},
        mAdvertisementListEventNotifier{},
        mSharedCentralManager{std::make_shared<CentralManager>()}
        {
            std::lock_guard<std::mutex> lock{create_enumerator_mutex_};
            mAdvertisementListEventNotifier.registerListener(mAdvertisementListener);
            registrateCallbacks();
            // make sure it's called after all necessary callbacks are registrated
            mSharedCentralManager->make();

        }
        
        AppleBleEnumerator(const std::vector<ScanFilter>& scan_filters) :
        mFilters{scan_filters},
        mAdvertisementListEventNotifier{},
        mSharedCentralManager{std::make_shared<CentralManager>()}
        {
            std::lock_guard<std::mutex> lock{create_enumerator_mutex_};
            mAdvertisementListEventNotifier.registerListener(mAdvertisementListener);
            registrateCallbacks();
            // make sure it's called after all necessary callbacks are registrated
            mSharedCentralManager->make();


        }
        
        virtual std::vector<AdvertisementData> advertisingDevices() const override
        {
            return mRegistry.advertisingDevices();
        }
        
        virtual Utilities::EventNotifier<> &advertisementListChanged() override
        {
            return mRegistry.advertisementListChanged();
        }
        
        virtual std::unique_ptr<IBleDevice> createBleDevice(const DeviceIdentifier& identifier) const override
        {
            AdvertisementData data{};
            if (mAdvertisingDevicesMap.find(identifier.Uuid) != mAdvertisingDevicesMap.end()) {
                data = mAdvertisingDevicesMap.at(identifier.Uuid);
            } else {
                throw std::runtime_error("identifier not found");
            }
            auto device = std::make_unique<AppleBleDevice>(identifier, mSharedCentralManager, data);
            return std::move(device);
        }
        
        ~AppleBleEnumerator() = default;
        
    };
    
	std::unique_ptr<IBleEnumerator> create_ble_enumerator()
	{
        auto ble_enumerator_inst = std::make_unique<AppleBleEnumerator>();
        return std::move(ble_enumerator_inst);
	}
    
    std::unique_ptr<IBleEnumerator> create_ble_enumerator(const std::vector<ScanFilter>& scan_filters) {
        auto ble_enumerator_inst = std::make_unique<AppleBleEnumerator>(scan_filters);
        return std::move(ble_enumerator_inst);
    }
}
