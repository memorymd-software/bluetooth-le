//
//  apple_ble_service.m
//  bluetoothle
//
//  Created by admin on 23/10/2019.
//

#include "apple_ble_service.h"

#include <CoreBluetooth/CoreBluetooth.h>
#include <sstream>
namespace Neuro::Ble
{
    AppleBleCharacteristic::AppleBleCharacteristic(CharacteristicPtr characteristic, CentralManagerPtr centralManager) :
        mSharedCentralManager{centralManager},
        mCharacteristic{characteristic},
        mUpdateCharacteristic{[this](const Neuro::Ble::UUID& uuid, const std::vector<std::byte> data, const Neuro::Ble::Debug::Error &) {
            if (mCharacteristic->uuid() == uuid) {
                mValueChanged.notify(data);
            }
        }}
    { mSharedCentralManager->DidUpdateValueForCharacteristic(mCharacteristic->service()->peripheral()).registerListener(mUpdateCharacteristic); }

    CharacteristicProperties AppleBleCharacteristic::properties() const noexcept
    { 
        return static_cast<CharacteristicProperties>(mCharacteristic->properties());
    }

    std::vector < std::byte > AppleBleCharacteristic::value() const {
        return mCharacteristic->data();
    }
    Utilities::EventNotifier < const std::vector < std::byte > & > &AppleBleCharacteristic::valueChanged() {
        return mValueChanged;
    }

    void AppleBleCharacteristic::resetCharacteristic(CharacteristicPtr characteristic)
    {
        if (mCharacteristic->service()) {
            return;
        }

        mCharacteristic = characteristic;
        auto peripheral = mCharacteristic->service()->peripheral();
        mSharedCentralManager->DidUpdateValueForCharacteristic(peripheral)
        .registerListener(mUpdateCharacteristic);
    }

    void AppleBleCharacteristic::write(const std::vector < std::byte >& data)
    {
        auto peripheral = mCharacteristic->service()->peripheral();
        if (!peripheral->isConnected()) {
            mSharedCentralManager->DidDisconnectPeripheral().notify(peripheral->uuid());
        }
        mCharacteristic->write(data);
    }

    AppleBleCharacteristic::~AppleBleCharacteristic()
    {
    }

    AppleBleService::Impl::Impl(ServicePtr service, CentralManagerPtr centralManager) :
        mService{service},
        mSharedCentralManager{centralManager},
        mDidConnectPeripheralListener {[] (const UUID &uuid) {} },
        mDidDiscoverCharacteristicsListener{[] (const std::vector < Neuro::Ble::UUID > &uuids, const Debug::Error &) {}
            
        }
    {
        registrateCallbacks();
    }
    void AppleBleService::Impl::registrateCallbacks() noexcept
    {
        mSharedCentralManager->DidConnectPeripheral().registerListener(mDidConnectPeripheralListener);
        mSharedCentralManager->
        DidDiscoverCharacteristicsForService(peripheral())
        .registerListener(mDidDiscoverCharacteristicsListener);
    }

    ServicePtr AppleBleService::Impl::service() const noexcept
    {
        return mService;
    }

    PeripheralPtr AppleBleService::Impl::peripheral() const noexcept
    {
        return mService->peripheral();
    }

    std::future < std::vector < std::shared_ptr < IBleCharacteristic >> > AppleBleService::Impl::getCharacteristicsByUuid(const std::vector < UUID > &uuids_) {
        CharacteristicPromise promise;
        Utilities::EventListener < const std::vector < Neuro::Ble::UUID > &, const Debug::Error & > listener {[&] (const std::vector < Neuro::Ble::UUID > &uuids, const Debug::Error &) {
            
            auto serviceTmp = this->service();
            serviceTmp->updateCharacteristics();
            for (auto uuid : uuids_) {
                auto chracteristic = serviceTmp->characteristicByUuid(uuid);
                if (chracteristic) {
                    chracteristic->setupServiceOwner(serviceTmp);
                } else {
                    break;
                }

                if (auto existCharacteristic = mCharacteristicMap.find(uuid); existCharacteristic != std::end(mCharacteristicMap)) {
                    auto rawBleCharacteristic = (AppleBleCharacteristic *)(existCharacteristic->second.get());
                    rawBleCharacteristic->resetCharacteristic(chracteristic);
                } else {
                  mCharacteristicMap.insert_or_assign(chracteristic->uuid(), std::make_shared < AppleBleCharacteristic > (chracteristic, mSharedCentralManager));
                }
            }
            
            std::vector < std::shared_ptr < IBleCharacteristic >> characteristics;
            for (auto key : uuids_) {
                if (auto data = mCharacteristicMap.find(key); data != std::end(mCharacteristicMap)) {
                    characteristics.push_back(data->second);
                }
            }
            promise.set_value(characteristics);
        } };

        mSharedCentralManager->DidDiscoverCharacteristicsForService(peripheral())
        .registerListener(listener);

        peripheral()->discoverCharacteristicsFor(mService, uuids_);

        auto future = promise.get_future();
        auto status = future.wait_for(std::chrono::seconds(5));
        if ( status == std::future_status::timeout) {
            std::stringstream str;
            str << "charasteristic uuid(" << to_string(uuids_[0]) << ") not found :(\n";
            throw std::runtime_error(str.str());
        }
        return future;
    }

    void AppleBleService::Impl::resetService(ServicePtr service)
    {
        if (mService->peripheral()) {
            return;
        }
        mService = service;
        registrateCallbacks();
    }

    AppleBleService::AppleBleService(ServicePtr service, CentralManagerPtr centralManager) : impl{std::make_unique < Impl > (service, centralManager)}
    {
    }

    void AppleBleService::resetService(ServicePtr service)
    {
        impl->resetService(service);
    }

    std::future < std::vector < std::shared_ptr < IBleCharacteristic >> > AppleBleService::getCharacteristicsByUuid(const std::vector < UUID > &char_uuids) const {
        return impl->getCharacteristicsByUuid(char_uuids);
    }
}
