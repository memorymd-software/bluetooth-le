#ifndef DEVICE_ID_H
#define DEVICE_ID_H

#include "platform_traits.h"
#include "UUID.h"

namespace Neuro::Ble
{
	namespace Impl
	{
		template <typename Platform>
		struct DeviceIdentifier;

		template <>
		struct DeviceIdentifier<Windows> final
		{
			unsigned long Address;
		};

		template <>
		struct DeviceIdentifier<Android> final
		{
			unsigned long Address;
		};

		template <>
		struct DeviceIdentifier<Apple> final
		{
			UUID Uuid;
		};
	}

	using DeviceId = Impl::DeviceIdentifier<CurrentPlatform>;
}
#endif // DEVICE_ID_H
