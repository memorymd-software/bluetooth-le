#ifndef WINRT_BLE_DEVICE_H
#define WINRT_BLE_DEVICE_H

#include "ble_device.h"
#include <winrt/Windows.Foundation.Collections.h>
#include "winrt/Windows.Devices.Bluetooth.GenericAttributeProfile.h"

namespace Neuro::Ble::Impl
{
	class WinRtBleDevice final : public IBleDevice
	{
	public:
		explicit WinRtBleDevice(const BleDeviceAddress &address);

		~WinRtBleDevice();
		
		[[nodiscard]] const DeviceIdentifier& identifier() const noexcept override;
		[[nodiscard]] const std::string& name() const noexcept override;
		[[nodiscard]] ConnectionState connectionState() const noexcept override;
		Utilities::EventNotifier<ConnectionState>& connectionStateChanged() noexcept override;
		std::future<std::vector<std::shared_ptr<IBleService>>> getServicesByUuid(const std::vector<UUID>& services_uuids) const override;

	private:
		Utilities::EventNotifier<ConnectionState> mConnectionStateNotifier;
		winrt::Windows::Devices::Bluetooth::BluetoothLEDevice mDevice;
		const DeviceIdentifier mIdentifier;
		const std::string mName;
		std::shared_ptr<Utilities::EventNotifier<>> mWinRTConnectionStateNotifier;
		Utilities::EventListener<> mWinRTConnectionStateListener;
		winrt::event_token mConnectionStateToken;
	};
}

#endif // WINRT_BLE_DEVICE_H
