#ifndef WINRT_BLE_SERVICE_H
#define WINRT_BLE_SERVICE_H

#include "ble_device.h"
#include <winrt/Windows.Foundation.Collections.h>
#include "winrt/Windows.Devices.Bluetooth.GenericAttributeProfile.h"

namespace Neuro::Ble::Impl
{
	class WinRtBleService final : public IBleService
	{
	public:
		WinRtBleService(const winrt::Windows::Devices::Bluetooth::GenericAttributeProfile::GattDeviceService& service);
		
		std::future<std::vector<std::shared_ptr<IBleCharacteristic>>> getCharacteristicsByUuid(const std::vector<UUID>& char_uuids) const override;

	private:
		winrt::Windows::Devices::Bluetooth::GenericAttributeProfile::GattDeviceService mService;
	};
}

#endif // WINRT_BLE_SERVICE_H
