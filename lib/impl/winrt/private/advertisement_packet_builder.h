#ifndef ADVERTISEMENT_PACKET_BUILDER_H
#define ADVERTISEMENT_PACKET_BUILDER_H

#include "advertising_devices_registry.h"
#include "winrt/Windows.Devices.Bluetooth.Advertisement.h"
#include "scan_filter.h"
#include <mutex>

namespace Neuro::Ble::Impl
{
	class AdvertisementPacketBuilder final
	{
	public:
		explicit AdvertisementPacketBuilder(AdvertisingDevicesRegistry& registry, const std::vector<ScanFilter>& scan_filters) :
			mFilters(scan_filters),
			mRegistry(registry)
		{}

		void onAdvertisementReceived(const winrt::Windows::Devices::Bluetooth::Advertisement::BluetoothLEAdvertisementReceivedEventArgs& args);

	private:
		std::mutex mAdvertisementReceivedMutex;
		std::vector<ScanFilter> mFilters;
		AdvertisingDevicesRegistry& mRegistry;
		std::unordered_map<DeviceIdentifier, AdvertisementData> mPackets;

		bool isValidFilteredDevice(const AdvertisementData &name);
	};
}
#endif // ADVERTISEMENT_PACKET_BUILDER_H
