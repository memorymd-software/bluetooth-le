#ifndef WINRT_BLE_CHARACTERISTIC_H
#define WINRT_BLE_CHARACTERISTIC_H

#include "ble_device.h"
#include <winrt/Windows.Foundation.Collections.h>
#include "winrt/Windows.Devices.Bluetooth.GenericAttributeProfile.h"

namespace Neuro::Ble::Impl
{
	class WinRtBleCharacteristic final : public IBleCharacteristic
	{
	public:
		WinRtBleCharacteristic(const winrt::Windows::Devices::Bluetooth::GenericAttributeProfile::GattCharacteristic &characteristic);

		~WinRtBleCharacteristic();
		
		[[nodiscard]] CharacteristicProperties properties() const noexcept override;
		[[nodiscard]] std::vector<std::byte> value() const override;
		Utilities::EventNotifier<const std::vector<std::byte>&>& valueChanged() override;
		void write(const std::vector<std::byte>&) override;

	private:
		Utilities::EventNotifier<const std::vector<std::byte>&> mValueChangedNotifier;
		winrt::Windows::Devices::Bluetooth::GenericAttributeProfile::GattCharacteristic mCharacteristic;
		std::shared_ptr<Utilities::EventNotifier<
			winrt::Windows::Devices::Bluetooth::GenericAttributeProfile::GattCharacteristic,
			winrt::Windows::Devices::Bluetooth::GenericAttributeProfile::GattValueChangedEventArgs>> mWinRTValueChangedNotifier;
		Utilities::EventListener<
			winrt::Windows::Devices::Bluetooth::GenericAttributeProfile::GattCharacteristic,
			winrt::Windows::Devices::Bluetooth::GenericAttributeProfile::GattValueChangedEventArgs> mWinRTValueChangedListener;
		winrt::event_token mValueChangedToken;

		
	};
}

#endif // WINRT_BLE_CHARACTERISTIC_H
