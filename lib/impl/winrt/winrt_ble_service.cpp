#include "winrt_ble_service.h"
#include "winrt_ble_characteristic.h"

using winrt::Windows::Devices::Bluetooth::GenericAttributeProfile::GattDeviceService;
using winrt::Windows::Foundation::IAsyncOperation;
using winrt::Windows::Devices::Bluetooth::GenericAttributeProfile::GattCharacteristicsResult;
using winrt::Windows::Devices::Bluetooth::GenericAttributeProfile::GattCharacteristic;
using winrt::Windows::Devices::Bluetooth::GenericAttributeProfile::GattCommunicationStatus;

namespace Neuro::Ble::Impl
{
	static winrt::guid to_winrt_guid(const UUID& uuid)
	{
		const uint32_t data1 =
			(static_cast<uint32_t>(uuid.low()[0]) << 24)
			| (static_cast<uint32_t>(uuid.low()[1]) << 16)
			| (static_cast<uint32_t>(uuid.low()[2]) << 8)
			| (static_cast<uint32_t>(uuid.low()[3]));

		const uint16_t data2 =
			static_cast<uint16_t>(uuid.mid()[0]) << 8
			| static_cast<uint16_t>(uuid.mid()[1]);

		const uint16_t data3 =
			static_cast<uint16_t>(uuid.high()[0]) << 8
			| static_cast<uint16_t>(uuid.high()[1]);

		const auto data4 = std::array<uint8_t, 8>
		{
			static_cast<uint8_t>(uuid.clock()[0]),
				static_cast<uint8_t>(uuid.clock()[1]),
				static_cast<uint8_t>(uuid.node()[0]),
				static_cast<uint8_t>(uuid.node()[1]),
				static_cast<uint8_t>(uuid.node()[2]),
				static_cast<uint8_t>(uuid.node()[3]),
				static_cast<uint8_t>(uuid.node()[4]),
				static_cast<uint8_t>(uuid.node()[5]),
		};

		return winrt::guid{ data1, data2, data3, data4 };
	}
	
	static GattCharacteristic get_characteristic(const GattDeviceService& service, const UUID& guid)
	{
		const auto asyncResult = service.GetCharacteristicsForUuidAsync(to_winrt_guid(guid)).get();
		if (asyncResult.Status() != winrt::Windows::Devices::Bluetooth::GenericAttributeProfile::GattCommunicationStatus::Success)
		{
			throw std::runtime_error("Cannot get characteristic with UUID " + to_string(guid));
		}

		const auto characteristics = asyncResult.Characteristics();
		if (characteristics.Size() == 0)
		{
			throw std::runtime_error("Characteristic with UUID " + to_string(guid) + " not found.");
		}

		return characteristics.GetAt(0);
	}
	
	WinRtBleService::WinRtBleService(const GattDeviceService& service) :
		mService(service)
	{}

	std::future<std::vector<std::shared_ptr<IBleCharacteristic>>> WinRtBleService::getCharacteristicsByUuid(const std::vector<UUID>& char_uuids) const
	{
		if (char_uuids.empty())
		{
			std::promise<std::vector<std::shared_ptr<IBleCharacteristic>>> charsPromise;
			auto futureResult = charsPromise.get_future();
			mService
				.GetCharacteristicsAsync()
				.Completed
				([charsResult = std::move(charsPromise)]
				(IAsyncOperation<GattCharacteristicsResult> async_operation, auto) mutable
			{
				const auto asyncResult = async_operation.GetResults();
				if (asyncResult.Status() != GattCommunicationStatus::Success)
				{
					throw std::runtime_error("Cannot get all characteristics of service.");
				}

				try
				{
					const auto services = asyncResult.Characteristics();
					std::vector<std::shared_ptr<IBleCharacteristic>> servicesPointers;
					std::transform(
						begin(services),
						end(services),
						std::back_inserter(servicesPointers), [](GattCharacteristic characteristic)
						{
							return std::make_shared<WinRtBleCharacteristic>(characteristic);
						});
					charsResult.set_value(std::move(servicesPointers));
				}
				catch (...)
				{
					try
					{
						charsResult.set_exception(std::current_exception());
					}
					catch (...) {}
				}
			});
			return futureResult;
		}
		else
		{
			return std::async(std::launch::async,
				[=]()
				{
					std::vector<std::shared_ptr<IBleCharacteristic>> charPointers;
					std::transform(
						char_uuids.begin(),
						char_uuids.end(),
						std::back_inserter(charPointers), [=](const UUID& uuid)
						{
							return std::make_shared<WinRtBleCharacteristic>(get_characteristic(mService, uuid));
						});
					return charPointers;
				});
		}
	}
}


