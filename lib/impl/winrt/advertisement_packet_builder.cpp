#include "advertisement_packet_builder.h"
#include <winrt/Windows.Foundation.Collections.h>
#include "winrt/Windows.Storage.Streams.h"
#include "string_utils.h"

using winrt::Windows::Devices::Bluetooth::Advertisement::BluetoothLEAdvertisementWatcher;
using winrt::Windows::Devices::Bluetooth::Advertisement::BluetoothLEAdvertisementReceivedEventArgs;
using winrt::Windows::Devices::Bluetooth::Advertisement::BluetoothLEAdvertisementType;
using winrt::Windows::Devices::Bluetooth::Advertisement::BluetoothLEManufacturerData;
using winrt::Windows::Devices::Bluetooth::Advertisement::BluetoothLEScanningMode;
using winrt::Windows::Storage::Streams::DataReader;

namespace Neuro::Ble::Impl
{
	static std::string parse_device_name(const BluetoothLEAdvertisementReceivedEventArgs& args)
	{
		return Utilities::to_narrow(args.Advertisement().LocalName().c_str());
	}

	static DeviceIdentifier parse_device_identifier(const BluetoothLEAdvertisementReceivedEventArgs& args)
	{
		return DeviceIdentifier{ BleDeviceAddress::fromUint64(args.BluetoothAddress()) };
	}

	static int parse_rssi(const BluetoothLEAdvertisementReceivedEventArgs& args)
	{
		return args.RawSignalStrengthInDBm();
	}

	static std::vector<UUID> parse_services(const BluetoothLEAdvertisementReceivedEventArgs& args)
	{
		const auto servicesGuids = args.Advertisement().ServiceUuids();
		std::vector<UUID> services;
		std::transform(begin(servicesGuids), end(servicesGuids), std::back_inserter(services), [](winrt::guid guid)
			{
				std::array<std::byte, 16> uuidArray
				{
					static_cast<std::byte>((guid.Data1 & 0xFF000000) >> 24),
					static_cast<std::byte>((guid.Data1 & 0x00FF0000) >> 16),
					static_cast<std::byte>((guid.Data1 & 0x0000FF00) >> 8),
					static_cast<std::byte>((guid.Data1 & 0x000000FF)),
					static_cast<std::byte>((guid.Data2 & 0xFF00) >> 8),
					static_cast<std::byte>((guid.Data2 & 0x00FF)),
					static_cast<std::byte>((guid.Data3 & 0xFF00) >> 8),
					static_cast<std::byte>((guid.Data3 & 0x00FF)),
					static_cast<std::byte>((guid.Data4[0])),
					static_cast<std::byte>((guid.Data4[1])),
					static_cast<std::byte>((guid.Data4[2])),
					static_cast<std::byte>((guid.Data4[3])),
					static_cast<std::byte>((guid.Data4[4])),
					static_cast<std::byte>((guid.Data4[5])),
					static_cast<std::byte>((guid.Data4[6])),
					static_cast<std::byte>((guid.Data4[7])),
				};
				return UUID(uuidArray);
			});
		return services;
	}

	static std::chrono::steady_clock::time_point parse_timestamp(const BluetoothLEAdvertisementReceivedEventArgs& args)
	{
		return std::chrono::steady_clock::now();
	}

	static std::unordered_map<int, std::vector<std::byte>> parse_manufacturer_data(const BluetoothLEAdvertisementReceivedEventArgs& args)
	{
		auto manuData = args.Advertisement().ManufacturerData();
		std::unordered_map<int, std::vector<std::byte>> result;
		std::transform(begin(manuData), end(manuData), std::inserter(result, result.end()), [](const BluetoothLEManufacturerData& data)
			{
				const auto buffer = data.Data();
				const auto dataReader = DataReader::FromBuffer(buffer);
				std::vector<unsigned char> intermediateBuffer(buffer.Length());
				dataReader.ReadBytes(intermediateBuffer);

				std::vector<std::byte> resultBuffer(intermediateBuffer.size());
				std::transform(intermediateBuffer.begin(), intermediateBuffer.end(), resultBuffer.begin(), [](unsigned char value)
					{
						return static_cast<std::byte>(value);
					});
				return std::pair{ static_cast<int>(data.CompanyId()), resultBuffer };
			});

		return result;
	}

	
	void AdvertisementPacketBuilder::onAdvertisementReceived(const BluetoothLEAdvertisementReceivedEventArgs& args)
	{
		std::unique_lock advertisementLock(mAdvertisementReceivedMutex);
		if (args.AdvertisementType() != BluetoothLEAdvertisementType::ScanResponse)
		{
			const AdvertisementData advertisementData
			{
				parse_device_name(args),
				parse_device_identifier(args),
				parse_rssi(args),
				parse_services(args),
				parse_timestamp(args),
				parse_manufacturer_data(args)
			};
			mPackets.insert_or_assign(advertisementData.Identifier, advertisementData);
		}
		else
		{
			const auto identifier = parse_device_identifier(args);
			if (auto recordIter = mPackets.find(identifier); recordIter != mPackets.end())
			{
				recordIter->second.RSSI = parse_rssi(args);
				recordIter->second.ServicesUUIDs = parse_services(args);
				recordIter->second.TimeStamp = parse_timestamp(args);
				if (isValidFilteredDevice(recordIter->second))
				{
					mRegistry.onAdvertisementReceived({ recordIter->second });
				}
				mPackets.erase(recordIter);
			}
		}
	}
		
	bool AdvertisementPacketBuilder::isValidFilteredDevice(const AdvertisementData& advertisement)
	{
		if (mFilters.empty())
			return true;
		
		for (const auto &filter : mFilters)
		{
			auto isValid = true;
			if (filter.DeviceId.has_value()) {
				isValid = isValid && (advertisement.Identifier == filter.DeviceId.value());
			}
			if (filter.DeviceName.has_value()) {
				isValid = isValid && (advertisement.Name == filter.DeviceName.value());
			}
			if (filter.MainServiceUuid.has_value()) {
				isValid = isValid && (std::find(advertisement.ServicesUUIDs.begin(), advertisement.ServicesUUIDs.end(),filter.MainServiceUuid.value())!= advertisement.ServicesUUIDs.end());
			}
			if (isValid)
				return true;
		}

		return false;
	}
}
