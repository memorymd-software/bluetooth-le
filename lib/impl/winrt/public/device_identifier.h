#ifndef DEVICE_IDENTIFIER_H
#define DEVICE_IDENTIFIER_H

#include "ble_device_address.h"

namespace Neuro::Ble
{
	struct DeviceIdentifier final
	{
		BleDeviceAddress Address;
	};

	bool operator==(const DeviceIdentifier& lhs, const DeviceIdentifier& rhs);

	std::ostream& operator<<(std::ostream& out_stream, const DeviceIdentifier& identifier);

	std::string to_string(const DeviceIdentifier&);
}

template<>
struct std::hash<Neuro::Ble::DeviceIdentifier>
{
	size_t operator()(const Neuro::Ble::DeviceIdentifier& identifier) const noexcept;
};

#endif // DEVICE_ID_H
