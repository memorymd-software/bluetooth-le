#include "ble_enumerator.h"
#include "advertising_devices_registry.h"
#include "winrt_ble_device.h"
#include "winrt/Windows.Devices.Bluetooth.Advertisement.h"
#include "advertisement_packet_builder.h"
#include <future>


using winrt::Windows::Devices::Bluetooth::Advertisement::BluetoothLEAdvertisementWatcher;
using winrt::Windows::Devices::Bluetooth::Advertisement::BluetoothLEAdvertisementReceivedEventArgs;
using winrt::Windows::Devices::Bluetooth::Advertisement::BluetoothLEScanningMode;

namespace Neuro::Ble
{
	namespace Impl
	{	
		class WinRtBleEnumerator final : public IBleEnumerator
		{
		public:
			explicit WinRtBleEnumerator(const std::vector<ScanFilter>& scan_filters = std::vector<ScanFilter>{}) :
				mPacketBuilder(mRegistry, scan_filters),
				mAdvertisementNotifier(std::make_shared<Utilities::EventNotifier<BluetoothLEAdvertisementWatcher, BluetoothLEAdvertisementReceivedEventArgs>>()),
				mAdvertisementListener([=](const auto& watcher, const auto& args)
				{
					onAdvertisementReceived(watcher, args);
				}),
				mReceivedEventToken
				(
					mWatcher.Received
					(
					[weakNotifier = std::weak_ptr<Utilities::EventNotifier<BluetoothLEAdvertisementWatcher, BluetoothLEAdvertisementReceivedEventArgs>>(mAdvertisementNotifier)]
					(const auto& watcher, const auto& args)
					{
						auto notifier = weakNotifier.lock();
						if (notifier != nullptr)
						{
							notifier->notifyAsync(watcher, args);
						}				
					})
				)
			{
				mAdvertisementNotifier->registerListener(mAdvertisementListener);
				mWatcher.ScanningMode(BluetoothLEScanningMode::Active);
				mWatcher.Start();
			}

			virtual ~WinRtBleEnumerator()
			{
				try
				{
					mWatcher.Stop();
					mWatcher.Received(mReceivedEventToken);
				}
				catch(...)
				{
					//cannot do anything
				}
			}
			
			std::vector<AdvertisementData> advertisingDevices() const override
			{
				return mRegistry.advertisingDevices();
			}
			
			Utilities::EventNotifier<>& advertisementListChanged() override
			{
				return mRegistry.advertisementListChanged();
			}
			
			std::unique_ptr<IBleDevice> createBleDevice(const DeviceIdentifier& identifier) const override
			{
				if (winrt::impl::is_sta())
				{
					return std::async(std::launch::async, [identifier]()
					{
						winrt::init_apartment();
						return std::make_unique<WinRtBleDevice>(identifier.Address);
					}).get();
				}
				else
				{
					winrt::init_apartment();
					return std::make_unique<WinRtBleDevice>(identifier.Address);
				}
			}

		private:
			AdvertisingDevicesRegistry mRegistry;
			AdvertisementPacketBuilder mPacketBuilder;
			BluetoothLEAdvertisementWatcher mWatcher;
			std::shared_ptr<Utilities::EventNotifier<BluetoothLEAdvertisementWatcher, BluetoothLEAdvertisementReceivedEventArgs>> mAdvertisementNotifier;
			Utilities::EventListener<BluetoothLEAdvertisementWatcher, BluetoothLEAdvertisementReceivedEventArgs> mAdvertisementListener;
			winrt::event_token mReceivedEventToken;

			
			void onAdvertisementReceived(const BluetoothLEAdvertisementWatcher&, const BluetoothLEAdvertisementReceivedEventArgs& args)
			{
				mPacketBuilder.onAdvertisementReceived(args);
			}
		};
	}

	std::unique_ptr<IBleEnumerator> create_ble_enumerator()
	{
		if (winrt::impl::is_sta())
		{
			return std::async(std::launch::async, []()
			{
				winrt::init_apartment();
				return std::make_unique<Impl::WinRtBleEnumerator>();
			}).get();
		}
		else
		{
			winrt::init_apartment();
			return std::make_unique<Impl::WinRtBleEnumerator>();
		}
	}
	
	std::unique_ptr<IBleEnumerator> create_ble_enumerator(const std::vector<ScanFilter>& scan_filters)
	{
		if (winrt::impl::is_sta())
		{
			return std::async(std::launch::async, [scan_filters]()
				{
					winrt::init_apartment();
					return std::make_unique<Impl::WinRtBleEnumerator>(scan_filters);
				}).get();
		}
		else
		{
			winrt::init_apartment();
			return std::make_unique<Impl::WinRtBleEnumerator>(scan_filters);
		}
	}
}
