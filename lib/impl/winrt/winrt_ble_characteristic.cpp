#include "winrt_ble_characteristic.h"
#include "winrt/Windows.Storage.Streams.h"

using winrt::Windows::Devices::Bluetooth::GenericAttributeProfile::GattDeviceService;
using winrt::Windows::Devices::Bluetooth::GenericAttributeProfile::GattCharacteristic;
using winrt::Windows::Devices::Bluetooth::GenericAttributeProfile::GattCommunicationStatus;
using winrt::Windows::Devices::Bluetooth::GenericAttributeProfile::GattCharacteristicProperties;
using winrt::Windows::Devices::Bluetooth::GenericAttributeProfile::GattValueChangedEventArgs;
using winrt::Windows::Devices::Bluetooth::GenericAttributeProfile::GattClientCharacteristicConfigurationDescriptorValue;
using winrt::Windows::Storage::Streams::DataReader;
using winrt::Windows::Storage::Streams::DataWriter;

namespace Neuro::Ble::Impl
{
	WinRtBleCharacteristic::WinRtBleCharacteristic(const GattCharacteristic &charact) :
		mCharacteristic(charact),
		mWinRTValueChangedNotifier(std::make_shared<Utilities::EventNotifier<GattCharacteristic, GattValueChangedEventArgs>>()),
		mWinRTValueChangedListener
		(
			[=](GattCharacteristic characteristic, GattValueChangedEventArgs args)
			{
				const auto value = args.CharacteristicValue();
				const auto dataReader = DataReader::FromBuffer(value);
				std::vector<unsigned char> intermediateBuffer(value.Length());
				dataReader.ReadBytes(intermediateBuffer);

				std::vector<std::byte> resultBuffer(intermediateBuffer.size());
				std::transform(intermediateBuffer.begin(), intermediateBuffer.end(), resultBuffer.begin(), [](unsigned char value)
					{
						return static_cast<std::byte>(value);
					});
				mValueChangedNotifier.notify(resultBuffer);
			}
		)
	{
		if (static_cast<unsigned char>(properties()) & static_cast<unsigned char>(CharacteristicProperties::Notify))
		{
			const auto writeDescriptorResult = mCharacteristic.WriteClientCharacteristicConfigurationDescriptorAsync(GattClientCharacteristicConfigurationDescriptorValue::Notify).get();
			if (writeDescriptorResult != GattCommunicationStatus::Success)
				throw std::runtime_error("Unable set notification callback");

			mValueChangedToken = mCharacteristic.ValueChanged
			(
				[weakNotifier = std::weak_ptr<Utilities::EventNotifier<GattCharacteristic, GattValueChangedEventArgs>>(mWinRTValueChangedNotifier)]
				(const auto& characteristic, const auto& args)
				{
					auto notifier = weakNotifier.lock();
					if (notifier != nullptr)
					{
						notifier->notify(characteristic, args);
					}
				}
			);
			mWinRTValueChangedNotifier->registerListener(mWinRTValueChangedListener);
		}
	}

	WinRtBleCharacteristic::~WinRtBleCharacteristic() = default;

	CharacteristicProperties WinRtBleCharacteristic::properties() const noexcept
	{
		auto winrtProperties = mCharacteristic.CharacteristicProperties();
		auto properties = CharacteristicProperties::None;
		if (static_cast<uint32_t>(winrtProperties) & static_cast<uint32_t>(GattCharacteristicProperties::Read))
		{
			properties = static_cast<CharacteristicProperties>(static_cast<unsigned char>(properties) | static_cast<unsigned char>(CharacteristicProperties::Read));
		}

		if ((static_cast<uint32_t>(winrtProperties) & static_cast<uint32_t>(GattCharacteristicProperties::Write)) |
			(static_cast<uint32_t>(winrtProperties) & static_cast<uint32_t>(GattCharacteristicProperties::WriteWithoutResponse)))
		{
			properties = static_cast<CharacteristicProperties>(static_cast<unsigned char>(properties) | static_cast<unsigned char>(CharacteristicProperties::Write));
		}

		if (static_cast<uint32_t>(winrtProperties) & static_cast<uint32_t>(GattCharacteristicProperties::Notify))
		{
			properties = static_cast<CharacteristicProperties>(static_cast<unsigned char>(properties) | static_cast<unsigned char>(CharacteristicProperties::Notify));
		}
		
		return properties;
	}
	
	std::vector<std::byte> WinRtBleCharacteristic::value() const
	{
		if (winrt::impl::is_sta())
		{
			return std::async(std::launch::async, [=]()
				{
					const auto result = mCharacteristic.ReadValueAsync(winrt::Windows::Devices::Bluetooth::BluetoothCacheMode::Uncached).get();
					const auto dataReader = DataReader::FromBuffer(result.Value());
					std::vector<unsigned char> intermediateBuffer(result.Value().Length());
					dataReader.ReadBytes(intermediateBuffer);

					std::vector<std::byte> resultBuffer(intermediateBuffer.size());
					std::transform(intermediateBuffer.begin(), intermediateBuffer.end(), resultBuffer.begin(), [](unsigned char value)
						{
							return static_cast<std::byte>(value);
						});
					return resultBuffer;
				}).get();
		}
		else
		{
			const auto result = mCharacteristic.ReadValueAsync(winrt::Windows::Devices::Bluetooth::BluetoothCacheMode::Uncached).get();
			const auto dataReader = DataReader::FromBuffer(result.Value());
			std::vector<unsigned char> intermediateBuffer(result.Value().Length());
			dataReader.ReadBytes(intermediateBuffer);

			std::vector<std::byte> resultBuffer(intermediateBuffer.size());
			std::transform(intermediateBuffer.begin(), intermediateBuffer.end(), resultBuffer.begin(), [](unsigned char value)
				{
					return static_cast<std::byte>(value);
				});
			return resultBuffer;
		}		
	}
	
	Utilities::EventNotifier<const std::vector<std::byte>&>& WinRtBleCharacteristic::valueChanged()
	{
		return mValueChangedNotifier;
	}
	
	void WinRtBleCharacteristic::write(const std::vector<std::byte> &data)
	{
		if (winrt::impl::is_sta())
		{
			return std::async(std::launch::async, [=]()
				{
					const DataWriter commandWriter;
					std::vector<unsigned char> buffer(data.size());
					std::transform(data.begin(), data.end(), buffer.begin(), [](std::byte value) {return static_cast<unsigned char>(value); });
					commandWriter.WriteBytes(buffer);
					const auto writeResult = mCharacteristic.WriteValueAsync(commandWriter.DetachBuffer()).get();
					if (writeResult != GattCommunicationStatus::Success) {
						throw std::runtime_error("Failed to write characteristic value");
					}
				}).get();
		}
		else
		{
			const DataWriter commandWriter;
			std::vector<unsigned char> buffer(data.size());
			std::transform(data.begin(), data.end(), buffer.begin(), [](std::byte value) {return static_cast<unsigned char>(value); });
			commandWriter.WriteBytes(buffer);
			const auto writeResult = mCharacteristic.WriteValueAsync(commandWriter.DetachBuffer()).get();
			if (writeResult != GattCommunicationStatus::Success) {
				throw std::runtime_error("Failed to write characteristic value");
			}
		}
	}
}
