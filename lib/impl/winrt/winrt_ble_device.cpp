#include "winrt_ble_device.h"
#include "winrt_ble_service.h"
#include "string_utils.h"

using winrt::Windows::Devices::Bluetooth::BluetoothLEDevice;
using winrt::Windows::Devices::Bluetooth::BluetoothConnectionStatus;
using winrt::Windows::Foundation::IAsyncOperation;
using winrt::Windows::Devices::Bluetooth::GenericAttributeProfile::GattDeviceService;
using winrt::Windows::Devices::Bluetooth::GenericAttributeProfile::GattDeviceServicesResult;
using winrt::Windows::Devices::Bluetooth::GenericAttributeProfile::GattCommunicationStatus;
using namespace std::string_literals;

namespace Neuro::Ble::Impl
{
	static winrt::guid to_winrt_guid(const UUID& uuid)
	{
		const uint32_t data1 =
			(static_cast<uint32_t>(uuid.low()[0]) << 24)
			| (static_cast<uint32_t>(uuid.low()[1]) << 16)
			| (static_cast<uint32_t>(uuid.low()[2]) << 8)
			| (static_cast<uint32_t>(uuid.low()[3]));

		const uint16_t data2 =
			static_cast<uint16_t>(uuid.mid()[0]) << 8
			| static_cast<uint16_t>(uuid.mid()[1]);

		const uint16_t data3 =
			static_cast<uint16_t>(uuid.high()[0]) << 8
			| static_cast<uint16_t>(uuid.high()[1]);

		const auto data4 = std::array<uint8_t, 8>
		{
			static_cast<uint8_t>(uuid.clock()[0]),
				static_cast<uint8_t>(uuid.clock()[1]),
				static_cast<uint8_t>(uuid.node()[0]),
				static_cast<uint8_t>(uuid.node()[1]),
				static_cast<uint8_t>(uuid.node()[2]),
				static_cast<uint8_t>(uuid.node()[3]),
				static_cast<uint8_t>(uuid.node()[4]),
				static_cast<uint8_t>(uuid.node()[5]),
		};

		return winrt::guid{ data1, data2, data3, data4 };
	}
	
	static GattDeviceService get_service(const BluetoothLEDevice& device, const UUID& guid)
	{
		const auto asyncResult = device.GetGattServicesForUuidAsync(to_winrt_guid(guid)).get();
		if (asyncResult.Status() != GattCommunicationStatus::Success)
		{
			throw std::runtime_error("Cannot get service with UUID " + to_string(guid));
		}

		const auto services = asyncResult.Services();
		if (services.Size() == 0)
		{
			throw std::runtime_error("Service with UUID " + to_string(guid) + " not found.");
		}

		return services.GetAt(0);
	}

	static BluetoothLEDevice device_from_address(const BleDeviceAddress& address)
	{
		const auto deviceAsyncOperation = BluetoothLEDevice::FromBluetoothAddressAsync(to_uint64(address));
		if (deviceAsyncOperation == nullptr) {
			throw std::runtime_error("Device is not reachable. Async operation is null.");
		}
		auto device = deviceAsyncOperation.get();
		if (device == nullptr) {
			throw std::runtime_error("Device is not reachable. Device object pointer is null.");
		}

		if (device.BluetoothAddress() != to_uint64(address))
		{
			throw std::runtime_error("Wrong BLE device address.");
		}
		
		return device;
	}
	
	WinRtBleDevice::WinRtBleDevice(const BleDeviceAddress& address)
	try :
		mDevice(device_from_address(address)),
		mIdentifier(DeviceIdentifier{ BleDeviceAddress::fromUint64(mDevice.BluetoothAddress()) }),
		mName(Utilities::to_narrow(mDevice.Name().c_str())),
		mWinRTConnectionStateNotifier(std::make_shared<Utilities::EventNotifier<>>()),
		mWinRTConnectionStateListener
		(
			[=]()
			{
				mConnectionStateNotifier.notifyAsync(connectionState());
			}
		),
		mConnectionStateToken
		(
			mDevice.ConnectionStatusChanged
			(
				[weakNotifier = std::weak_ptr<Utilities::EventNotifier<>>(mWinRTConnectionStateNotifier)]
				(BluetoothLEDevice, auto)
				{
					auto notifier = weakNotifier.lock();
					if (notifier != nullptr)
					{
						notifier->notify();
					}
				}
			)
		)
	{
		mWinRTConnectionStateNotifier->registerListener(mWinRTConnectionStateListener);
	}
	catch (std::exception & e)
	{
		throw std::runtime_error("Cannot create BLE device: "s + e.what());
	}
	catch (...)
	{
		throw std::runtime_error("Cannot create BLE device.");
	}

	WinRtBleDevice::~WinRtBleDevice()
	{
		mDevice.ConnectionStatusChanged(mConnectionStateToken);
	}

	const DeviceIdentifier& WinRtBleDevice::identifier() const noexcept
	{
		return mIdentifier;
	}

	const std::string& WinRtBleDevice::name() const noexcept
	{
		return mName;
	}

	ConnectionState WinRtBleDevice::connectionState() const noexcept
	{
		try 
		{
			if (mDevice.ConnectionStatus() == BluetoothConnectionStatus::Connected)
			{
				return ConnectionState::InRage;
			}
			else
			{
				return ConnectionState::OutOfRange;
			}
		}
		catch(...)
		{
			return ConnectionState::OutOfRange;
		}
	}

	Utilities::EventNotifier<ConnectionState>& WinRtBleDevice::connectionStateChanged() noexcept
	{
		return mConnectionStateNotifier;
	}

	std::future<std::vector<std::shared_ptr<IBleService>>> WinRtBleDevice::getServicesByUuid(const std::vector<UUID>& services_uuids) const
	{		
		if (services_uuids.empty())
		{
			std::promise<std::vector<std::shared_ptr<IBleService>>> servicesPromise;
			auto futureResult = servicesPromise.get_future();
			mDevice
			.GetGattServicesAsync()
			.Completed
			([servicesResult = std::move(servicesPromise)]
			(IAsyncOperation<GattDeviceServicesResult> async_operation, auto) mutable
			{
				const auto asyncResult = async_operation.GetResults();				
				if (asyncResult.Status() != GattCommunicationStatus::Success)
				{
					throw std::runtime_error("Cannot get all services of device.");
				}

				try 
				{
					const auto services = asyncResult.Services();
					std::vector<std::shared_ptr<IBleService>> servicesPointers;
					std::transform(
						begin(services),
						end(services),
						std::back_inserter(servicesPointers), [](GattDeviceService service)
						{
							return std::make_unique<WinRtBleService>(service);
						});
					servicesResult.set_value(std::move(servicesPointers));
				}
				catch (...)
				{
					try 
					{
						servicesResult.set_exception(std::current_exception());
					}
					catch(...){}
				}
			});
			return futureResult;
		}
		else
		{
			return std::async(std::launch::async, 
				[=]()
				{
					std::vector<std::shared_ptr<IBleService>> servicesPointers;
					std::transform(
						services_uuids.begin(),
						services_uuids.end(),
						std::back_inserter(servicesPointers), [=](const UUID& uuid)
						{
							return std::make_unique<WinRtBleService>(get_service(mDevice, uuid));
						});
					return servicesPointers;
				});
		}
	}
}
