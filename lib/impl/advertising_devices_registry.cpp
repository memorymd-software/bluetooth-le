#include "advertising_devices_registry.h"
#include <algorithm>
#include <iostream>

namespace Neuro::Ble::Impl
{
	AdvertisingDevicesRegistry::AdvertisingDevicesRegistry(const ScanSettings& settings, std::chrono::seconds check_every) :
		AdvertisingDevicesRegistry(settings.MinRssi.value_or(-75), settings.AdvertisementLostTimeout.value_or(std::chrono::seconds(15)), check_every)
	{}
	
	AdvertisingDevicesRegistry::AdvertisingDevicesRegistry(int rssi_threshold, std::chrono::seconds remove_older, std::chrono::seconds check_every) :
		mRSSIThreshold(rssi_threshold),
		mDeviceUpdateLoop([&, remove_older]()
		{
			std::unique_lock filterAdvertisementsLock(mTimestampMapMutex);
			std::vector<DeviceIdentifier> staleDevices;
			for (const auto &advertisement : mAdvertisementsMap)
			{
				if (advertisement.second.TimeStamp < std::chrono::steady_clock::now() - remove_older)
				{					
					staleDevices.push_back(advertisement.first);
				}
			}

			if (!staleDevices.empty())
			{
				for (auto staleDeviceId : staleDevices)
				{
					mAdvertisementsMap.erase(staleDeviceId);
				}
				mListChangedNotifier.notifyAsync();
			}
		}, 
		std::chrono::seconds(check_every))
	{}
	
	void AdvertisingDevicesRegistry::onAdvertisementReceived(const std::vector<AdvertisementData>& advertisements)
	{
		for (const auto &advertisement : advertisements)
		{
			if (advertisement.RSSI < mRSSIThreshold)
				continue;

			std::unique_lock addAdvertisementsLock(mTimestampMapMutex);
			mAdvertisementsMap.insert_or_assign(advertisement.Identifier, advertisement);
			mListChangedNotifier.notifyAsync();
		}
	}
	
	Utilities::EventNotifier<>& AdvertisingDevicesRegistry::advertisementListChanged()
	{
		return mListChangedNotifier;
	}
	
	std::vector<AdvertisementData> AdvertisingDevicesRegistry::advertisingDevices() const
	{
		std::shared_lock readAdvertisementsLock(mTimestampMapMutex);
		std::vector<AdvertisementData> currentlyAdvertising;
		std::transform(mAdvertisementsMap.begin(), mAdvertisementsMap.end(), std::back_inserter(currentlyAdvertising), [](const auto& advertise_pair)
		{
			return advertise_pair.second;
		});		
		return currentlyAdvertising;
	}
}
