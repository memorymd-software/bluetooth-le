#include "advertising_devices_registry.h"
#include "always_on_scanner.h"
#include "android_ble_device.h"
#include "ble_enumerator.h"
#include "java_bt_adapter.h"
#include "java_ble_scanner.h"
#include "jni_global_ref.h"

namespace Neuro::Ble
{
	namespace Impl
	{		
		class AndroidBleEnumerator final : public IBleEnumerator
		{
		public:
			AndroidBleEnumerator(JNIEnv* env, std::unique_ptr<IJavaBtAdapter> &&adapter) :
				mJavaVM(Jni::get_java_vm(env)),
				mScanCallback(env),
				mAdapter(std::move(adapter)),
				mScanner(env, mAdapter->getBluetoothLeScanner(env), mScanCallback.javaCallback())
			{
				mScanCallback.advertisementsReceived().registerListener(mAdvertisementListener);
			}

			AndroidBleEnumerator(JNIEnv* env, std::unique_ptr<IJavaBtAdapter>&& adapter, const std::vector<ScanFilter>& scan_filters) :
				mJavaVM(Jni::get_java_vm(env)),
				mScanCallback(env),
				mAdapter(std::move(adapter)),
				mScanner(env, mAdapter->getBluetoothLeScanner(env), mScanCallback.javaCallback(), scan_filters)
			{
				mScanCallback.advertisementsReceived().registerListener(mAdvertisementListener);
			}

			AndroidBleEnumerator(JNIEnv* env, std::unique_ptr<IJavaBtAdapter>&& adapter, const ScanSettings& scan_settings) :
				mRegistry(scan_settings),
				mJavaVM(Jni::get_java_vm(env)),
				mScanCallback(env),
				mAdapter(std::move(adapter)),
				mScanner(env, mAdapter->getBluetoothLeScanner(env), mScanCallback.javaCallback())
			{
				mScanCallback.advertisementsReceived().registerListener(mAdvertisementListener);
			}

			AndroidBleEnumerator(JNIEnv* env, std::unique_ptr<IJavaBtAdapter>&& adapter, const std::vector<ScanFilter>& scan_filters, const ScanSettings& scan_settings) :
				mRegistry(scan_settings),
				mJavaVM(Jni::get_java_vm(env)),
				mScanCallback(env),
				mAdapter(std::move(adapter)),
				mScanner(env, mAdapter->getBluetoothLeScanner(env), mScanCallback.javaCallback(), scan_filters)
			{
				mScanCallback.advertisementsReceived().registerListener(mAdvertisementListener);
			}

			std::vector<AdvertisementData> advertisingDevices() const override
			{
				return mRegistry.advertisingDevices();
			}
			
			Utilities::EventNotifier<>& advertisementListChanged() override
			{
				return mRegistry.advertisementListChanged();
			}
			
			std::unique_ptr<IBleDevice> createBleDevice(const DeviceIdentifier& identifier) const override
			{
				try
				{
					return Jni::call_in_attached_env(mJavaVM, [&](JNIEnv* env)
					{
						return mAdapter->getRemoteDevice(env, identifier);
					});
				}
				catch (std::exception & e)
				{
					throw std::runtime_error(std::string("Cannot create BLE device: ") + e.what());
				}
				catch (...)
				{
					throw std::runtime_error("Cannot create BLE device.");
				}
			}

		private:
			AdvertisingDevicesRegistry mRegistry;
			JavaVM* mJavaVM;
			BleScanCallback mScanCallback;
			std::unique_ptr<IJavaBtAdapter> mAdapter;
			AlwaysOnScanner mScanner;
			Utilities::EventListener<const std::vector<AdvertisementData>&> mAdvertisementListener
			{
				[=](const auto& advertisement_data)
				{
					mRegistry.onAdvertisementReceived(advertisement_data);
				}
			};
		};
	}

	std::unique_ptr<IBleEnumerator> create_ble_enumerator(JNIEnv *env, jobject context)
	{
		try
		{
			auto adapter = std::make_unique<Impl::JavaBtAdapter>(env, Impl::Jni::JavaObject(context));
			if (!adapter->isEnabled(env))
			{
				throw std::runtime_error("BT adapter disabled.");
			}

			return std::make_unique<Impl::AndroidBleEnumerator>(env, std::move(adapter));
		}
		catch (std::exception & e)
		{
			throw std::runtime_error(std::string("Cannot get BLE enumerator: ") + e.what());
		}
		catch (...)
		{
			throw std::runtime_error("Cannot get bluetooth enumerator.");
		}
	}

	std::unique_ptr<IBleEnumerator> create_ble_enumerator(JNIEnv* env, jobject context, const std::vector<ScanFilter>& scan_filters)
	{
		try
		{
			auto adapter = std::make_unique<Impl::JavaBtAdapter>(env, Impl::Jni::JavaObject(context));
			if (!adapter->isEnabled(env))
			{
				throw std::runtime_error("BT adapter disabled.");
			}

			return std::make_unique<Impl::AndroidBleEnumerator>(env, std::move(adapter), scan_filters);
		}
		catch (std::exception & e)
		{
			throw std::runtime_error(std::string("Cannot get BLE enumerator: ") + e.what());
		}
		catch (...)
		{
			throw std::runtime_error("Cannot get bluetooth enumerator.");
		}
	}
	
	std::unique_ptr<IBleEnumerator> create_ble_enumerator(JNIEnv* env, jobject context, const ScanSettings& scan_settings)
	{
		try
		{
			auto adapter = std::make_unique<Impl::JavaBtAdapter>(env, Impl::Jni::JavaObject(context));
			if (!adapter->isEnabled(env))
			{
				throw std::runtime_error("BT adapter disabled.");
			}

			return std::make_unique<Impl::AndroidBleEnumerator>(env, std::move(adapter), scan_settings);
		}
		catch (std::exception & e)
		{
			throw std::runtime_error(std::string("Cannot get BLE enumerator: ") + e.what());
		}
		catch (...)
		{
			throw std::runtime_error("Cannot get bluetooth enumerator.");
		}
	}
	
	std::unique_ptr<IBleEnumerator> create_ble_enumerator(JNIEnv* env, jobject context, const std::vector<ScanFilter>& scan_filters, const ScanSettings& scan_settings)
	{
		try
		{
			auto adapter = std::make_unique<Impl::JavaBtAdapter>(env, Impl::Jni::JavaObject(context));
			if (!adapter->isEnabled(env))
			{
				throw std::runtime_error("BT adapter disabled.");
			}

			return std::make_unique<Impl::AndroidBleEnumerator>(env, std::move(adapter), scan_filters, scan_settings);
		}
		catch (std::exception & e)
		{
			throw std::runtime_error(std::string("Cannot get BLE enumerator: ") + e.what());
		}
		catch (...)
		{
			throw std::runtime_error("Cannot get bluetooth enumerator.");
		}
	}
}
