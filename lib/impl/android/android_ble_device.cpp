#include "android_ble_device.h"
#include <exception>
#include "jni_string.h"
#include "android_ble_service.h"
#include <thread>
#include <chrono>

using namespace std::string_literals;

namespace Neuro::Ble::Impl
{
	static std::string get_device_name(JNIEnv* env, const Jni::JavaObject& ble_device)
	{
		try
		{
			jstring deviceNameJString{ nullptr };
			auto attempts = 3;
			do
			{				
				deviceNameJString = static_cast<jstring>(ble_device.callMethod<jobject>(env, "getName", "()Ljava/lang/String;"));
				if (deviceNameJString == nullptr) std::this_thread::sleep_for(std::chrono::milliseconds(100));
			}
			while (deviceNameJString == nullptr && attempts-- > 0);
			
			Jni::JavaString deviceName{ deviceNameJString };
			return deviceName.toStdString(env);
		}
		catch (std::exception &e)
		{
			throw std::runtime_error("Cannot get an Android BLE device name: "s + e.what());
		}
		catch (...)
		{
			throw std::runtime_error("Cannot get an Android BLE device name.");
		}
	}

	static DeviceIdentifier get_device_identifier(JNIEnv* env, const Jni::JavaObject& ble_device)
	{
		try
		{
			Jni::JavaString deviceAddress{ static_cast<jstring>(ble_device.callMethod<jobject>(env, "getAddress", "()Ljava/lang/String;")) };
			return DeviceIdentifier{ BleDeviceAddress { deviceAddress.toStdString(env) } };
		}
		catch (std::exception &e)
		{
			throw std::runtime_error("Cannot get an Android BLE device identifier: "s + e.what());
		}
		catch (...)
		{
			throw std::runtime_error("Cannot get an Android BLE device identifier.");
		}
	}
	
	AndroidBleDevice::AndroidBleDevice(JNIEnv* env, const Jni::JavaObject& device_ref, const Jni::JavaObject& context, std::chrono::seconds timeout)
	try :
		mJavaVM(Jni::get_java_vm(env)),
		mName(get_device_name(env, device_ref)),
		mIdentifier(get_device_identifier(env, device_ref)),
		mGatt(env, device_ref, context, timeout)
	{}
	catch (std::exception &e)
	{
		throw std::runtime_error("Cannot create Android BLE device: "s + e.what());
	}
	catch (...)
	{
		throw std::runtime_error("Cannot create Android BLE device.");
	}

	const DeviceIdentifier& AndroidBleDevice::identifier() const noexcept
	{
		return mIdentifier;
	}

	const std::string& AndroidBleDevice::name() const noexcept
	{
		return mName;
	}

	ConnectionState AndroidBleDevice::connectionState() const noexcept
	{
		return mGatt.connectionState();
	}

	Utilities::EventNotifier<ConnectionState>& AndroidBleDevice::connectionStateChanged() noexcept
	{
		return mGatt.connectionStateChanged();
	}

	std::future<std::vector<std::shared_ptr<IBleService>>> AndroidBleDevice::getServicesByUuid(const std::vector<UUID>& services_uuids) const
	{
		return mGatt.getServicesByUuid(services_uuids);
	}
}