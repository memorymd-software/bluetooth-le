#include "device_identifier.h"

namespace Neuro::Ble
{

	bool operator==(const DeviceIdentifier& lhs, const DeviceIdentifier& rhs)
	{
		return lhs.Address == rhs.Address;
	}

	std::ostream& operator<<(std::ostream& out_stream, const DeviceIdentifier& identifier)
	{
		return out_stream << identifier.Address;
	}

	std::string to_string(const DeviceIdentifier &identifier)
	{
		return to_string(identifier.Address);
	}
}

size_t std::hash<Neuro::Ble::DeviceIdentifier>::operator()(const Neuro::Ble::DeviceIdentifier& identifier) const noexcept
{
	using std::hash;
	return hash<Neuro::Ble::BleDeviceAddress>()(identifier.Address);
}