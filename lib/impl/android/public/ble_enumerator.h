#ifndef BLE_ENUMERATOR_H
#define BLE_ENUMERATOR_H

#include "ble_enumerator_interface.h"
#include "scan_filter.h"
#include "scan_settings.h"
#include "jni.h"
#include <vector>

namespace Neuro::Ble
{
	std::unique_ptr<IBleEnumerator> create_ble_enumerator(JNIEnv *env, jobject context);
	std::unique_ptr<IBleEnumerator> create_ble_enumerator(JNIEnv *env, jobject context, const std::vector<ScanFilter>& scan_filters);
	std::unique_ptr<IBleEnumerator> create_ble_enumerator(JNIEnv *env, jobject context, const ScanSettings &scan_settings);
	std::unique_ptr<IBleEnumerator> create_ble_enumerator(JNIEnv *env, jobject context, const std::vector<ScanFilter>& scan_filters, const ScanSettings& scan_settings);
}

#endif // BLE_ENUMERATOR_H