cmake_minimum_required(VERSION 3.14)

if (TEST_PRIVATE_MODULES)
	target_include_directories(bluetoothle PUBLIC include)
else()
	target_include_directories(bluetoothle PRIVATE include)
endif()

target_sources(bluetoothle 
	PUBLIC 
		java_bt_adapter.cpp
	)