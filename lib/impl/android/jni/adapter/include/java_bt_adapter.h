#ifndef JAVA_BT_ADAPTER_H
#define JAVA_BT_ADAPTER_H

#include "jni_object.h"
#include "jni_global_ref.h"
#include "java_ble_scanner.h"
#include "ble_device.h"
#include <memory>

namespace Neuro::Ble::Impl
{
	class IJavaBtAdapter
	{
	public:
		virtual ~IJavaBtAdapter() = default;

		virtual bool isEnabled(JNIEnv* env) const = 0;
		virtual std::unique_ptr<IJavaBleScanner> getBluetoothLeScanner(JNIEnv* env) const = 0;
		virtual std::unique_ptr<IBleDevice> getRemoteDevice(JNIEnv* env, const DeviceIdentifier& id) const = 0;
	};
	
	class JavaBtAdapter final : public IJavaBtAdapter
	{
	public:
		JavaBtAdapter(JNIEnv* env, const Jni::JavaObject &context);

		bool isEnabled(JNIEnv* env) const override;
		std::unique_ptr<IJavaBleScanner> getBluetoothLeScanner(JNIEnv* env) const override;
		std::unique_ptr<IBleDevice> getRemoteDevice(JNIEnv* env, const DeviceIdentifier& id) const override;
	private:
		const Jni::GlobalRef<Jni::JavaObject> mContext;
		const Jni::GlobalRef<Jni::JavaObject> mAdapterObject;
	};
}

#endif //JAVA_BT_ADAPTER_H
