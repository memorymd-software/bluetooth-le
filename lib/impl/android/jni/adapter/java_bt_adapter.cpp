#include "java_bt_adapter.h"
#include "jni_string.h"
#include "android_ble_device.h"

namespace Neuro::Ble::Impl
{
	Jni::GlobalRef<Jni::JavaObject> obtain_adapter(JNIEnv* env, const Jni::JavaObject& context)
	{
		try
		{
			Jni::JavaClass contextClass{ env, "android/content/Context" };
			const auto btServiceName = contextClass.getStaticFieldValue<jstring>(env, "BLUETOOTH_SERVICE");
			Jni::JavaObject bluetoothManager(context.callMethod<jobject>(env, "getSystemService", "(Ljava/lang/String;)Ljava/lang/Object;", btServiceName));
			auto javaAdapter = bluetoothManager.callMethod<jobject>(env, "getAdapter", "()Landroid/bluetooth/BluetoothAdapter;");
			if (javaAdapter == nullptr)
			{
				throw std::runtime_error("Adapter is null.");
			}
			Jni::JavaObject adapter(javaAdapter);
			return Jni::GlobalRef<Jni::JavaObject>(env, adapter);
		}
		catch (std::exception & e)
		{
			throw std::runtime_error(std::string("Cannot get bluetooth adapter: ") + e.what());
		}
		catch (...)
		{
			throw std::runtime_error("Cannot get bluetooth adapter");
		}
	}

	JavaBtAdapter::JavaBtAdapter(JNIEnv* env, const Jni::JavaObject& context) :
		mContext(env, context),
		mAdapterObject(obtain_adapter(env, *mContext))
	{}

	bool JavaBtAdapter::isEnabled(JNIEnv* env) const
	{
		return mAdapterObject->callMethod<jboolean>(env, "isEnabled", "()Z");
	}

	std::unique_ptr<IJavaBleScanner> JavaBtAdapter::getBluetoothLeScanner(JNIEnv * env) const
	{
		return std::make_unique<JavaBleScanner>(env, *mAdapterObject);
	}

	std::unique_ptr<IBleDevice> JavaBtAdapter::getRemoteDevice(JNIEnv* env, const DeviceIdentifier& id) const
	{
		Jni::JavaString addressString{ env, to_string(id.Address) };
		auto javaDevice = mAdapterObject->callMethod<jobject>
			(
				env,
				"getRemoteDevice",
				"(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;",
				addressString
			);
		if (javaDevice == nullptr)
		{
			throw std::runtime_error("Java Device is null.");
		}
		auto device = Jni::JavaObject{javaDevice};

		return std::make_unique<AndroidBleDevice>(env, device, *mContext);
	}
}
