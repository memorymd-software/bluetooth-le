#include "android_ble_characteristic.h"
#include <mutex>
#include <condition_variable>
#include <chrono>
#include "jni_string.h"

using namespace std::string_literals;

namespace Neuro::Ble::Impl
{
	static void subscribe_notifications(JNIEnv* env, const Jni::JavaObject& gatt, const std::shared_ptr<IBleGattCallback> &gatt_callback, const Jni::JavaObject& characteristic, const Utilities::EventListener<const UUID&, std::vector<std::byte>> &listener)
	{
		try
		{
			auto setResult = gatt.callMethod<jboolean>
			(
				env, 
				"setCharacteristicNotification",
				"(Landroid/bluetooth/BluetoothGattCharacteristic;Z)Z", 
				static_cast<jobject>(characteristic),
				static_cast<jboolean>(true)
			);
			if (!setResult)
			{
				throw std::runtime_error("Failed to set characteristic notifications.");
			}

			Jni::JavaClass uuidClass{ env, "java/util/UUID" };
			Jni::JavaString uuidJavaString{ env, "00002902-0000-1000-8000-00805f9b34fb" };
			Jni::JavaObject javaCCCDUuid{ uuidClass.callStaticMethod<jobject>(env, "fromString", "(Ljava/lang/String;)Ljava/util/UUID;", static_cast<jstring>(uuidJavaString)) };			
			Jni::JavaObject descriptor{ characteristic.callMethod<jobject>
			(
					env,
					"getDescriptor",
					"(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattDescriptor;",
					static_cast<jobject>(javaCCCDUuid)
			) };

			Jni::JavaClass descriptorClass{ descriptor.objectClass(env) };
			Jni::JavaObject enableNotificationValue{ descriptorClass.getStaticFieldValue<jobject>(env, "ENABLE_NOTIFICATION_VALUE", "[B") };
			auto descriptorSetResult = descriptor.callMethod<jboolean>(env, "setValue", "([B)Z",  static_cast<jobject>(enableNotificationValue));
			if (!descriptorSetResult)
			{
				throw std::runtime_error("Set descriptor value operation failed.");
			}
			
			std::mutex writeMutex;
			std::condition_variable writeCondition;
			auto descWritten{ false };
			Utilities::EventListener<const UUID&> descWriteListener{ [&writeMutex, &writeCondition, &descWritten](const UUID& guid)
			{
				std::unique_lock writeLock{writeMutex};
				descWritten = true;
				writeCondition.notify_all();
			} };
			gatt_callback->descriptorWritten().registerListener(descWriteListener);

			std::unique_lock writeLock{ writeMutex };
			auto descWriteResult = gatt.callMethod<jboolean>(env, "writeDescriptor", "(Landroid/bluetooth/BluetoothGattDescriptor;)Z", static_cast<jobject>(descriptor));
			if (!descWriteResult)
			{
				throw std::runtime_error("Write descriptor value operation failed.");
			}

			const auto isWritten = writeCondition.wait_for(writeLock, std::chrono::seconds(5), [&descWritten]() {return descWritten; });
			if (!isWritten)
			{
				throw std::runtime_error("Descriptor write operation timeout.");
			}
			gatt_callback->characteristicChanged().registerListener(listener);
		}
		catch (std::exception &e)
		{
			throw std::runtime_error("Cannot subscribe BleCharacteristic notifications: "s + e.what());
		}
		catch (...)
		{
			throw std::runtime_error("Cannot subscribe BleCharacteristic notifications.");
		}
	}

	static UUID get_characteristic_uuid(JNIEnv* env, const Jni::JavaObject& characteristic)
	{
		try
		{
			Jni::JavaObject javaUuid(characteristic.callMethod<jobject>(env, "getUuid", "()Ljava/util/UUID;"));
			Jni::JavaString uuidJavaString(static_cast<jstring>(javaUuid.callMethod<jobject>(env, "toString", "()Ljava/lang/String;")));
			return UUID{ uuidJavaString.toStdString(env) };
		}
		catch (std::exception & e)
		{
			throw std::runtime_error("Cannot subscribe BleCharacteristic notifications: "s + e.what());
		}
		catch (...)
		{
			throw std::runtime_error("Cannot subscribe BleCharacteristic notifications.");
		}
	}
	
	AndroidBleCharacteristic::AndroidBleCharacteristic
	(
		JNIEnv* env, 
		const Jni::GlobalRef<Jni::JavaObject>& gatt, 
		std::shared_ptr<IBleGattCallback> gatt_callback, 
		const Jni::JavaObject& characteristic
	)
	try :
		mJavaVm(Jni::get_java_vm(env)),
		mGattCallback(gatt_callback),
		mGatt(gatt),
		mCharacteristic(env, characteristic),
		mCharChangedListener([=](const auto& uuid, std::vector<std::byte> value)
			{ 
				Jni::call_in_attached_env(mJavaVm, [=](auto e)
					{
						const auto guid = get_characteristic_uuid(e, *mCharacteristic);
						if (guid == uuid)
						{
							mCharChangedNotifier.notifyAsync(value);
						}
					});
			})
	{
		if (static_cast<unsigned char>(properties()) & static_cast<unsigned char>(CharacteristicProperties::Notify))
		{
			subscribe_notifications(env, *mGatt, mGattCallback, characteristic, mCharChangedListener);
		}
	}
	catch (std::exception & e)
	{
		throw std::runtime_error("Cannot create BleCharacteristic: "s + e.what());
	}
	catch (...)
	{
		throw std::runtime_error("Cannot create BleCharacteristic.");
	}
	
	CharacteristicProperties AndroidBleCharacteristic::properties() const noexcept
	{
		try
		{
			return Jni::call_in_attached_env(mJavaVm, [=](auto env)
				{
					Jni::JavaClass characteristicClass{ mCharacteristic->objectClass(env) };
					const auto propertyRead = characteristicClass.getStaticFieldValue<jint>(env, "PROPERTY_READ");
					const auto propertyNotify = characteristicClass.getStaticFieldValue<jint>(env, "PROPERTY_NOTIFY");
					const auto propertyWrite = characteristicClass.getStaticFieldValue<jint>(env, "PROPERTY_WRITE");
					const auto propertyWriteNoResponse = characteristicClass.getStaticFieldValue<jint>(env, "PROPERTY_WRITE_NO_RESPONSE");

					auto propertiesFlags = mCharacteristic->callMethod<jint>(env, "getProperties", "()I");

					auto properties = CharacteristicProperties::None;
					if (propertiesFlags & propertyRead)
					{
						properties = static_cast<CharacteristicProperties>(static_cast<unsigned char>(properties) | static_cast<unsigned char>(CharacteristicProperties::Read));
					}

					if ((propertiesFlags & propertyWrite) | (propertiesFlags & propertyWriteNoResponse))
					{
						properties = static_cast<CharacteristicProperties>(static_cast<unsigned char>(properties) | static_cast<unsigned char>(CharacteristicProperties::Write));
					}

					if (propertiesFlags & propertyNotify)
					{
						properties = static_cast<CharacteristicProperties>(static_cast<unsigned char>(properties) | static_cast<unsigned char>(CharacteristicProperties::Notify));
					}

					return properties;
				});
		}
		catch (std::exception & e)
		{
			throw std::runtime_error("Cannot get BleCharacteristic properties: "s + e.what());
		}
		catch (...)
		{
			throw std::runtime_error("Cannot get BleCharacteristic properties.");
		}
	}
	
	std::vector<std::byte> AndroidBleCharacteristic::value() const
	{
		try
		{
			if (!(static_cast<unsigned char>(properties()) & static_cast<unsigned char>(CharacteristicProperties::Read)))
			{
				throw std::runtime_error("Characteristic does not have Read property.");
			}

			return Jni::call_in_attached_env(mJavaVm, [=](auto env)
			{
				std::mutex readMutex;
				std::condition_variable readCondition;
				auto charRead{ false };
				const auto uuid = get_characteristic_uuid(env, *mCharacteristic);
				Utilities::EventListener<const UUID&> charReadListener{ [&readMutex, &readCondition, &charRead, &uuid](const UUID& guid)
				{
					if (guid == uuid)
					{
						std::unique_lock readLock{readMutex};
						charRead = true;
						readCondition.notify_all();
					}
				} };
				mGattCallback->characteristicRead().registerListener(charReadListener);

				std::unique_lock readLock{ readMutex };
				if (!mGatt->callMethod<jboolean>(env, "readCharacteristic", "(Landroid/bluetooth/BluetoothGattCharacteristic;)Z", static_cast<jobject>(*mCharacteristic)))
				{
					throw std::runtime_error("Read operation failed.");
				}
				
				const auto isRead = readCondition.wait_for(readLock, std::chrono::seconds(5), [&charRead]() {return charRead; });
				if (!isRead)
				{
					throw std::runtime_error("Read operation timeout.");
				}

				Jni::JavaArray<jbyte> charValue{ static_cast<jbyteArray>(mCharacteristic->callMethod<jobject>(env, "getValue", "()[B"))};
				auto charValueVector = charValue.toVector(env);
				std::vector<std::byte> result{ charValueVector.size() };
				std::transform(charValueVector.begin(), charValueVector.end(), result.begin(), [](jbyte value) {return static_cast<std::byte>(value); });
				return result;
			});
		}
		catch (std::exception & e)
		{
			throw std::runtime_error("Cannot read BleCharacteristic value: "s + e.what());
		}
		catch (...)
		{
			throw std::runtime_error("Cannot read BleCharacteristic value.");
		}
	}

	std::vector<std::byte> AndroidBleCharacteristic::cachedValue() const
	{
		try
		{
			return Jni::call_in_attached_env(mJavaVm, [=](auto env)
				{					
					Jni::JavaArray<jbyte> charValue{ static_cast<jbyteArray>(mCharacteristic->callMethod<jobject>(env, "getValue", "()[B")) };
					auto charValueVector = charValue.toVector(env);
					std::vector<std::byte> result{ charValueVector.size() };
					std::transform(charValueVector.begin(), charValueVector.end(), result.begin(), [](jbyte value) {return static_cast<std::byte>(value); });
					return result;
				});
		}
		catch (std::exception & e)
		{
			throw std::runtime_error("Cannot read cached BleCharacteristic value: "s + e.what());
		}
		catch (...)
		{
			throw std::runtime_error("Cannot read cached BleCharacteristic value.");
		}
	}
	
	Utilities::EventNotifier<const std::vector<std::byte>&>& AndroidBleCharacteristic::valueChanged()
	{
		return mCharChangedNotifier;
	}
	
	void AndroidBleCharacteristic::write(const std::vector<std::byte> &value)
	{
		try
		{
			if (!(static_cast<unsigned char>(properties())& static_cast<unsigned char>(CharacteristicProperties::Write)))
			{
				throw std::runtime_error("Characteristic does not have Write property.");
			}

			auto isWritten = false;
			Jni::call_in_attached_env(mJavaVm, [=, &isWritten](auto env)
				{
					Jni::JavaArray<jbyte> javaValue{ env, value.begin(), value.size() };
					if (!mCharacteristic->callMethod<jboolean>(env, "setValue", "([B)Z", javaValue.javaArray()))
					{
						//throw std::runtime_error("Write operation failed.");
						return;
					}

					std::mutex writeMutex;
					std::condition_variable writeCondition;
					auto charWritten{ false };
					const auto uuid = get_characteristic_uuid(env, *mCharacteristic);
					Utilities::EventListener<const UUID&> charWriteListener{ [&writeMutex, &writeCondition, &charWritten, &uuid](const UUID& guid)
					{
						if (guid == uuid)
						{
							std::unique_lock writeLock{writeMutex};
							charWritten = true;
							writeCondition.notify_all();
						}
					} };
					mGattCallback->characteristicWritten().registerListener(charWriteListener);

					std::unique_lock writeLock{ writeMutex };
					auto writeResult = true;
					auto attempts = 3;
					do
					{
						if (!writeResult) std::this_thread::sleep_for(std::chrono::milliseconds(100));
						writeResult = mGatt->callMethod<jboolean>(env, "writeCharacteristic", "(Landroid/bluetooth/BluetoothGattCharacteristic;)Z", static_cast<jobject>(*mCharacteristic));
					} while (!writeResult && attempts-- > 0);
					if (!writeResult)
					{
						//throw std::runtime_error("Write operation failed.");
						return;
					}

					isWritten = writeCondition.wait_for(writeLock, std::chrono::seconds(2), [&charWritten]() {return charWritten; });
					writeLock.unlock();
				});
			if (!isWritten)
			{
				throw std::runtime_error("Write operation timeout.");
			}
		}
		catch (std::exception & e)
		{
			throw std::runtime_error("Cannot write BleCharacteristic value: "s + e.what());
		}
		catch (...)
		{
			throw std::runtime_error("Cannot write BleCharacteristic value.");
		}
	}
}