#include "android_ble_service.h"
#include "android_ble_characteristic.h"
#include "jni_string.h"

using namespace std::string_literals;

namespace Neuro::Ble::Impl
{
	AndroidBleService::AndroidBleService(JNIEnv* env, const Jni::GlobalRef<Jni::JavaObject>& gatt, std::shared_ptr<IBleGattCallback> gatt_callback, const Jni::JavaObject& service)
	try :
		mJavaVm(Jni::get_java_vm(env)),
		mGattCallback(gatt_callback),
		mGatt(gatt),
		mService(env, service)
	{}
	catch (std::exception & e)
	{
		throw std::runtime_error("Cannot create BleService: "s + e.what());
	}
	catch (...)
	{
		throw std::runtime_error("Cannot create BleService.");
	}
	
	std::future<std::vector<std::shared_ptr<IBleCharacteristic>>> AndroidBleService::getCharacteristicsByUuid(const std::vector<UUID>& char_uuids) const 
	{
		return std::async(std::launch::async, [=]()
		{
			if (char_uuids.empty())
			{
				return Jni::call_in_attached_env(mJavaVm, [=](auto env)
				{
					Jni::JavaObject charsList{ mGatt->callMethod<jobject>(env, "getCharacteristics", "()Ljava/util/List;") };
					const auto charsListSize = charsList.callMethod<jint>(env, "size", "()I");
					std::vector<std::shared_ptr<IBleCharacteristic>> charsPointers(charsListSize);
					for (size_t i = 0; i < charsPointers.size(); ++i)
					{
						Jni::JavaObject characteristic{ charsList.callMethod<jobject>(env, "get", "(I)java/util/Object") };
						charsPointers[i] = std::make_unique<AndroidBleCharacteristic>(env, mGatt, mGattCallback, characteristic);
					}
					return charsPointers;
				});
			}
			else
			{
				return Jni::call_in_attached_env(mJavaVm, [=](auto env)
				{
					std::vector<std::shared_ptr<IBleCharacteristic>> charPointers;
					std::transform(char_uuids.begin(), char_uuids.end(), std::back_inserter(charPointers), [=](const auto &guid)
					{
						Jni::JavaClass uuidClass{ env, "java/util/UUID" };
						Jni::JavaString uuidJavaString{ env, to_string(guid) };
						Jni::JavaObject characteristicUuid{ uuidClass.callStaticMethod<jobject>(env, "fromString", "(Ljava/lang/String;)Ljava/util/UUID;", static_cast<jstring>(uuidJavaString)) };
						Jni::JavaObject characteristic{ mService->callMethod<jobject>(env, "getCharacteristic", "(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattCharacteristic;", static_cast<jobject>(characteristicUuid)) };
						return std::make_shared<AndroidBleCharacteristic>(env, mGatt, mGattCallback, characteristic);
					});
					return charPointers;
				});
			}
		});
	}
}
