#include "ble_gatt.h"
#include "android_ble_service.h"
#include "jni_string.h"

using namespace std::string_literals;

namespace Neuro::Ble::Impl
{
	static Jni::GlobalRef<Jni::JavaObject> connect_gatt(JNIEnv* env, const Jni::JavaObject& ble_device, const Jni::JavaObject& context, const JavaGattCallback& gatt_callback)
	{
		try
		{
			Jni::JavaObject deviceGatt
			{
				ble_device.callMethod<jobject>
				(
					env,
					"connectGatt",
					"(Landroid/content/Context;ZLandroid/bluetooth/BluetoothGattCallback;)Landroid/bluetooth/BluetoothGatt;",
					static_cast<jobject>(context),
					static_cast<jboolean>(false),
					static_cast<jobject>(gatt_callback.javaObject())
				)
			};
			return Jni::GlobalRef{ env, deviceGatt };
		}
		catch (std::exception &e)
		{
			throw std::runtime_error("Cannot connect to GATT server: "s + e.what());
		}
		catch (...)
		{
			throw std::runtime_error("Cannot connect to GATT server.");
		}		
	}
	
	BleGatt::BleGatt(JNIEnv* env, const Jni::JavaObject& ble_device, const Jni::JavaObject& context, std::chrono::seconds timeout)
		try :
		mJavaVm(Jni::get_java_vm(env)),
		mConnectionListener([&](auto state) { onConnectionStateChanged(state); }),
		mServiceDiscoveryListener([&]() { onServicesDiscovered(); }),
		mGattCallback(std::make_shared<BleGattCallback>(env, mConnectionListener, mServiceDiscoveryListener)),
		mGatt(connect_gatt(env, ble_device, context, mGattCallback->javaCallback()))
	{
		waitForConnect(timeout);
	}
	catch (std::exception & e)
	{
		throw std::runtime_error("Cannot create BleGatt: "s + e.what());
	}
	catch (...)
	{
		throw std::runtime_error("Cannot create BleGatt.");
	}

	BleGatt::~BleGatt()
	{
		try 
		{
			Jni::call_in_attached_env(mJavaVm, [=](auto env)
			{
				mGatt->callMethod<void>(env, "disconnect", "()V");
				mGatt->callMethod<void>(env, "close", "()V");
			});
		}
		catch(...)
		{
			//do nothing
		}
	}
	
	std::future<std::vector<std::shared_ptr<IBleService>>> BleGatt::getServicesByUuid(const std::vector<UUID>& services_uuids) const
	{
		return std::async(std::launch::async, [=]() {
			if (!mIsDiscovered)
			{
				discoverServices();
			}
			if (services_uuids.empty())
			{
				return Jni::call_in_attached_env(mJavaVm, [=](auto env)
					{						
						Jni::JavaObject servicesList{ mGatt->callMethod<jobject>(env, "getServices", "()Ljava/util/List;") };
						const auto servicesListSize = servicesList.callMethod<jint>(env, "size", "()I");
						std::vector<std::shared_ptr<IBleService>> servicesPointers(servicesListSize);
						for (size_t i = 0; i < servicesPointers.size(); ++i)
						{
							Jni::JavaObject service{ servicesList.callMethod<jobject>(env, "get", "(I)java/util/Object") };
							servicesPointers[i] = std::make_shared<AndroidBleService>(env, mGatt, mGattCallback, service);
						}
						return servicesPointers;
					});
			}
			else
			{
				return Jni::call_in_attached_env(mJavaVm, [=](auto env)
					{
						std::vector<std::shared_ptr<IBleService>> servicesPointers;
						std::transform(services_uuids.begin(), services_uuids.end(), std::back_inserter(servicesPointers), [=](const auto& guid) {
							Jni::JavaClass uuidClass{ env, "java/util/UUID" };
							Jni::JavaString uuidJavaString{ env, to_string(guid) };
							Jni::JavaObject serviceUuid{ uuidClass.callStaticMethod<jobject>(env, "fromString", "(Ljava/lang/String;)Ljava/util/UUID;", static_cast<jstring>(uuidJavaString)) };
							Jni::JavaObject service{ mGatt->callMethod<jobject>(env, "getService", "(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattService;", static_cast<jobject>(serviceUuid)) };
							return std::make_shared<AndroidBleService>(env, mGatt, mGattCallback, service);
							});
						return servicesPointers;
					});
			}

			});
	}

	ConnectionState BleGatt::connectionState() const noexcept
	{
		if (mIsConnected)
		{
			return ConnectionState::InRage;
		}
		else
		{
			return ConnectionState::OutOfRange;
		}
	}

	Utilities::EventNotifier<ConnectionState>& BleGatt::connectionStateChanged() noexcept
	{
		return mConnectionStateNotifier;
	}

	void BleGatt::waitForConnect(std::chrono::seconds timeout)
	{
		std::unique_lock connectionLock(mConnectMutex);
		
		if (mIsConnected)
			return;
		
		const auto deviceConnected = mConnectCondition.wait_for(connectionLock, timeout, [&]() {return mIsConnected; });
		if (!deviceConnected)
		{
			Jni::call_in_attached_env(mJavaVm, [=](auto env)
			{
				mGatt->callMethod<void>(env, "close", "()V");
			});
			throw std::runtime_error("Connection timeout.");
		}
	}
	void BleGatt::onConnectionStateChanged(AndroidGattConnectionState state)
	{
		std::unique_lock connectionLock(mConnectMutex);
		mIsConnected = state == AndroidGattConnectionState::Connected;
		mConnectionStateNotifier.notifyAsync(connectionState());
		mConnectCondition.notify_all();
	}
	
	void BleGatt::onServicesDiscovered()
	{
		std::unique_lock discoveryLock(mDiscoveryMutex);
		mIsDiscovered = true;
		mDiscoveryCondition.notify_all();
	}
	
	void BleGatt::discoverServices() const
	{
		std::unique_lock discoveryLock(mDiscoveryMutex);
		
		if (mIsDiscovered)
			return;

		Jni::call_in_attached_env(mJavaVm, [&](auto env)
		{
			if (!mGatt->callMethod<jboolean>(env, "discoverServices", "()Z"))
			{
				throw std::runtime_error("Discover operation failed.");
			}
		});
		
		const auto servicesDiscovered = mDiscoveryCondition.wait_for(discoveryLock, std::chrono::seconds(5), [&]() { return mIsDiscovered; });
		if (!servicesDiscovered)
		{
			throw std::runtime_error("Services discovery timeout.");
		}
	}
}