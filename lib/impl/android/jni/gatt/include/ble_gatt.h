#ifndef BLE_GATT_H
#define BLE_GATT_H

#include "jni_object.h"
#include "jni_global_ref.h"
#include "ble_gatt_callback.h"
#include "ble_device.h"
#include <mutex>
#include <condition_variable>

namespace Neuro::Ble::Impl
{
	class BleGatt final
	{
	public:
		BleGatt(JNIEnv* env, const Jni::JavaObject& ble_device, const Jni::JavaObject& context, std::chrono::seconds timeout);
		~BleGatt();

		std::future<std::vector<std::shared_ptr<IBleService>>> getServicesByUuid(const std::vector<UUID>& services_uuids) const;
		[[nodiscard]] ConnectionState connectionState() const noexcept;
		Utilities::EventNotifier<ConnectionState>& connectionStateChanged() noexcept;
				
	private:
		std::mutex mConnectMutex;
		std::condition_variable mConnectCondition;
		mutable std::mutex mDiscoveryMutex;
		mutable std::condition_variable mDiscoveryCondition;
		bool mIsConnected{ false };
		bool mIsDiscovered{ false };
		JavaVM* mJavaVm;
		Utilities::EventNotifier<ConnectionState> mConnectionStateNotifier;
		Utilities::EventListener<AndroidGattConnectionState> mConnectionListener;
		Utilities::EventListener<> mServiceDiscoveryListener;
		std::shared_ptr<IBleGattCallback> mGattCallback;
		const Jni::GlobalRef<Jni::JavaObject> mGatt;

		void waitForConnect(std::chrono::seconds);
		void onConnectionStateChanged(AndroidGattConnectionState);
		void onServicesDiscovered();
		void discoverServices() const;
	};
}

#endif // BLE_GATT_H
