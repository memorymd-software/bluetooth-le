#ifndef ANDROID_BLE_SERVICE_H
#define ANDROID_BLE_SERVICE_H

#include "ble_device.h"
#include "jni_object.h"
#include "jni_global_ref.h"
#include "ble_gatt_callback.h"

namespace Neuro::Ble::Impl
{
	class AndroidBleService final : public IBleService
	{
	public:
		AndroidBleService(JNIEnv* env, const Jni::GlobalRef<Jni::JavaObject>& gatt, std::shared_ptr<IBleGattCallback> gatt_callback, const Jni::JavaObject &service);
		
		[[nodiscard]] std::future<std::vector<std::shared_ptr<IBleCharacteristic>>> getCharacteristicsByUuid(const std::vector<UUID>& char_uuids) const override;

	private:
		JavaVM* mJavaVm;
		std::shared_ptr<IBleGattCallback> mGattCallback;
		const Jni::GlobalRef<Jni::JavaObject> mGatt;
		const Jni::GlobalRef<Jni::JavaObject> mService;
	};
}

#endif // ANDROID_BLE_SERVICE_H
