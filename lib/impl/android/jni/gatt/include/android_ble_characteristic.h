#ifndef ANDROID_BLE_CHARACTERISTIC_H
#define ANDROID_BLE_CHARACTERISTIC_H

#include "ble_device.h"
#include "jni_object.h"
#include "jni_global_ref.h"
#include "ble_gatt_callback.h"

namespace Neuro::Ble::Impl
{
	class AndroidBleCharacteristic final : public IBleCharacteristic
	{
	public:
		AndroidBleCharacteristic
		(
			JNIEnv* env, 
			const Jni::GlobalRef<Jni::JavaObject>& gatt, 
			std::shared_ptr<IBleGattCallback> gatt_callback, 
			const Jni::JavaObject& characteristic
		);
		
		[[nodiscard]] CharacteristicProperties properties() const noexcept override;
		[[nodiscard]] std::vector<std::byte> value() const override;
		Utilities::EventNotifier<const std::vector<std::byte>&>& valueChanged() override;
		void write(const std::vector<std::byte>&) override;

	private:
		JavaVM* mJavaVm;
		std::shared_ptr<IBleGattCallback> mGattCallback;
		const Jni::GlobalRef<Jni::JavaObject> mGatt;
		const Jni::GlobalRef<Jni::JavaObject> mCharacteristic;
		Utilities::EventNotifier<const std::vector<std::byte>&> mCharChangedNotifier;
		Utilities::EventListener<const UUID&, std::vector<std::byte>> mCharChangedListener;

		[[nodiscard]] std::vector<std::byte> cachedValue() const;
	};
}

#endif // ANDROID_BLE_CHARACTERISTIC_H
