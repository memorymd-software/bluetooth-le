#include "java_ble_scanner.h"
#include "jni_string.h"

namespace Neuro::Ble::Impl
{
	static Jni::GlobalRef<Jni::JavaObject> obtain_le_scanner(JNIEnv* env, const Jni::JavaObject& adapter_object)
	{
		try
		{
			Jni::JavaObject leScanner{ adapter_object.callMethod<jobject>(env, "getBluetoothLeScanner", "()Landroid/bluetooth/le/BluetoothLeScanner;") };
			return Jni::GlobalRef<Jni::JavaObject>{ env, leScanner };
		}
		catch (std::exception & e)
		{
			throw std::runtime_error(std::string("Cannot get bluetooth le scanner: ") + e.what());
		}
		catch (...)
		{
			throw std::runtime_error("Cannot get bluetooth le scanner.");
		}
	}
	
	static Jni::JavaObject get_default_settings(JNIEnv* env)
	{
		try
		{
			Jni::JavaClass settingsBuilderClass(env, "android/bluetooth/le/ScanSettings$Builder");
			Jni::JavaObject settingsBuilder(env, settingsBuilderClass, "()V");
			settingsBuilder = Jni::JavaObject(settingsBuilder.callMethod<jobject>(env, "setReportDelay", "(J)Landroid/bluetooth/le/ScanSettings$Builder;", static_cast<jlong>(0)));

			Jni::JavaClass settingsClass(env, "android/bluetooth/le/ScanSettings");
			auto balancedScanModeValue = settingsClass.getStaticFieldValue<jint>(env, "SCAN_MODE_BALANCED");
			settingsBuilder = Jni::JavaObject(settingsBuilder.callMethod<jobject>(env, "setScanMode", "(I)Landroid/bluetooth/le/ScanSettings$Builder;", balancedScanModeValue));

			return Jni::JavaObject{ settingsBuilder.callMethod<jobject>(env, "build", "()Landroid/bluetooth/le/ScanSettings;") };
		}
		catch (std::exception & e)
		{
			throw std::runtime_error(std::string("Cannot create default scan settings: ") + e.what());
		}
		catch (...)
		{
			throw std::runtime_error("Cannot create default scan settings.");
		}
	}
	
	static Jni::JavaObject build_java_filter(JNIEnv* env, const ScanFilter& filter)
	{
		try
		{
			Jni::JavaClass filterBuilderClass(env, "android/bluetooth/le/ScanFilter$Builder");
			Jni::JavaObject filterBuilder(env, filterBuilderClass, "()V");
			if (filter.DeviceId.has_value())
			{
				Jni::JavaString deviceAddressString{ env, to_string(filter.DeviceId.value().Address) };
				filterBuilder = Jni::JavaObject(filterBuilder.callMethod<jobject>(env, "setDeviceAddress", "(Ljava/lang/String;)Landroid/bluetooth/le/ScanFilter$Builder;", static_cast<jstring>(deviceAddressString)));
			}
			if (filter.DeviceName.has_value())
			{
				Jni::JavaString deviceNameString{ env, filter.DeviceName.value() };
				filterBuilder = Jni::JavaObject(filterBuilder.callMethod<jobject>(env, "setDeviceName", "(Ljava/lang/String;)Landroid/bluetooth/le/ScanFilter$Builder;", static_cast<jstring>(deviceNameString)));
			}
			if (filter.MainServiceUuid.has_value())
			{
				Jni::JavaClass uuidClass(env, "java/util/UUID");
				Jni::JavaString uuidJavaString(env, to_string(filter.MainServiceUuid.value()));
				auto javaUuid = uuidClass.callStaticMethod<jobject>(env, "fromString", "(Ljava/lang/String;)Ljava/util/UUID;", static_cast<jstring>(uuidJavaString));
				Jni::JavaClass parcelUUIDClass(env, "android/os/ParcelUuid");
				Jni::JavaObject parcelUUID(env, parcelUUIDClass, "(Ljava/util/UUID;)V", javaUuid);
				filterBuilder = Jni::JavaObject(filterBuilder.callMethod<jobject>(env, "setServiceUuid", "(Landroid/os/ParcelUuid;)Landroid/bluetooth/le/ScanFilter$Builder;", static_cast<jobject>(parcelUUID)));
			}

			return Jni::JavaObject{ filterBuilder.callMethod<jobject>(env, "build", "()Landroid/bluetooth/le/ScanFilter;") };
		}
		catch (std::exception & e)
		{
			throw std::runtime_error(std::string("Cannot convert all filters to java representation: ") + e.what());
		}
		catch (...)
		{
			throw std::runtime_error("Cannot convert all filters to java representation.");
		}
	}
	
	static Jni::JavaObject convert_to_java_filters(JNIEnv* env, const std::vector<ScanFilter>& filters)
	{
		try
		{
			Jni::JavaClass arrayListClass(env, "java/util/ArrayList");
			Jni::JavaObject arrayList(env, arrayListClass, "()V");
			for (const auto &filter : filters)
			{
				arrayList.callMethod<jboolean>(env, "add", "(Ljava/lang/Object;)Z", static_cast<jobject>(build_java_filter(env, filter)));
			}

			return arrayList;
		}
		catch (std::exception & e)
		{
			throw std::runtime_error(std::string("Cannot convert all filters to java representation: ") + e.what());
		}
		catch (...)
		{
			throw std::runtime_error("Cannot convert all filters to java representation.");
		}
	}
	
	JavaBleScanner::JavaBleScanner(JNIEnv* env, const Jni::JavaObject& adapter_object):
		mScannerObject(obtain_le_scanner(env, adapter_object))
	{}
	
	void JavaBleScanner::startScan(JNIEnv* env, const JavaScanCallback& scan_callback)
	{
		try
		{
			mScannerObject->callMethod<void>(env, "startScan", "(Landroid/bluetooth/le/ScanCallback;)V", static_cast<jobject>(scan_callback.javaObject()));
		}
		catch (std::exception & e)
		{
			throw std::runtime_error(std::string("Failed to start scan: ") + e.what());
		}
		catch (...)
		{
			throw std::runtime_error("Failed to start scan.");
		}
	}
		
	void JavaBleScanner::startScan(JNIEnv* env, const JavaScanCallback& scan_callback, const std::vector<ScanFilter>& filter)
	{
		try
		{
			auto scanFilters = convert_to_java_filters(env, filter);
			mScannerObject->callMethod<void>
			(
				env, 
				"startScan", 
				"(Ljava/util/List;Landroid/bluetooth/le/ScanSettings;Landroid/bluetooth/le/ScanCallback;)V",
				static_cast<jobject>(convert_to_java_filters(env, filter)),
				static_cast<jobject>(get_default_settings(env)),
				static_cast<jobject>(scan_callback.javaObject())
			);
		}
		catch (std::exception & e)
		{
			throw std::runtime_error(std::string("Failed to start scan: ") + e.what());
		}
		catch (...)
		{
			throw std::runtime_error("Failed to start scan.");
		}
	}
	
	void JavaBleScanner::stopScan(JNIEnv* env, const JavaScanCallback& scan_callback)
	{
		try
		{
			mScannerObject->callMethod<void>(env, "stopScan", "(Landroid/bluetooth/le/ScanCallback;)V", static_cast<jobject>(scan_callback.javaObject()));
		}
		catch (std::exception & e)
		{
			throw std::runtime_error(std::string("Failed to start scan: ") + e.what());
		}
		catch (...)
		{
			throw std::runtime_error("Failed to start scan.");
		}
	}
}
