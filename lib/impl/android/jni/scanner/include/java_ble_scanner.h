#ifndef JAVA_BLE_SCANNER_H
#define JAVA_BLE_SCANNER_H

#include "ble_scan_callback.h"
#include "scan_filter.h"
#include "jni_object.h"
#include "jni_global_ref.h"
#include <memory>
#include <vector>

namespace Neuro::Ble::Impl
{
	class IJavaBleScanner
	{
	public:
		virtual ~IJavaBleScanner() = default;

		virtual void startScan(JNIEnv* env, const JavaScanCallback& scan_callback) = 0;
		virtual void startScan(JNIEnv* env, const JavaScanCallback& scan_callback, const std::vector<ScanFilter>& filter) = 0;
		virtual void stopScan(JNIEnv* env, const JavaScanCallback& scan_callback) = 0;
	};
	
	class JavaBleScanner final : public IJavaBleScanner
	{
	public:
		JavaBleScanner(JNIEnv* env, const Jni::JavaObject &adapter_object);

		void startScan(JNIEnv* env, const JavaScanCallback& scan_callback) override;
		void startScan(JNIEnv* env, const JavaScanCallback& scan_callback, const std::vector<ScanFilter>& filter) override;
		void stopScan(JNIEnv* env, const JavaScanCallback& scan_callback) override;

	private:
		const Jni::GlobalRef<Jni::JavaObject> mScannerObject;
	};
}

#endif //JAVA_BLE_SCANNER_H
