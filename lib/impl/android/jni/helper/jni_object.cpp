#include "jni_object.h"
#include "jni_exceptions.h"

namespace Neuro::Ble::Impl::Jni
{
	jmethodID get_method_id(JNIEnv* env, jclass java_class, const char* method_name, const char* signature)
	{
		return call_and_handle_java_exceptions(env, &JNIEnv::GetMethodID, java_class, method_name, signature);
	}

	jclass get_object_class(JNIEnv* env, jobject object)
	{
		if (object == nullptr)
		{
			throw std::invalid_argument("Object cannot be null");
		}

		return call_and_handle_java_exceptions(env, &JNIEnv::GetObjectClass, object);
	}
	jobject create_java_object_var(JNIEnv* env, jclass java_class, jmethodID constructor_id, ...)
	{
		va_list args;
		va_start(args, constructor_id);
		auto result = call_and_handle_java_exceptions(env, &JNIEnv::NewObjectV, java_class, constructor_id, args);
        va_end(args);
        return result;
	}
}