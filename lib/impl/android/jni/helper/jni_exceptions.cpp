#include "jni_exceptions.h"

namespace Neuro::Ble::Impl::Jni
{
	bool is_exception_pending(JNIEnv* env)
	{
		return env->ExceptionCheck() == JNI_TRUE;
	}

	static std::string get_exception_class_name(JNIEnv* env, jclass exception_class) {
		jclass exception_classClass = env->GetObjectClass(exception_class);
		if (exception_classClass == NULL)
		{
			if (is_exception_pending(env))
			{
				env->ExceptionClear();
			}
			return "UnknownException";
		}

		auto toStringID = env->GetMethodID(exception_classClass, "getName", "()Ljava/lang/String;");
		if (toStringID == NULL)
		{
			if (is_exception_pending(env))
			{
				env->ExceptionClear();
			}
			return "UnknownException";
		}

		auto strObj = static_cast<jstring>(env->CallObjectMethod(exception_class, toStringID));
		if (strObj == NULL)
		{
			if (is_exception_pending(env))
			{
				env->ExceptionClear();
			}
			return "UnknownException";
		}

		const char* classNameChars = env->GetStringUTFChars(strObj, NULL);
		if (classNameChars == NULL)
		{
			if (is_exception_pending(env))
			{
				env->ExceptionClear();
			}
			return "UnknownException";
		}

		std::string res(classNameChars);
		env->ReleaseStringUTFChars(strObj, classNameChars);

		return res;
	}

	std::string extract_java_exception_message(JNIEnv* env, jthrowable java_exception)
	{
		auto exceptionClass = env->GetObjectClass(java_exception);
		if (exceptionClass == NULL)
		{
			if (is_exception_pending(env))
			{
				env->ExceptionClear();
			}
			return "Corrupted java exception object: unable to get exception class";
		}

		auto getMessageID = env->GetMethodID(exceptionClass, "getMessage", "()Ljava/lang/String;");
		if (getMessageID == NULL)
		{
			if (is_exception_pending(env))
			{
				env->ExceptionClear();
			}
			return get_exception_class_name(env, exceptionClass) + "(Java). No message method.";
		}

		auto exceptionMessage = static_cast<jstring>(env->CallObjectMethod(java_exception, getMessageID));
		if (exceptionMessage == NULL)
		{
			if (is_exception_pending(env))
			{
				env->ExceptionClear();
			}
			return get_exception_class_name(env, exceptionClass) + "(Java). No message.";
		}

		const char* exceptionMessageChars = env->GetStringUTFChars(exceptionMessage, NULL);
		if (exceptionMessageChars == NULL)
		{
			if (is_exception_pending(env))
			{
				env->ExceptionClear();
			}
			return "Corrupted java exception message: unable to parse convert message string";
		}

		std::string exceptionMessageString(exceptionMessageChars);
		env->ReleaseStringUTFChars(exceptionMessage, exceptionMessageChars);

		return get_exception_class_name(env, exceptionClass) + "(Java): " + exceptionMessageString;
	}
}