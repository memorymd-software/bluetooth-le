#include "jni_string.h"
#include <exception>

namespace Neuro::Ble::Impl::Jni
{
	static jstring create_java_string(JNIEnv* env, const char* string)
	{
		if (string == nullptr)
		{
			throw std::invalid_argument("Source string cannot be null");
		}
		
		try
		{
			return call_and_handle_java_exceptions(env, &JNIEnv::NewStringUTF, string);
		}
		catch (std::exception &e)
		{
			throw std::runtime_error(std::string("Unable to create a java string: ") + e.what());
		}
		catch (...)
		{
			throw std::runtime_error("Unable to create a java string.");
		}
	}
	
	JavaString::JavaString(JNIEnv* env, const char* string):
		mJavaString(create_java_string(env, string))
	{}
	
	std::string JavaString::toStdString(JNIEnv *env) const
	{
		try
		{
			if (mJavaString == nullptr)
			{
				throw std::runtime_error("The source string is null.");
			}
			
			auto stringChars = Jni::call_and_handle_java_exceptions(env, &JNIEnv::GetStringUTFChars, mJavaString, static_cast<jboolean*>(nullptr));
			if (stringChars == nullptr)
			{
				throw std::runtime_error("The result string is null.");
			}
			
			std::string stdString(stringChars);
			Jni::call_and_handle_java_exceptions(env, &JNIEnv::ReleaseStringUTFChars, mJavaString, stringChars);
			return stdString;
		}
		catch (std::exception &e)
		{
			throw std::runtime_error(std::string("Cannot convert java string to std::string: ") + e.what());
		}
		catch (...)
		{
			throw std::runtime_error("Cannot convert java string to std::string.");
		}
	}
}