#include "jni_class.h"

namespace Neuro::Ble::Impl::Jni
{
	jmethodID get_static_method_id(JNIEnv* env, jclass java_class, const char* method_name, const char* signature)
	{
		return call_and_handle_java_exceptions(env, &JNIEnv::GetStaticMethodID, java_class, method_name, signature);
	}

	template <>
	jint get_static_field_value<jint>(JNIEnv* env, jclass class_object, jfieldID field_id)
	{
		return call_and_handle_java_exceptions(env, &JNIEnv::GetStaticIntField, class_object, field_id);
	}

	template <>
	jlong get_static_field_value<jlong>(JNIEnv* env, jclass class_object, jfieldID field_id)
	{
		return call_and_handle_java_exceptions(env, &JNIEnv::GetStaticLongField, class_object, field_id);
	}

	template <>
	jstring get_static_field_value<jstring>(JNIEnv* env, jclass class_object, jfieldID field_id)
	{
		return static_cast<jstring>(call_and_handle_java_exceptions(env, &JNIEnv::GetStaticObjectField, class_object, field_id));
	}

	template <>
	jobject get_static_field_value<jobject>(JNIEnv* env, jclass class_object, jfieldID field_id)
	{
		return call_and_handle_java_exceptions(env, &JNIEnv::GetStaticObjectField, class_object, field_id);
	}

	jobject call_helper(JNIEnv* env, jobject object, jmethodID method_id, ...)
	{
		va_list args;
		va_start(args, method_id);
		auto result = call_and_handle_java_exceptions(env, &JNIEnv::CallObjectMethodV, object, method_id, args);
		va_end(args);
		return jobject(result);
	}

	jobject static_call_helper(JNIEnv* env, jclass java_class, jmethodID method_id, ...)
	{
		va_list args;
		va_start(args, method_id);
		auto result = call_and_handle_java_exceptions(env, &JNIEnv::CallStaticObjectMethodV, java_class, method_id, args);
		va_end(args);
		return jobject(result);
	}

	jclass find_java_class(JNIEnv* env, const char* java_class_path)
	{
		try
		{
			return call_and_handle_java_exceptions(env, &JNIEnv::FindClass, java_class_path);
		}
		catch (std::exception &e)
		{
			throw std::runtime_error(std::string("Unable to find java class ") + java_class_path + ": " + e.what());
		}
		catch (...)
		{
			throw std::runtime_error("Unable to find java class");
		}
	}
}