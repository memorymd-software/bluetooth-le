#ifndef JNI_STRING_H
#define JNI_STRING_H

#include "jni_object.h"
#include <string>

namespace Neuro::Ble::Impl::Jni
{
	struct JavaString final
	{
		explicit JavaString(jstring java_string) noexcept :
			mJavaString(java_string)
		{}

		JavaString(JNIEnv* env, const char* string);
		
		JavaString(JNIEnv *env, const std::string &string):
			JavaString(env, string.c_str())
		{}
		
		operator jstring() const noexcept
		{
			return mJavaString;
		}

		std::string toStdString(JNIEnv* env) const;
		
	private:
		jstring mJavaString;
	};
}

#endif