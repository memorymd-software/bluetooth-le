#ifndef JNI_BUILTIN_TYPES_H
#define JNI_BUILTIN_TYPES_H

#include "jni.h"

namespace Neuro::Ble::Impl::Jni
{
	template <typename T>
	struct TypeSignature;

	template <>
	struct TypeSignature<jboolean>
	{
		static constexpr const char* value = "Z";
	};

	template <>
	struct TypeSignature<jbyte>
	{
		static constexpr const char* value = "B";
	};

	template <>
	struct TypeSignature<jchar>
	{
		static constexpr const char* value = "C";
	};

	template <>
	struct TypeSignature<jshort>
	{
		static constexpr const char* value = "S";
	};

	template <>
	struct TypeSignature<jint>
	{
		static constexpr const char* value = "I";
	};

	template <>
	struct TypeSignature<jlong>
	{
		static constexpr const char* value = "J";
	};

	template <>
	struct TypeSignature<jfloat>
	{
		static constexpr const char* value = "F";
	};

	template <>
	struct TypeSignature<jdouble>
	{
		static constexpr const char* value = "D";
	};

	template <>
	struct TypeSignature<jstring>
	{
		static constexpr const char* value = "Ljava/lang/String;";
	};

	template <typename T>
	inline constexpr const char* TypeSignatureV = TypeSignature<T>::value;
}

#endif //JNI_BUILTIN_TYPES_H