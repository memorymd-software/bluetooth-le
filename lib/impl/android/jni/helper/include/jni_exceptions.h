#ifndef JNI_EXCEPTIONS_H
#define JNI_EXCEPTIONS_H

#include "jni.h"
#include <exception>
#include <string>
#include <utility>

namespace Neuro::Ble::Impl::Jni
{
	bool is_exception_pending(JNIEnv *env);

	std::string extract_java_exception_message(JNIEnv* env, jthrowable java_exception);

	struct JavaException final : public std::exception
	{
		explicit JavaException(JNIEnv* env, jthrowable java_exception) :
			mWhat(extract_java_exception_message(env, java_exception))
		{}

		const char * what() const noexcept override
		{
			return mWhat.c_str();
		}

	private:
		std::string mWhat;
	};

	template <typename Ret, typename... Args>
	std::enable_if_t<std::is_same_v<Ret, void>, Ret> call_and_handle_java_exceptions(JNIEnv* env, Ret(JNIEnv::* function)(std::remove_const_t<std::remove_reference_t<Args>>...), Args&&... args)
	{
		if (env == nullptr)
		{
			throw std::invalid_argument("JNI Environment pointer cannot be null.");
		}

		(env->*function)(std::forward<Args>(args)...);
		if (!is_exception_pending(env))
		{
			return;
		}

		auto exception = env->ExceptionOccurred();
		env->ExceptionClear();
		throw JavaException(env, exception);
	}

	template <typename Ret, typename... Args>
	std::enable_if_t<!std::is_same_v<Ret, void>, Ret> call_and_handle_java_exceptions(JNIEnv* env, Ret(JNIEnv::*function)(std::remove_reference_t<Args>...), Args&&... args)
	{
		if (env == nullptr)
		{
			throw std::invalid_argument("JNI Environment pointer cannot be null.");
		}

		auto result = (env->*function)(std::forward<Args>(args)...);
		if (!is_exception_pending(env))
		{
			return result;
		}

		auto exception = env->ExceptionOccurred();
		env->ExceptionClear();
		throw JavaException(env, exception);
	}
}

#endif //JNI_EXCEPTIONS_H
 