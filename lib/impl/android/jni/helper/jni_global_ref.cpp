#include "jni_global_ref.h"
#include "jni_exceptions.h"
#include <string>

namespace Neuro::Ble::Impl::Jni
{
	JavaVM* get_java_vm(JNIEnv* env)
	{
		try
		{
			JavaVM* jvm;
			if (auto resultCode = call_and_handle_java_exceptions(env, &JNIEnv::GetJavaVM, &jvm); resultCode != 0)
				throw std::runtime_error("GetJavaVM method returned error code " + std::to_string(resultCode));
			
			return jvm;
		}
		catch (std::exception & e)
		{
			throw std::runtime_error(std::string("Cannot create get JVM instance: ") + e.what());
		}
		catch (...)
		{
			throw std::runtime_error("Cannot create get JVM instance.");
		}
	}
}