#ifndef JAVA_SCAN_CALLBACK_H
#define JAVA_SCAN_CALLBACK_H

#include "cxx_scan_callback.h"
#include "jni_object.h"
#include "jni_global_ref.h"

namespace Neuro::Ble::Impl
{

	/*
	 *Java class description
	 *
	 public class com.memorymd.bluetoothle.BleScanCallback extends android.bluetooth.le.ScanCallback{
	   public com.memorymd.bluetoothle.BleScanCallback(long);
		 descriptor: (J)V

	   public void onBatchScanResults(java.util.List<android.bluetooth.le.ScanResult>);
		 descriptor: (Ljava / util / List;)V

	   public void onScanFailed(int);
		 descriptor: (I)V

	   public void onScanResult(int, android.bluetooth.le.ScanResult);
		 descriptor: (ILandroid / bluetooth / le / ScanResult;)V
	 }
	 */
	class JavaScanCallback
	{
	public:
		JavaScanCallback(JNIEnv* env, const std::weak_ptr<CXXScanCallback>& cxx_callback, const char *callback_classpath);

		Jni::JavaObject& javaObject() const noexcept
		{
			return *mCallbackObject;
		}

	private:
		const Jni::GlobalRef<Jni::JavaObject> mCallbackObject;
	};
}

#endif //JAVA_SCAN_CALLBACK_H