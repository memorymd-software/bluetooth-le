#ifndef CXX_SCAN_CALLBACK_H
#define CXX_SCAN_CALLBACK_H

#include "advertisement_data.h"
#include "event_notifier.h"
#include "jni_array.h"
#include "jni_object.h"
#include <vector>

namespace Neuro::Ble::Impl
{
	enum class BleScanError : jint
	{
		ALREADY_STARTED = 1,
		APPLICATION_REGISTRATION_FAILED = 2,
		FEATURE_UNSUPPORTED = 4,
		INTERNAL_ERROR = 3
	};

	class CXXScanCallback
	{
	public:		
		void onScanResult(JNIEnv *env, jint callback_type, const Jni::JavaObject& scanResult) noexcept;
		void onScanFailed(JNIEnv *env, jint error_code) noexcept;
		void onBatchScanResults(JNIEnv *env, const Jni::JavaArray<jobject>& scan_result_array) noexcept;

		Utilities::EventNotifier<const std::vector<AdvertisementData>&>& advertisementsReceived() noexcept
		{
			return mAdvertisementsReceivedNotifier;
		}

		Utilities::EventNotifier<BleScanError>& scanErrorOccured() noexcept
		{
			return mScanErrorOccuredNotifier;
		}

	private:
		Utilities::EventNotifier<const std::vector<AdvertisementData> &> mAdvertisementsReceivedNotifier;
		Utilities::EventNotifier<BleScanError> mScanErrorOccuredNotifier;
	};
}

#endif //CXX_SCAN_CALLBACK_H