#ifndef BLE_SCAN_CALLBACK_H
#define BLE_SCAN_CALLBACK_H

#include "cxx_scan_callback.h"
#include "java_scan_callback.h"

namespace Neuro::Ble::Impl
{
	class IBleScanCallback
	{
	public:
		virtual ~IBleScanCallback() = default;
		
		virtual Utilities::EventNotifier<const std::vector<AdvertisementData>&>& advertisementsReceived() noexcept = 0;
		virtual Utilities::EventNotifier<BleScanError>& scanErrorOccured() noexcept = 0;
		virtual const JavaScanCallback& javaCallback() const noexcept = 0;
	};
	
	class BleScanCallback final : public IBleScanCallback
	{
	public:
		explicit BleScanCallback(JNIEnv *env, const char* callback_classpath = "com/memorymd/bluetoothle/ScanCallbackAdapter");

		Utilities::EventNotifier<const std::vector<AdvertisementData>&>& advertisementsReceived() noexcept
		{
			return mCxxScanCallback->advertisementsReceived();
		}

		Utilities::EventNotifier<BleScanError>& scanErrorOccured() noexcept
		{
			return mCxxScanCallback->scanErrorOccured();
		}

		const JavaScanCallback& javaCallback() const noexcept
		{
			return mJavaScanCallback;
		}

	private:
		//Order is crucial!
		std::shared_ptr<CXXScanCallback> mCxxScanCallback;
		JavaScanCallback mJavaScanCallback;
	};
}

#endif //BLE_SCAN_CALLBACK_H
