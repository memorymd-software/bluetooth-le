#include "cxx_scan_callback.h"
#include "jni_array.h"
#include "jni_exceptions.h"
#include "jni_string.h"
#include "android/log.h"
#include <unordered_map>
#include <vector>

namespace Neuro::Ble::Impl
{
	static bool is_match_lost(JNIEnv* env, jint callback_type)
	{
		auto settingsClass = Jni::JavaClass(env, "android/bluetooth/le/ScanSettings");
		auto matchLostFieldValue = settingsClass.getStaticFieldValue<jint>(env, "CALLBACK_TYPE_MATCH_LOST");
		return callback_type == matchLostFieldValue;
	}

	static std::string obtain_address_string_from_scan_result(JNIEnv* env, const Jni::JavaObject& scan_result)
	{
		try
		{
			Jni::JavaObject bluetoothDevice{ scan_result.callMethod<jobject>(env, "getDevice", "()Lcom/memorymd/bluetoothle/IBluetoothDevice;") };
			Jni::JavaString addressJavaString(static_cast<jstring>(bluetoothDevice.callMethod<jobject>(env, "getAddress", "()Ljava/lang/String;")));
			return addressJavaString.toStdString(env);
		}
		catch (std::exception & e)
		{
			throw std::runtime_error(std::string("Unable to get address string from ScanResult: ") + e.what());
		}
		catch (...)
		{
			throw std::runtime_error("Unable to get address string from ScanResult.");
		}
	}

	static auto obtain_manufacturer_data_from_scan_result(JNIEnv* env, const Jni::JavaObject& scan_result)
	{
		try
		{
			std::unordered_map<int, std::vector<std::byte>> manufacturerData;
			Jni::JavaObject scanRecord{ scan_result.callMethod<jobject>(env, "getScanRecord", "()Lcom/memorymd/bluetoothle/IScanRecord;") };
			Jni::JavaObject manufacturerDataSparseArray{ scanRecord.callMethod<jobject>(env, "getManufacturerSpecificData", "()Landroid/util/SparseArray;") };
			jint manuDataArraySize = manufacturerDataSparseArray.callMethod<jint>(env, "size", "()I");
			for (jint i = 0; i < manuDataArraySize; ++i)
			{
				auto manufacturerID = manufacturerDataSparseArray.callMethod<jint>(env, "keyAt", "(I)I", i);
				Jni::JavaArray<jbyte> dataArray(static_cast<jbyteArray>(manufacturerDataSparseArray.callMethod<jobject>(env, "valueAt", "(I)Ljava/lang/Object;", i)));
				auto jbyteVector = dataArray.toVector(env);
				std::vector<std::byte> manuDataVector(jbyteVector.size());
				for (size_t j = 0; j < jbyteVector.size(); ++j)
				{
					manuDataVector[j] = static_cast<std::byte>(jbyteVector[j]);
				}
				manufacturerData[manufacturerID] = manuDataVector;
			}
			return manufacturerData;
		}
		catch (std::exception & e)
		{
			throw std::runtime_error(std::string("Unable to get manufacturer data from ScanResult: ") + e.what());
		}
		catch (...)
		{
			throw std::runtime_error("Unable to get manufacturer data from ScanResult.");
		}
	}

	static auto obtain_device_name_from_scan_result(JNIEnv* env, const Jni::JavaObject& scan_result)
	{
		try
		{
			Jni::JavaObject scanRecord{ scan_result.callMethod<jobject>(env, "getScanRecord", "()Lcom/memorymd/bluetoothle/IScanRecord;") };
			Jni::JavaString nameJavaString(static_cast<jstring>(scanRecord.callMethod<jobject>(env, "getDeviceName", "()Ljava/lang/String;")));
			return nameJavaString.toStdString(env);
		}
		catch (std::exception & e)
		{
			throw std::runtime_error(std::string("Unable to get device name from ScanResult: ") + e.what());
		}
		catch (...)
		{
			throw std::runtime_error("Unable to get device name from ScanResult.");
		}
	}

	static auto obtain_rssi_from_scan_result(JNIEnv* env, const Jni::JavaObject& scan_result)
	{
		try
		{
			return scan_result.callMethod<jint>(env, "getRssi", "()I");
		}
		catch (std::exception & e)
		{
			throw std::runtime_error(std::string("Unable to get RSSI from ScanResult: ") + e.what());
		}
		catch (...)
		{
			throw std::runtime_error("Unable to get RSSI from ScanResult.");
		}
	}

	static auto obtain_service_uuids_from_scan_result(JNIEnv* env, const Jni::JavaObject& scan_result)
	{
		try
		{
			std::vector<UUID> uuidsVector;
			Jni::JavaObject scanRecord{ scan_result.callMethod<jobject>(env, "getScanRecord", "()Lcom/memorymd/bluetoothle/IScanRecord;") };
			auto uuidListObject = scanRecord.callMethod<jobject>(env, "getServiceUuids", "()Ljava/util/List;");
			if (uuidListObject == nullptr)
			{
				return uuidsVector;
			}

			Jni::JavaObject uuidsList{ uuidListObject };
			auto uuidsListSize = uuidsList.callMethod<jint>(env, "size", "()I");

			uuidsVector.reserve(uuidsListSize);
			for (jint i = 0; i < uuidsListSize; ++i)
			{
				Jni::JavaObject parcelUuid(uuidsList.callMethod<jobject>(env, "get", "(I)Ljava/lang/Object;", i));
				Jni::JavaObject javaUuid(parcelUuid.callMethod<jobject>(env, "getUuid", "()Ljava/util/UUID;"));
				Jni::JavaString uuidJavaString(static_cast<jstring>(javaUuid.callMethod<jobject>(env, "toString", "()Ljava/lang/String;")));
				uuidsVector.emplace_back(uuidJavaString.toStdString(env));
			}

			return uuidsVector;
		}
		catch (std::exception & e)
		{
			throw std::runtime_error(std::string("Unable to get services UUIDs from ScanResult: ") + e.what());
		}
		catch (...)
		{
			throw std::runtime_error("Unable to get services UUIDs from ScanResult.");
		}
	}

	static auto obtain_timestamp_from_scan_result(JNIEnv* env, const Jni::JavaObject& scan_result)
	{
		try
		{
			return std::chrono::steady_clock::now();
		}
		catch (std::exception & e)
		{
			throw std::runtime_error(std::string("Unable to get timestamp from ScanResult: ") + e.what());
		}
		catch (...)
		{
			throw std::runtime_error("Unable to get timestamp from ScanResult.");
		}
	}

	static AdvertisementData advertisement_data_from_scan_result(JNIEnv* env, const Jni::JavaObject& scan_result)
	{
		try
		{
			AdvertisementData advertisementData
			{
				obtain_device_name_from_scan_result(env, scan_result),
				DeviceIdentifier{ BleDeviceAddress{ obtain_address_string_from_scan_result(env, scan_result) } },
				obtain_rssi_from_scan_result(env, scan_result),
				obtain_service_uuids_from_scan_result(env, scan_result),
				obtain_timestamp_from_scan_result(env, scan_result),
				obtain_manufacturer_data_from_scan_result(env, scan_result)
			};

			return advertisementData;
		}
		catch (std::exception & e)
		{
			throw std::runtime_error(std::string("Unable to get advertisement data from ScanResult: ") + e.what());
		}
		catch (...)
		{
			throw std::runtime_error("Unable to get advertisement data from ScanResult.");
		}
	}

	static BleScanError scanErrorFromCode(JNIEnv* env, jint error_code)
	{
		try
		{
			Jni::JavaClass scan_callback_class(env, "android/bluetooth/le/ScanCallback");
			std::unordered_map<jint, BleScanError> scanErrorMap
			{
				{ scan_callback_class.getStaticFieldValue<jint>(env, "SCAN_FAILED_ALREADY_STARTED"), BleScanError::ALREADY_STARTED },
				{ scan_callback_class.getStaticFieldValue<jint>(env, "SCAN_FAILED_APPLICATION_REGISTRATION_FAILED"), BleScanError::APPLICATION_REGISTRATION_FAILED },
				{ scan_callback_class.getStaticFieldValue<jint>(env, "SCAN_FAILED_FEATURE_UNSUPPORTED"), BleScanError::FEATURE_UNSUPPORTED },
				{ scan_callback_class.getStaticFieldValue<jint>(env, "SCAN_FAILED_INTERNAL_ERROR"), BleScanError::INTERNAL_ERROR }
			};
			return scanErrorMap.at(error_code);
		}
		catch (std::exception & e)
		{
			throw std::invalid_argument("Unable to get BleScanError value from error code {" + std::to_string(error_code) + "}: " + e.what());
		}
		catch (...)
		{
			throw std::invalid_argument("Unable to get BleScanError value from error code {" + std::to_string(error_code) + "}");
		}
	}


	void CXXScanCallback::onScanResult(JNIEnv* env, jint callback_type, const Jni::JavaObject& scan_result) noexcept
	{
		try
		{
			if (is_match_lost(env, callback_type))
				return;

			auto advertisementData = advertisement_data_from_scan_result(env, scan_result);
			mAdvertisementsReceivedNotifier.notify({ advertisementData });
		}
		catch (std::exception & e)
		{
			__android_log_print(ANDROID_LOG_ERROR, "CXXScanCallback", "Exception thrown during onScanResult handling: %s", e.what());
		}
		catch (...)
		{
			__android_log_print(ANDROID_LOG_ERROR, "CXXScanCallback", "Exception thrown during onScanResult handling");
		}
	}

	void CXXScanCallback::onScanFailed(JNIEnv* env, jint error_code) noexcept
	{
		try
		{
			auto scanError = scanErrorFromCode(env, error_code);
			mScanErrorOccuredNotifier.notify(scanError);
		}
		catch (std::exception & e)
		{
			__android_log_print(ANDROID_LOG_ERROR, "CXXScanCallback", "Exception thrown during onScanFailed handling: %s", e.what());
		}
		catch (...)
		{
			__android_log_print(ANDROID_LOG_ERROR, "CXXScanCallback", "Exception thrown during onScanFailed handling");
		}
	}

	void CXXScanCallback::onBatchScanResults(JNIEnv* env, const Jni::JavaArray<jobject>& scan_result_array) noexcept
	{
		try
		{
			const auto advertisementSize = scan_result_array.size(env);
			std::vector<AdvertisementData> advertisements;
			advertisements.reserve(advertisementSize);
			for (size_t i = 0; i < advertisementSize; ++i)
			{
				auto scanResult = Jni::JavaObject(scan_result_array.at(env, i));
				advertisements.push_back(advertisement_data_from_scan_result(env, scanResult));
			}
			mAdvertisementsReceivedNotifier.notify(advertisements);
		}
		catch (std::exception & e)
		{
			__android_log_print(ANDROID_LOG_ERROR, "CXXScanCallback", "Exception thrown during onBatchScanResults handling: %s", e.what());
		}
		catch (...)
		{
			__android_log_print(ANDROID_LOG_ERROR, "CXXScanCallback", "Exception thrown during onBatchScanResults handling");
		}
	}
}
