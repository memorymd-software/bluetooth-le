#include "cxx_scan_callback.h"
#include <memory>
#include <vector>

using Neuro::Ble::Impl::CXXScanCallback;
using Neuro::Ble::Impl::Jni::JavaClass;
using Neuro::Ble::Impl::Jni::JavaObject;
using Neuro::Ble::Impl::Jni::JavaArray;

/*
 *JNI function mandatory name template
 *
JNIEXPORT <RETURN_TYPE> JNICALL Java_<PACKAGE_NAME>_<JAVA_CLASS>_<METHOD_NAME>(
		JNIEnv *env, jobject obj, <METHOD_PARAMETERS>...) {
	...
}
 */

extern "C"
{
	JNIEXPORT void JNICALL Java_com_memorymd_bluetoothle_BleScanCallback_destroyNativeCallbackLink
	(
		JNIEnv* env,
		jclass class_obj,
		jlong native_callback_ptr
	)
	{
		using Neuro::Ble::Impl::CXXScanCallback;
		auto scannerCallbackWeakPtrPtr = reinterpret_cast<std::weak_ptr<CXXScanCallback>*>(native_callback_ptr);
		delete scannerCallbackWeakPtrPtr;
	}

	JNIEXPORT void JNICALL Java_com_memorymd_bluetoothle_BleScanCallback_onScanResult
	(
		JNIEnv* env,
		jclass class_obj,
		jlong native_callback_ptr,
		jint callback_type,
		jobject scan_result
	)
	{
		auto scannerCallbackWeakPtr = *reinterpret_cast<std::weak_ptr<CXXScanCallback>*>(native_callback_ptr);
		auto scannerCallbackPtr = scannerCallbackWeakPtr.lock();
		if (scannerCallbackPtr == nullptr)
			return;

		scannerCallbackPtr->onScanResult(env, callback_type, JavaObject(scan_result));
	}

	JNIEXPORT void JNICALL Java_com_memorymd_bluetoothle_BleScanCallback_onScanFailed
	(
		JNIEnv* env,
		jclass class_obj,
		jlong native_callback_ptr,
		jint error_code
	)
	{
		auto scannerCallbackWeakPtr = *reinterpret_cast<std::weak_ptr<CXXScanCallback>*>(native_callback_ptr);
		auto scannerCallbackPtr = scannerCallbackWeakPtr.lock();
		if (scannerCallbackPtr == nullptr)
			return;

		scannerCallbackPtr->onScanFailed(env, error_code);
	}

	JNIEXPORT void JNICALL Java_com_memorymd_bluetoothle_BleScanCallback_onBatchScanResults
	(
		JNIEnv* env,
		jclass class_obj,
		jlong native_callback_ptr,
		jobjectArray scan_result_array
	)
	{
		auto scannerCallbackWeakPtr = *reinterpret_cast<std::weak_ptr<CXXScanCallback>*>(native_callback_ptr);
		auto scannerCallbackPtr = scannerCallbackWeakPtr.lock();
		if (scannerCallbackPtr == nullptr)
			return;

		scannerCallbackPtr->onBatchScanResults(env, JavaArray<jobject>(scan_result_array));
	}
}