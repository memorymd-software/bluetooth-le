#include "java_scan_callback.h"

namespace Neuro::Ble::Impl
{
	static Jni::GlobalRef<Jni::JavaObject> create_java_scan_callback(JNIEnv* env, const std::weak_ptr<CXXScanCallback> &cxx_callback, const char* callback_classpath)
	{
		if (env == nullptr)
		{
			throw std::invalid_argument("JNI Environment pointer cannot be null.");
		}

		try
		{
			const auto weakPtrCallback = new std::weak_ptr<CXXScanCallback>(cxx_callback);
			const auto callbackLink = reinterpret_cast<jlong>(weakPtrCallback);
			const Jni::JavaClass scanCallbackClass(env, callback_classpath);
			Jni::JavaObject scanCallback(env, scanCallbackClass, "(J)V", callbackLink);
			return Jni::GlobalRef<Jni::JavaObject>{env, scanCallback};
		}
		catch (std::exception &e)
		{
			throw std::runtime_error(std::string("Cannot create ScanCallbackAdapter(Java): ") + e.what());
		}
		catch (...)
		{
			throw std::runtime_error("Cannot create ScanCallback(Java)");
		}
	}

	JavaScanCallback::JavaScanCallback(JNIEnv* env, const std::weak_ptr<CXXScanCallback> &cxx_callback, const char *callback_classpath) :
		mCallbackObject(create_java_scan_callback(env, cxx_callback, callback_classpath))
	{}
}