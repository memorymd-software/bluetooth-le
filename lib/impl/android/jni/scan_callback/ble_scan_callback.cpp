#include "ble_scan_callback.h"

namespace Neuro::Ble::Impl
{
	BleScanCallback::BleScanCallback(JNIEnv* env, const char* callback_classpath):
		mCxxScanCallback(std::make_shared<CXXScanCallback>()),
		mJavaScanCallback(env, mCxxScanCallback, callback_classpath)
	{}
}