#include "cxx_gatt_callback.h"
#include "jni_string.h"
#include "android/log.h"
#include <unordered_map>
#include <vector>

using namespace std::string_literals;

namespace Neuro::Ble::Impl
{
	void CXXGattCallback::onConnectionStateChanged(JNIEnv* env, jint status, jint new_state) noexcept
	{
		try
		{
			if (status != 0)
			{
				mConnectionStateNotifier.notify(AndroidGattConnectionState::Disconnected);
				return;
			}
			
			auto connectionState = static_cast<AndroidGattConnectionState>(new_state);
			mConnectionStateNotifier.notify(connectionState);
		}
		catch (std::exception & e)
		{
			__android_log_print(ANDROID_LOG_ERROR, "CXXGattCallback", "Exception thrown during onConnectionStateChanged handling: %s", e.what());
		}
		catch (...)
		{
			__android_log_print(ANDROID_LOG_ERROR, "CXXGattCallback", "Exception thrown during onConnectionStateChanged handling");
		}
	}
	
	void CXXGattCallback::onServicesDiscovered(JNIEnv* env, jint status) noexcept
	{
		try
		{
			if (status != 0)
			{
				__android_log_print(ANDROID_LOG_ERROR, "CXXGattCallback", "Gatt error status %d on discovering services", status);
				return;
			}

			mServicesDiscoveredNotifier.notify();
		}
		catch (std::exception & e)
		{
			__android_log_print(ANDROID_LOG_ERROR, "CXXGattCallback", "Exception thrown during onServicesDiscovered handling: %s", e.what());
		}
		catch (...)
		{
			__android_log_print(ANDROID_LOG_ERROR, "CXXGattCallback", "Exception thrown during onServicesDiscovered handling");
		}
	}

	static std::vector<std::byte> cachedValue(JNIEnv* env, const Jni::JavaObject& characteristic)
	{
		try
		{
			Jni::JavaArray<jbyte> charValue{ static_cast<jbyteArray>(characteristic.callMethod<jobject>(env, "getValue", "()[B")) };
			auto charValueVector = charValue.toVector(env);
			std::vector<std::byte> result{ charValueVector.size() };
			std::transform(charValueVector.begin(), charValueVector.end(), result.begin(), [](jbyte value) {return static_cast<std::byte>(value); });
			return result;
		}
		catch (std::exception & e)
		{
			throw std::runtime_error("Cannot read cached BleCharacteristic value: "s + e.what());
		}
		catch (...)
		{
			throw std::runtime_error("Cannot read cached BleCharacteristic value.");
		}
	}
	
	void CXXGattCallback::onCharacteristicChanged(JNIEnv* env, const Jni::JavaObject& characteristic) noexcept
	{
		try
		{
			Jni::JavaObject javaUuid(characteristic.callMethod<jobject>(env, "getUuid", "()Ljava/util/UUID;"));
			Jni::JavaString uuidJavaString(static_cast<jstring>(javaUuid.callMethod<jobject>(env, "toString", "()Ljava/lang/String;")));
			const UUID characteristicUuid{ uuidJavaString.toStdString(env) };
			mCharacteristicChangedNotifier.notify(characteristicUuid, cachedValue(env, characteristic));
		}
		catch (std::exception & e)
		{
			__android_log_print(ANDROID_LOG_ERROR, "CXXGattCallback", "Exception thrown during onCharacteristicChanged handling: %s", e.what());
		}
		catch (...)
		{
			__android_log_print(ANDROID_LOG_ERROR, "CXXGattCallback", "Exception thrown during onCharacteristicChanged handling");
		}
	}
	
	void CXXGattCallback::onCharacteristicRead(JNIEnv* env, const Jni::JavaObject& characteristic, jint status) noexcept
	{
		try
		{
			if (status != 0)
			{
				__android_log_print(ANDROID_LOG_ERROR, "CXXGattCallback", "Gatt error status %d on characteristic read", status);
				return;
			}
			
			Jni::JavaObject javaUuid(characteristic.callMethod<jobject>(env, "getUuid", "()Ljava/util/UUID;"));
			Jni::JavaString uuidJavaString(static_cast<jstring>(javaUuid.callMethod<jobject>(env, "toString", "()Ljava/lang/String;")));
			const UUID characteristicUuid{ uuidJavaString.toStdString(env) };
			mCharacteristicReadNotifier.notify(characteristicUuid);
		}
		catch (std::exception & e)
		{
			__android_log_print(ANDROID_LOG_ERROR, "CXXGattCallback", "Exception thrown during onCharacteristicRead handling: %s", e.what());
		}
		catch (...)
		{
			__android_log_print(ANDROID_LOG_ERROR, "CXXGattCallback", "Exception thrown during onCharacteristicRead handling");
		}
	}
	
	void CXXGattCallback::onCharacteristicWritten(JNIEnv* env, const Jni::JavaObject& characteristic, jint status) noexcept
	{
		try
		{
			if (status != 0)
			{
				__android_log_print(ANDROID_LOG_ERROR, "CXXGattCallback", "Gatt error status %d on characteristic written", status);
				return;
			}

			Jni::JavaObject javaUuid(characteristic.callMethod<jobject>(env, "getUuid", "()Ljava/util/UUID;"));
			Jni::JavaString uuidJavaString(static_cast<jstring>(javaUuid.callMethod<jobject>(env, "toString", "()Ljava/lang/String;")));
			const UUID characteristicUuid{ uuidJavaString.toStdString(env) };
			mCharacteristicWrittenNotifier.notify(characteristicUuid);
		}
		catch (std::exception & e)
		{
			__android_log_print(ANDROID_LOG_ERROR, "CXXGattCallback", "Exception thrown during onCharacteristicWritten handling: %s", e.what());
		}
		catch (...)
		{
			__android_log_print(ANDROID_LOG_ERROR, "CXXGattCallback", "Exception thrown during onCharacteristicWritten handling");
		}
	}
	void CXXGattCallback::onDescriptorWritten(JNIEnv* env, const Jni::JavaObject& descriptor, jint status) noexcept
	{
		try
		{
			if (status != 0)
			{
				__android_log_print(ANDROID_LOG_ERROR, "CXXGattCallback", "Gatt error status %d on descriptor written", status);
				return;
			}

			Jni::JavaObject javaUuid(descriptor.callMethod<jobject>(env, "getUuid", "()Ljava/util/UUID;"));
			Jni::JavaString uuidJavaString(static_cast<jstring>(javaUuid.callMethod<jobject>(env, "toString", "()Ljava/lang/String;")));
			const UUID descriptorUuid{ uuidJavaString.toStdString(env) };
			mDescriptorWrittenNotifier.notify(descriptorUuid);
		}
		catch (std::exception &e)
		{
			__android_log_print(ANDROID_LOG_ERROR, "CXXGattCallback", "Exception thrown during onDescriptorWritten handling: %s", e.what());
		}
		catch (...)
		{
			__android_log_print(ANDROID_LOG_ERROR, "CXXGattCallback", "Exception thrown during onDescriptorWritten handling");
		}
	}
}
