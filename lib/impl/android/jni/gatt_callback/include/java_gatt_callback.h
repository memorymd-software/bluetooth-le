#ifndef JAVA_GATT_CALLBACK_H
#define JAVA_GATT_CALLBACK_H

#include "cxx_gatt_callback.h"
#include "jni_object.h"
#include "jni_global_ref.h"

namespace Neuro::Ble::Impl
{
	class JavaGattCallback
	{
	public:
		JavaGattCallback(JNIEnv* env, const std::weak_ptr<CXXGattCallback>& cxx_callback, const char *callback_classpath);

		Jni::JavaObject& javaObject() const noexcept
		{
			return *mCallbackObject;
		}

	private:
		const Jni::GlobalRef<Jni::JavaObject> mCallbackObject;
	};
}

#endif //JAVA_GATT_CALLBACK_H