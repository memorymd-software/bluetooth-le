#ifndef CXX_GATT_CALLBACK_H
#define CXX_GATT_CALLBACK_H

#include "event_notifier.h"
#include "jni_array.h"
#include "jni_object.h"
#include "uuid.h"

namespace Neuro::Ble::Impl
{
	enum class AndroidGattConnectionState
	{
		Disconnected = 0,
		Connecting = 1,
		Connected = 2,
		Disconnecting = 3
	};
	
	class CXXGattCallback
	{
	public:

		void onConnectionStateChanged(JNIEnv* env, jint status, jint new_state) noexcept;
		void onServicesDiscovered(JNIEnv* env, jint status) noexcept;		
		void onCharacteristicChanged(JNIEnv *env, const Jni::JavaObject& characteristic) noexcept;
		void onCharacteristicRead(JNIEnv *env, const Jni::JavaObject& characteristic, jint status) noexcept;
		void onCharacteristicWritten(JNIEnv *env, const Jni::JavaObject& characteristic, jint status) noexcept;
		void onDescriptorWritten(JNIEnv *env, const Jni::JavaObject& descriptor, jint status) noexcept;

		Utilities::EventNotifier<const UUID &, std::vector<std::byte>>& characteristicChanged() noexcept
		{
			return mCharacteristicChangedNotifier;
		}

		Utilities::EventNotifier<const UUID&>& characteristicRead() noexcept
		{
			return mCharacteristicReadNotifier;
		}

		Utilities::EventNotifier<const UUID&>& characteristicWritten() noexcept
		{
			return mCharacteristicWrittenNotifier;
		}

		Utilities::EventNotifier<const UUID&>& descriptorWritten() noexcept
		{
			return mDescriptorWrittenNotifier;
		}

		Utilities::EventNotifier<AndroidGattConnectionState>& connectionStateChanged() noexcept
		{
			return mConnectionStateNotifier;
		}

		Utilities::EventNotifier<>& servicesDiscovered() noexcept
		{
			return mServicesDiscoveredNotifier;
		}
		
	private:
		Utilities::EventNotifier<const UUID &, std::vector<std::byte>> mCharacteristicChangedNotifier;
		Utilities::EventNotifier<const UUID &> mCharacteristicReadNotifier;
		Utilities::EventNotifier<const UUID &> mCharacteristicWrittenNotifier;
		Utilities::EventNotifier<const UUID &> mDescriptorWrittenNotifier;
		Utilities::EventNotifier<AndroidGattConnectionState> mConnectionStateNotifier;
		Utilities::EventNotifier<> mServicesDiscoveredNotifier;
	};
}

#endif //CXX_SCAN_CALLBACK_H