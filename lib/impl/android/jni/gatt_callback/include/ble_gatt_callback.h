#ifndef BLE_GATT_CALLBACK_H
#define BLE_GATT_CALLBACK_H

#include "cxx_gatt_callback.h"
#include "java_gatt_callback.h"

namespace Neuro::Ble::Impl
{
	class IBleGattCallback
	{
	public:
		virtual ~IBleGattCallback() = default;
		
		virtual Utilities::EventNotifier<const UUID &, std::vector<std::byte>>& characteristicChanged() noexcept = 0;
		virtual Utilities::EventNotifier<const UUID &>& characteristicRead() noexcept = 0;
		virtual Utilities::EventNotifier<const UUID &>& characteristicWritten() noexcept = 0;
		virtual Utilities::EventNotifier<const UUID &>& descriptorWritten() noexcept = 0;
		virtual const JavaGattCallback& javaCallback() const noexcept = 0;
	};
	
	class BleGattCallback final : public IBleGattCallback
	{
	public:
		explicit BleGattCallback
		(
			JNIEnv *env, 
			const Utilities::EventListener<AndroidGattConnectionState> &,
			const Utilities::EventListener<> &,
			const char* callback_classpath = "com/memorymd/bluetoothle/BleGattCallback"
		);

		Utilities::EventNotifier<const UUID &, std::vector<std::byte>>& characteristicChanged() noexcept
		{
			return mCxxGattCallback->characteristicChanged();
		}

		Utilities::EventNotifier<const UUID&>& characteristicRead() noexcept
		{
			return mCxxGattCallback->characteristicRead();
		}

		Utilities::EventNotifier<const UUID&>& characteristicWritten() noexcept
		{
			return mCxxGattCallback->characteristicWritten();
		}

		Utilities::EventNotifier<const UUID&>& descriptorWritten() noexcept
		{
			return mCxxGattCallback->descriptorWritten();
		}
	
		const JavaGattCallback& javaCallback() const noexcept
		{
			return mJavaGattCallback;
		}

	private:
		//Order is crucial!
		std::shared_ptr<CXXGattCallback> mCxxGattCallback;
		JavaGattCallback mJavaGattCallback;
	};
}

#endif //BLE_SCAN_CALLBACK_H
