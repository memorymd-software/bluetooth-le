#include "java_gatt_callback.h"

namespace Neuro::Ble::Impl
{
	static Jni::GlobalRef<Jni::JavaObject> create_java_gatt_callback(JNIEnv* env, const std::weak_ptr<CXXGattCallback> &cxx_callback, const char* callback_classpath)
	{
		if (env == nullptr)
		{
			throw std::invalid_argument("JNI Environment pointer cannot be null.");
		}

		try
		{
			const auto weakPtrCallback = new std::weak_ptr<CXXGattCallback>(cxx_callback);
			const auto callbackLink = reinterpret_cast<jlong>(weakPtrCallback);
			const Jni::JavaClass gattCallbackClass(env, callback_classpath);
			Jni::JavaObject gattCallback(env, gattCallbackClass, "(J)V", callbackLink);
			return Jni::GlobalRef<Jni::JavaObject>{env, gattCallback};
		}
		catch (std::exception &e)
		{
			throw std::runtime_error(std::string("Cannot create GattCallback(Java): ") + e.what());
		}
		catch (...)
		{
			throw std::runtime_error("Cannot create GattCallback");
		}
	}

	JavaGattCallback::JavaGattCallback(JNIEnv* env, const std::weak_ptr<CXXGattCallback> &cxx_callback, const char *callback_classpath) :
		mCallbackObject(create_java_gatt_callback(env, cxx_callback, callback_classpath))
	{}
}