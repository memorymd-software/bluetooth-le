#include "cxx_gatt_callback.h"
#include <memory>
#include <vector>

using Neuro::Ble::Impl::CXXGattCallback;
using Neuro::Ble::Impl::Jni::JavaClass;
using Neuro::Ble::Impl::Jni::JavaObject;
using Neuro::Ble::Impl::Jni::JavaArray;

/*
 *JNI function mandatory name template
 *
JNIEXPORT <RETURN_TYPE> JNICALL Java_<PACKAGE_NAME>_<JAVA_CLASS>_<METHOD_NAME>(
		JNIEnv *env, jobject obj, <METHOD_PARAMETERS>...) {
	...
}
 */

extern "C"
{
	JNIEXPORT void JNICALL Java_com_memorymd_bluetoothle_BleGattCallback_destroyNativeCallbackLink
	(
		JNIEnv* env,
		jclass class_obj,
		jlong native_callback_ptr
	)
	{
		using Neuro::Ble::Impl::CXXGattCallback;
		auto scannerCallbackWeakPtrPtr = reinterpret_cast<std::weak_ptr<CXXGattCallback>*>(native_callback_ptr);
		delete scannerCallbackWeakPtrPtr;
	}

	JNIEXPORT void JNICALL Java_com_memorymd_bluetoothle_BleGattCallback_onCharacteristicChanged
	(
		JNIEnv* env,
		jclass class_obj,
		jlong native_callback_ptr,
		jobject characteristic
	)
	{
		auto scannerCallbackWeakPtr = *reinterpret_cast<std::weak_ptr<CXXGattCallback>*>(native_callback_ptr);
		auto scannerCallbackPtr = scannerCallbackWeakPtr.lock();
		if (scannerCallbackPtr == nullptr)
			return;

		scannerCallbackPtr->onCharacteristicChanged(env, JavaObject(characteristic));
	}

	JNIEXPORT void JNICALL Java_com_memorymd_bluetoothle_BleGattCallback_onCharacteristicRead
	(
		JNIEnv* env,
		jclass class_obj,
		jlong native_callback_ptr,
		jobject characteristic,
		jint status
	)
	{
		auto scannerCallbackWeakPtr = *reinterpret_cast<std::weak_ptr<CXXGattCallback>*>(native_callback_ptr);
		auto scannerCallbackPtr = scannerCallbackWeakPtr.lock();
		if (scannerCallbackPtr == nullptr)
			return;

		scannerCallbackPtr->onCharacteristicRead(env, JavaObject(characteristic), status);
	}

	JNIEXPORT void JNICALL Java_com_memorymd_bluetoothle_BleGattCallback_onCharacteristicWrite
	(
		JNIEnv* env,
		jclass class_obj,
		jlong native_callback_ptr,
		jobject characteristic,
		jint status
	)
	{
		auto scannerCallbackWeakPtr = *reinterpret_cast<std::weak_ptr<CXXGattCallback>*>(native_callback_ptr);
		auto scannerCallbackPtr = scannerCallbackWeakPtr.lock();
		if (scannerCallbackPtr == nullptr)
			return;

		scannerCallbackPtr->onCharacteristicWritten(env, JavaObject(characteristic), status);
	}

	JNIEXPORT void JNICALL Java_com_memorymd_bluetoothle_BleGattCallback_onDescriptorWrite
	(
		JNIEnv* env,
		jclass class_obj,
		jlong native_callback_ptr,
		jobject descriptor,
		jint status
	)
	{
		auto scannerCallbackWeakPtr = *reinterpret_cast<std::weak_ptr<CXXGattCallback>*>(native_callback_ptr);
		auto scannerCallbackPtr = scannerCallbackWeakPtr.lock();
		if (scannerCallbackPtr == nullptr)
			return;

		scannerCallbackPtr->onDescriptorWritten(env, JavaObject(descriptor), status);
	}

	JNIEXPORT void JNICALL Java_com_memorymd_bluetoothle_BleGattCallback_onConnectionStateChange
	(
		JNIEnv* env,
		jclass class_obj,
		jlong native_callback_ptr,
		jint status,
		jint new_state
	)
	{
		auto scannerCallbackWeakPtr = *reinterpret_cast<std::weak_ptr<CXXGattCallback>*>(native_callback_ptr);
		auto scannerCallbackPtr = scannerCallbackWeakPtr.lock();
		if (scannerCallbackPtr == nullptr)
			return;

		scannerCallbackPtr->onConnectionStateChanged(env, status, new_state);
	}

	JNIEXPORT void JNICALL Java_com_memorymd_bluetoothle_BleGattCallback_onServicesDiscovered
	(
		JNIEnv* env,
		jclass class_obj,
		jlong native_callback_ptr,
		jint status
	)
	{
		auto scannerCallbackWeakPtr = *reinterpret_cast<std::weak_ptr<CXXGattCallback>*>(native_callback_ptr);
		auto scannerCallbackPtr = scannerCallbackWeakPtr.lock();
		if (scannerCallbackPtr == nullptr)
			return;

		scannerCallbackPtr->onServicesDiscovered(env, status);
	}
}