#include "ble_gatt_callback.h"

namespace Neuro::Ble::Impl
{
	BleGattCallback::BleGattCallback
	(
		JNIEnv* env, 
		const Utilities::EventListener<AndroidGattConnectionState> &connection_listener,
		const Utilities::EventListener<> &service_discovered_listener,
		const char* callback_classpath
	):
		mCxxGattCallback(std::make_shared<CXXGattCallback>()),
		mJavaGattCallback(env, mCxxGattCallback, callback_classpath)
	{
		mCxxGattCallback->connectionStateChanged().registerListener(connection_listener);
		mCxxGattCallback->servicesDiscovered().registerListener(service_discovered_listener);
	}
}