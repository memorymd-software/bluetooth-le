#ifndef ALWAYS_ON_SCANNER_H
#define ALWAYS_ON_SCANNER_H

#include "java_bt_adapter.h"

namespace Neuro::Ble::Impl
{
	class AlwaysOnScanner final
	{
	public:
		AlwaysOnScanner(JNIEnv *env, std::unique_ptr<IJavaBleScanner> &&scanner, const JavaScanCallback& callback) :
			mJavaVM(Jni::get_java_vm(env)),
			mCallback(callback),
			mScanner(std::move(scanner))
		{
			try
			{
				mScanner->startScan(env, mCallback);
			}
			catch (std::exception & e)
			{
				throw std::runtime_error(std::string("Cannot enable scanning: ") + e.what());
			}
			catch (...)
			{
				throw std::runtime_error("Cannot enable scanning.");
			}
		}

		AlwaysOnScanner(JNIEnv* env, std::unique_ptr<IJavaBleScanner>&& scanner, const JavaScanCallback &callback, const std::vector<ScanFilter>& scan_filters) :
			mJavaVM(Jni::get_java_vm(env)),
			mCallback(callback),
			mScanner(std::move(scanner))
		{
			try
			{
				mScanner->startScan(env, mCallback, scan_filters);
			}
			catch (std::exception & e)
			{
				throw std::runtime_error(std::string("Cannot enable scanning: ") + e.what());
			}
			catch (...)
			{
				throw std::runtime_error("Cannot enable scanning.");
			}
		}

		AlwaysOnScanner(const AlwaysOnScanner&) = delete;
		AlwaysOnScanner& operator=(const AlwaysOnScanner&) = delete;
		AlwaysOnScanner(AlwaysOnScanner &&) = delete;
		AlwaysOnScanner& operator=(AlwaysOnScanner &&) = delete;
		
		~AlwaysOnScanner()
		{
			try
			{
				Jni::call_in_attached_env(mJavaVM,
					[=](auto env)
					{
						mScanner->stopScan(env, mCallback);
					});
			}
			catch (...)
			{
				//do nothing
			}
		}

	private:
		JavaVM* mJavaVM;
		const JavaScanCallback& mCallback;
		std::unique_ptr<IJavaBleScanner> mScanner;
	};
}

#endif // ALWAYS_ON_SCANNER_H
