#ifndef ANDROID_BLE_DEVICE_H
#define ANDROID_BLE_DEVICE_H

#include "ble_device.h"
#include "java_bt_adapter.h"
#include "ble_gatt_callback.h"
#include "ble_gatt.h"
#include <chrono>

namespace Neuro::Ble
{
	namespace Impl
	{
		class AndroidBleDevice final : public IBleDevice
		{
		public:
			AndroidBleDevice(JNIEnv* env, const Jni::JavaObject &device_ref, const Jni::JavaObject &context, std::chrono::seconds timeout = std::chrono::seconds(10));

			[[nodiscard]] const DeviceIdentifier& identifier() const noexcept override;			
			const std::string& name() const noexcept override;
			[[nodiscard]] ConnectionState connectionState() const noexcept override;
			Utilities::EventNotifier<ConnectionState>& connectionStateChanged() noexcept override;
			std::future<std::vector<std::shared_ptr<IBleService>>> getServicesByUuid(const std::vector<UUID>& services_uuids) const override;

		private:
			JavaVM* mJavaVM;
			const std::string mName;
			const DeviceIdentifier mIdentifier;
			BleGatt mGatt;
		};
	}
}

#endif // ANDROID_BLE_DEVICE_H
