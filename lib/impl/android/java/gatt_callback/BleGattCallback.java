package com.memorymd.bluetoothle;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.util.Log;

import java.util.List;
import java.util.ArrayList;

public class BleGattCallback extends BluetoothGattCallback
{
	private final long mNativeCallbackPtr;

	public BleGattCallback(long nativeCallbackPtr)
	{
		mNativeCallbackPtr = nativeCallbackPtr;
	}

	@Override
    public void finalize()
	{
		destroyNativeCallbackLink(mNativeCallbackPtr);
	}
	
	@Override
    public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
        onCharacteristicChanged(mNativeCallbackPtr, characteristic);
    }

	@Override
	public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState){
		onConnectionStateChange(mNativeCallbackPtr, status, newState);
	}

	@Override
	public void onServicesDiscovered(BluetoothGatt gatt, int status){
		onServicesDiscovered(mNativeCallbackPtr, status);
	}

	@Override
	public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status){
		onCharacteristicRead(mNativeCallbackPtr, characteristic, status);
	}

	@Override
	public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status){
		onCharacteristicWrite(mNativeCallbackPtr, characteristic, status);
	}

	@Override
    public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status){
		onDescriptorWrite(mNativeCallbackPtr, descriptor, status);
	}

	private static native void destroyNativeCallbackLink(long nativeCallbackPtr);
	private static native void onCharacteristicChanged(long nativeCallbackPtr, BluetoothGattCharacteristic characteristic);
	private static native void onCharacteristicRead(long nativeCallbackPtr, BluetoothGattCharacteristic characteristic, int status);
	private static native void onCharacteristicWrite(long nativeCallbackPtr, BluetoothGattCharacteristic characteristic, int status);
	private static native void onDescriptorWrite(long nativeCallbackPtr, BluetoothGattDescriptor descriptor, int status);
	private static native void onConnectionStateChange(long nativeCallbackPtr, int status, int newState);
	private static native void onServicesDiscovered(long nativeCallbackPtr, int status);
}