package com.memorymd.bluetoothle;

import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanCallback;
import android.util.Log;

import java.util.List;
import java.util.ArrayList;

public class ScanCallbackAdapter extends ScanCallback
{
	private final BleScanCallback mScanCallback;

	public ScanCallbackAdapter(long nativeCallbackPtr)
	{
		mScanCallback = new BleScanCallback(nativeCallbackPtr);
	}

    @Override
    public void onBatchScanResults(List<ScanResult> results) 
    {
        try
        {
			List<IScanResult> batchScanResult = new ArrayList<>();
			for (ScanResult result : results)
			{
				batchScanResult.add(new ScanResultWrapper(result));
			}
			mScanCallback.onBatchScanResults(batchScanResult);
		}
		catch (Exception e)
		{
			Log.e("ScanCallbackAdapter", String.format("Exception in batch scan result handler: %s", e.getMessage()));
		}
    }

	@Override
    public void onScanFailed(int errorCode) 
	{
		try
		{
			mScanCallback.onScanFailed(errorCode);
		}
		catch (Exception e)
		{
			Log.e("ScanCallbackAdapter", String.format("Exception in scan failed handler: %s", e.getMessage()));
		}		
    }

	@Override
    public void onScanResult(int callbackType, final ScanResult result) 
	{
		try
		{
			mScanCallback.onScanResult(callbackType, new ScanResultWrapper(result));
		}
		catch (Exception e)
		{
			Log.e("ScanCallbackAdapter", String.format("Exception in scan result handler: %s", e.getMessage()));
		}	
    }
}