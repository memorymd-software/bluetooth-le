package com.memorymd.bluetoothle;

import java.util.List;
import android.util.SparseArray;
import android.os.ParcelUuid;

public interface IScanRecord
{
	SparseArray<byte[]> getManufacturerSpecificData();
	String getDeviceName();
	List<ParcelUuid> getServiceUuids();
}