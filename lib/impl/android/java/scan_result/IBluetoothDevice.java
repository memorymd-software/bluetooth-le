package com.memorymd.bluetoothle;

public interface IBluetoothDevice
{
	String getAddress();
}