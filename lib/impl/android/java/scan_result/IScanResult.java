package com.memorymd.bluetoothle;

import android.bluetooth.le.ScanResult;
import android.util.Log;

import java.util.List;

public interface IScanResult 
{
	IBluetoothDevice getDevice();
	IScanRecord getScanRecord();
	int getRssi();
	long getTimestampNanos();
}