package com.memorymd.bluetoothle;

import android.bluetooth.le.ScanRecord;
import java.util.List;
import android.util.SparseArray;
import android.os.ParcelUuid;

public class ScanRecordWrapper implements IScanRecord
{
	private final ScanRecord mRecord;

	public ScanRecordWrapper(ScanRecord record)
	{
		if (record == null)
			throw new IllegalArgumentException("ScanRecord cannot be null");

		mRecord = record;
	}
	
	@Override
	public SparseArray<byte[]> getManufacturerSpecificData()
	{
		return mRecord.getManufacturerSpecificData();
	}

	@Override
	public String getDeviceName()
	{
		return mRecord.getDeviceName();
	}

	@Override
	public List<ParcelUuid> getServiceUuids()
	{
		return mRecord.getServiceUuids();
	}
}