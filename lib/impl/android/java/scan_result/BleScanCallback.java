package com.memorymd.bluetoothle;

import android.bluetooth.le.ScanResult;
import android.util.Log;

import java.util.List;
import java.util.ArrayList;

public class BleScanCallback
{
	private final long mNativeCallbackPtr;

	public BleScanCallback(long nativeCallbackPtr)
	{
		mNativeCallbackPtr = nativeCallbackPtr;
	}

	@Override
    public void finalize()
	{
		destroyNativeCallbackLink(mNativeCallbackPtr);
	}
	
    public void onBatchScanResults(List<IScanResult> results) 
    {
        try
        {
			onBatchScanResults(mNativeCallbackPtr, results);
		}
		catch (Exception e)
		{
			Log.e("BleScanCallback", String.format("Exception in batch scan result handler: %s", e.getMessage()));
		}
    }

    public void onScanFailed(int errorCode) 
	{
		try
		{
			onScanFailed(mNativeCallbackPtr, errorCode);
		}
		catch (Exception e)
		{
			Log.e("BleScanCallback", String.format("Exception in scan failed handler: %s", e.getMessage()));
		}		
    }

    public void onScanResult(int callbackType, final IScanResult result) 
	{
		try
		{
			onScanResult(mNativeCallbackPtr, callbackType, result);
		}
		catch (Exception e)
		{
			Log.e("BleScanCallback", String.format("Exception in scan result handler: %s", e.getMessage()));
		}	
    }

	private static native void destroyNativeCallbackLink(long nativeCallbackPtr);
	private static native void onScanResult(long nativeCallbackPtr, int callbackType, IScanResult scanResult);
	private static native void onScanFailed(long nativeCallbackPtr, int errorCode);
	private static native void onBatchScanResults(long nativeCallbackPtr, List<IScanResult> scanResults);
}