package com.memorymd.bluetoothle;

import android.bluetooth.le.ScanResult;

public class ScanResultWrapper implements IScanResult
{
	private final ScanResult mResult;

	public ScanResultWrapper(ScanResult result)
	{
		if (result == null)
			throw new IllegalArgumentException("ScanResult cannot be null");

		mResult = result;
	}

	@Override
	public IBluetoothDevice getDevice()
	{
		return new BluetoothDeviceWrapper(mResult.getDevice());
	}
	
	@Override
	public IScanRecord getScanRecord()
	{
		return new ScanRecordWrapper(mResult.getScanRecord());
	}

	@Override
	public int getRssi()
	{
		return mResult.getRssi();
	}
	
	@Override
	public long getTimestampNanos()
	{
		return mResult.getTimestampNanos();
	}
}