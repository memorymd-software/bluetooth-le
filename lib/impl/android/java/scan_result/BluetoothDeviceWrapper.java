package com.memorymd.bluetoothle;

import android.bluetooth.BluetoothDevice;

public class BluetoothDeviceWrapper implements IBluetoothDevice
{
	private final BluetoothDevice mDevice;

	public BluetoothDeviceWrapper(BluetoothDevice device)
	{
		if (device == null)
			throw new IllegalArgumentException("Device cannot be null");

		mDevice = device;
	}

	@Override
	public String getAddress()
	{
		return mDevice.getAddress();
	}
}