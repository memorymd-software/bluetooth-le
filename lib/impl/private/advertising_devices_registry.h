#ifndef ADVERTISING_DEVICES_REGISTRY_H
#define ADVERTISING_DEVICES_REGISTRY_H

#include "advertisement_data.h"
#include "event_notifier.h"
#include "scan_settings.h"
#include "loop.h"
#include <vector>
#include <unordered_map>
#include <shared_mutex>

namespace Neuro::Ble::Impl
{
	class IAdvertisingDevicesRegistry
	{
	public:
		virtual ~IAdvertisingDevicesRegistry() = default;
		
		virtual void onAdvertisementReceived(const std::vector<AdvertisementData>& advertisements) = 0;
		virtual Utilities::EventNotifier<>& advertisementListChanged() = 0;
		virtual std::vector<AdvertisementData> advertisingDevices() const = 0;
	};
	
	class AdvertisingDevicesRegistry final : public IAdvertisingDevicesRegistry
	{
	public:
		using AdvertisementMap = std::unordered_map<DeviceIdentifier, AdvertisementData>;

		explicit AdvertisingDevicesRegistry
		(
			const ScanSettings& settings,
			std::chrono::seconds check_every = std::chrono::seconds(1)
		);
		
		explicit AdvertisingDevicesRegistry
		(
			int rssi_threshold = -75, 
			std::chrono::seconds remove_older = std::chrono::seconds(15), 
			std::chrono::seconds check_every = std::chrono::seconds(1)
		);
		
		void onAdvertisementReceived(const std::vector<AdvertisementData>& advertisements) override;
		Utilities::EventNotifier<>& advertisementListChanged() override;
		std::vector<AdvertisementData> advertisingDevices() const override;
	private:
		int mRSSIThreshold;
		Utilities::EventNotifier<> mListChangedNotifier;
		mutable std::shared_mutex mTimestampMapMutex;
		AdvertisementMap mAdvertisementsMap;
		Utilities::Loop<void()> mDeviceUpdateLoop;
	};
}

#endif // ADVERTISING_DEVICES_REGISTRY_H
