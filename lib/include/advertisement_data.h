#ifndef ADVERTISEMENT_DATA_H
#define ADVERTISEMENT_DATA_H

#include "device_identifier.h"
#include "uuid.h"
#include <chrono>
#include <cstddef>
#include <string>
#include <vector>
#include <unordered_map>

namespace Neuro::Ble
{	
	struct AdvertisementData final
	{
        std::string Name;
        DeviceIdentifier Identifier;
        int RSSI;
        std::vector<UUID> ServicesUUIDs;
        std::chrono::steady_clock::time_point TimeStamp;
        std::unordered_map<int, std::vector<std::byte>> ManufacturerData;
	};
}

#endif // ADVERTISEMENT_DATA_H
