#ifndef SCAN_SETTINGS_H
#define SCAN_SETTINGS_H

#include <optional>
#include <chrono>

namespace Neuro::Ble
{
	struct ScanSettings final
	{
		std::optional<int> MinRssi;
		std::optional<std::chrono::seconds> AdvertisementLostTimeout;
	};
}

#endif // SCAN_SETTINGS_H
