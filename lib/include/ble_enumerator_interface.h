#ifndef BLE_ENUMERATOR_INTERFACE_H
#define BLE_ENUMERATOR_INTERFACE_H

#include "advertisement_data.h"
#include "event_notifier.h"
#include "ble_device.h"
#include <vector>

namespace Neuro::Ble
{
	class IBleEnumerator
	{
	public:
		virtual ~IBleEnumerator() = default;

		virtual std::vector<AdvertisementData> advertisingDevices() const = 0;
		virtual Utilities::EventNotifier<>& advertisementListChanged() = 0;
		virtual std::unique_ptr<IBleDevice> createBleDevice(const DeviceIdentifier& identifier) const = 0;
	};
}

#endif // BLE_ENUMERATOR_INTERFACE_H
