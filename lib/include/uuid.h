#ifndef UUID_H
#define UUID_H

#include <array>
#include <cstddef>
#include <functional>
#include <string>

namespace Neuro::Ble
{
	class UUID final
	{
	public:
        UUID() = default;
		explicit UUID(const std::string& uuid_string);

		explicit constexpr UUID(const std::array<std::byte, 16>& uuid_array) noexcept :
			mLow({uuid_array[0], uuid_array[1], uuid_array[2], uuid_array[3]}),
			mMid({uuid_array[4], uuid_array[5]}),
			mHi({uuid_array[6], uuid_array[7]}),
			mClock({uuid_array[8], uuid_array[9]}),
			mNode({uuid_array[10], uuid_array[11], uuid_array[12], uuid_array[13], uuid_array[14], uuid_array[15]}) {}

		constexpr UUID(const std::array<std::byte, 4>& low,
		               const std::array<std::byte, 2>& mid,
		               const std::array<std::byte, 2>& hi,
		               const std::array<std::byte, 2>& clock,
		               const std::array<std::byte, 6>& node) noexcept :
			mLow(low),
			mMid(mid),
			mHi(hi),
			mClock(clock),
			mNode(node) {}

		constexpr const std::array<std::byte, 4>& low() const noexcept { return mLow; }
		constexpr std::array<std::byte, 4>& low() noexcept { return mLow; }

		constexpr const std::array<std::byte, 2>& mid() const noexcept { return mMid; }
		constexpr std::array<std::byte, 2>& mid() noexcept { return mMid; }

		constexpr const std::array<std::byte, 2>& high() const noexcept { return mHi; }
		constexpr std::array<std::byte, 2>& high() noexcept { return mHi; }

		constexpr const std::array<std::byte, 2>& clock() const noexcept { return mClock; }
		constexpr std::array<std::byte, 2>& clock() noexcept { return mClock; }

		constexpr const std::array<std::byte, 6>& node() const noexcept { return mNode; }
		constexpr std::array<std::byte, 6>& node() noexcept { return mNode; }

	private:
		std::array<std::byte, 4> mLow;
		std::array<std::byte, 2> mMid;
		std::array<std::byte, 2> mHi;
		std::array<std::byte, 2> mClock;
		std::array<std::byte, 6> mNode;
	};

    std::ostream &operator<<(std::ostream &out_stream, const UUID &uuid);

	std::string to_string(const UUID& uuid);

    bool operator==(const UUID &lhs, const UUID &rhs);

}

template<>
struct std::hash<Neuro::Ble::UUID>
{
	size_t operator()(const Neuro::Ble::UUID& uuid) const noexcept;
};

#endif // UUID_H
