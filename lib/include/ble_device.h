#ifndef BLE_DEVICE_INTERFACE_H
#define BLE_DEVICE_INTERFACE_H

#include "device_identifier.h"
#include "event_notifier.h"
#include "uuid.h"
#include <future>
#include <memory>
#include <vector>
#include <string>

namespace Neuro::Ble
{

enum class CharacteristicProperties : unsigned char
{
	None = 0b00000000,
	Read = 0b00000001,
	Write = 0b00000010,
	Notify = 0b00000100,
};

class IBleCharacteristic
{
public:
	virtual ~IBleCharacteristic() = default;

	[[nodiscard]] virtual CharacteristicProperties properties() const noexcept = 0;
	[[nodiscard]] virtual std::vector<std::byte> value() const = 0;
	virtual Utilities::EventNotifier<const std::vector<std::byte> &>& valueChanged() = 0;
	virtual void write(const std::vector<std::byte> &) = 0;
};

class IBleService
{
public:
	virtual ~IBleService() = default;
	
	virtual std::future<std::vector<std::shared_ptr<IBleCharacteristic>>> getCharacteristicsByUuid(const std::vector<UUID>& char_uuids) const = 0;
};

enum class ConnectionState
{
	InRage,
	OutOfRange
};
	
class IBleDevice
{
public:
    virtual ~IBleDevice() = default;

	[[nodiscard]] virtual const DeviceIdentifier& identifier() const noexcept = 0;
	[[nodiscard]] virtual const std::string& name() const noexcept = 0;
	[[nodiscard]] virtual ConnectionState connectionState() const noexcept = 0;
	virtual Utilities::EventNotifier<ConnectionState>& connectionStateChanged() noexcept = 0;
    virtual std::future<std::vector<std::shared_ptr<IBleService>>> getServicesByUuid(const std::vector<UUID>& services_uuids) const = 0;
};

}
#endif // BLE_DEVICE_INTERFACE_H
