#ifndef SCAN_FILTER_H
#define SCAN_FILTER_H

#include "device_identifier.h"
#include "uuid.h"
#include <string>
#include <optional>

namespace Neuro::Ble
{
	struct ScanFilter final
	{
		std::optional<DeviceIdentifier> DeviceId;
		std::optional<std::string> DeviceName;
		std::optional<UUID> MainServiceUuid;
	};
}

#endif // SCAN_FILTER_H
