#ifndef BLE_DEVICE_ADDRESS_H
#define BLE_DEVICE_ADDRESS_H

#include <array>
#include <cstddef>
#include <functional>
#include <iosfwd>
#include <string>

namespace Neuro::Ble
{
    struct BleDeviceAddress final
    {
        static constexpr size_t AddressSize = 6;

        using value_type = std::byte;
        using container_type = std::array<value_type, AddressSize>;
        using size_type = container_type::size_type;
        using difference_type = container_type::difference_type;
        using reference = container_type::reference;
        using const_reference = container_type::const_reference;
        using pointer = container_type::pointer;
        using const_pointer = container_type::const_pointer;
        using iterator = container_type::iterator;
        using const_iterator = container_type::const_iterator;
        using reverse_iterator = container_type::reverse_iterator;
        using const_reverse_iterator = container_type::const_reverse_iterator;

        explicit constexpr BleDeviceAddress(const std::array<value_type, AddressSize> &value) noexcept
            :
            mValue(value)
        {}
		   	
        explicit BleDeviceAddress(const std::string &address_string);

		static BleDeviceAddress fromLSByteFirstUint64(uint64_t);

		static BleDeviceAddress fromMSByteFirstUint64(uint64_t);

		static BleDeviceAddress fromUint64(uint64_t);

        [[nodiscard]]
        constexpr size_type size() const noexcept
        {
            return AddressSize;
        }

        constexpr iterator begin() noexcept
        {
            return mValue.begin();
        }

        [[nodiscard]]
        constexpr const_iterator begin() const noexcept
        {
            return mValue.begin();
        }

        [[nodiscard]]
        constexpr const_iterator cbegin() const noexcept
        {
            return mValue.cbegin();
        }

        constexpr iterator end() noexcept
        {
            return mValue.end();
        }

        [[nodiscard]]
        constexpr const_iterator end() const noexcept
        {
            return mValue.end();
        }

        [[nodiscard]]
        constexpr const_iterator cend() const noexcept
        {
            return mValue.cend();
        }

        constexpr const_reference operator[](size_t index) const noexcept
        {
            return mValue[index];
        }

        constexpr reference operator[](size_t index) noexcept
        {
            return mValue[index];
        }

    private:
        std::array<value_type, AddressSize> mValue;
    };

    bool operator==(const BleDeviceAddress &lhs, const BleDeviceAddress &rhs);

    std::ostream &operator<<(std::ostream &out_stream, const BleDeviceAddress &address);

    std::string to_string(const BleDeviceAddress &);

	uint64_t to_LSByteFirst_uint64(const BleDeviceAddress &address);

	uint64_t to_MSByteFirst_uint64(const BleDeviceAddress &address);

    uint64_t to_uint64(const BleDeviceAddress &address);
}

template<>
struct std::hash<Neuro::Ble::BleDeviceAddress>
{
    size_t operator()(const Neuro::Ble::BleDeviceAddress &address) const noexcept;
};

#endif // BLE_DEVICE_ADDRESS_H
