#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "advertising_devices_registry.h"

using Neuro::Ble::Impl::AdvertisingDevicesRegistry;
using Neuro::Ble::AdvertisementData;
using Neuro::Ble::DeviceIdentifier;
using Neuro::Ble::UUID;

static DeviceIdentifier get_device_id()
{
#ifdef __APPLE__
	return DeviceIdentifier{ UUID { "11111111-2222-3333-4444-555555555555" } };
#else
	return DeviceIdentifier{ Neuro::Ble::BleDeviceAddress { "11:22:33:44:55:66" } };
#endif
}

TEST(AdvertisingDevicesRegistryTests, Create_DefaultParams_NoException)
{
	AdvertisingDevicesRegistry registry;
}

TEST(AdvertisingDevicesRegistryTests, NoNotification_NoDevices)
{
	AdvertisingDevicesRegistry registry;
	ASSERT_TRUE(registry.advertisingDevices().empty());
}

TEST(AdvertisingDevicesRegistryTests, RemoveTime5Sec_ContainsDevice)
{
	AdvertisingDevicesRegistry registry(-75, std::chrono::seconds(5), std::chrono::seconds(1));
	AdvertisementData data
	{
		"DumbDevice",
		get_device_id(),
		-70,
	{UUID{"11111111-2222-3333-4444-555555555555"}, UUID{"AAAAAAAA-BBBB-CCCC-DDDD-EEEEEEEEEEEE"}},
		std::chrono::steady_clock::now(),
		std::unordered_map<int, std::vector<std::byte>>{ {14, std::vector{std::byte{0x00}}}, {28, std::vector{std::byte{0x11}}}}
	};
	registry.onAdvertisementReceived({ data });
	auto devices = registry.advertisingDevices();
	auto result = std::find_if(devices.begin(), devices.end(), [&data](const auto& device_advertisement)
		{
			return data.Identifier == device_advertisement.Identifier;
		});

	ASSERT_NE(result, devices.end());
}

TEST(AdvertisingDevicesRegistryTests, LowRssi_NoDevice)
{
	AdvertisingDevicesRegistry registry(-75, std::chrono::seconds(5), std::chrono::seconds(1));
	AdvertisementData data
	{
		"DumbDevice",
		get_device_id(),
		-76,
	{UUID{"11111111-2222-3333-4444-555555555555"}, UUID{"AAAAAAAA-BBBB-CCCC-DDDD-EEEEEEEEEEEE"}},
		std::chrono::steady_clock::now(),
		std::unordered_map<int, std::vector<std::byte>>{ {14, std::vector{std::byte{0x00}}}, {28, std::vector{std::byte{0x11}}}}
	};
	registry.onAdvertisementReceived({ data });
	auto devices = registry.advertisingDevices();
	auto result = std::find_if(devices.begin(), devices.end(), [&data](const auto& device_advertisement)
		{
			return data.Identifier == device_advertisement.Identifier;
		});

	ASSERT_EQ(result, devices.end());
}

TEST(AdvertisingDevicesRegistryTests, RemoveTime5Sec_ContainsDeviceAfter4Sec)
{
	AdvertisingDevicesRegistry registry(-75, std::chrono::seconds(5), std::chrono::seconds(1));
	AdvertisementData data
	{
		"DumbDevice",
		get_device_id(),
		-70,
	{UUID{"11111111-2222-3333-4444-555555555555"}, UUID{"AAAAAAAA-BBBB-CCCC-DDDD-EEEEEEEEEEEE"}},
		std::chrono::steady_clock::now(),
		std::unordered_map<int, std::vector<std::byte>>{ {14, std::vector{std::byte{0x00}}}, {28, std::vector{std::byte{0x11}}}}
	};
	registry.onAdvertisementReceived({ data });
	std::this_thread::sleep_for(std::chrono::seconds(4));
	auto devices = registry.advertisingDevices();
	auto result = std::find_if(devices.begin(), devices.end(), [&data](const auto& device_advertisement)
		{
			return data.Identifier == device_advertisement.Identifier;
		});

	ASSERT_NE(result, devices.end());
}

TEST(AdvertisingDevicesRegistryTests, RemoveTime5Sec_NoDeviceAfter6Sec)
{
	AdvertisingDevicesRegistry registry(-75, std::chrono::seconds(5), std::chrono::seconds(1));
	AdvertisementData data
	{
		"DumbDevice",
		get_device_id(),
		-70,
	{UUID{"11111111-2222-3333-4444-555555555555"}, UUID{"AAAAAAAA-BBBB-CCCC-DDDD-EEEEEEEEEEEE"}},
		std::chrono::steady_clock::now(),
		std::unordered_map<int, std::vector<std::byte>>{ {14, std::vector{std::byte{0x00}}}, {28, std::vector{std::byte{0x11}}}}
	};
	registry.onAdvertisementReceived({ data });
	std::this_thread::sleep_for(std::chrono::seconds(6));
	auto devices = registry.advertisingDevices();
	auto result = std::find_if(devices.begin(), devices.end(), [&data](const auto& device_advertisement)
		{
			return data.Identifier == device_advertisement.Identifier;
		});

	ASSERT_EQ(result, devices.end());
}

TEST(AdvertisingDevicesRegistryTests, RemoveTime5Sec_UpdateAfter4sec_ContainsDeviceAfter7Sec)
{
	AdvertisingDevicesRegistry registry(-75, std::chrono::seconds(5), std::chrono::seconds(1));
	AdvertisementData data
	{
		"DumbDevice",
		get_device_id(),
		-70,
	{UUID{"11111111-2222-3333-4444-555555555555"}, UUID{"AAAAAAAA-BBBB-CCCC-DDDD-EEEEEEEEEEEE"}},
		std::chrono::steady_clock::now(),
		std::unordered_map<int, std::vector<std::byte>>{ {14, std::vector{std::byte{0x00}}}, {28, std::vector{std::byte{0x11}}}}
	};
	registry.onAdvertisementReceived({ data });
	std::this_thread::sleep_for(std::chrono::seconds(4));
	data.TimeStamp = std::chrono::steady_clock::now();
	registry.onAdvertisementReceived({ data });
	std::this_thread::sleep_for(std::chrono::seconds(3));
	auto devices = registry.advertisingDevices();
	auto result = std::find_if(devices.begin(), devices.end(), [&data](const auto& device_advertisement)
		{
			return data.Identifier == device_advertisement.Identifier;
		});

	ASSERT_NE(result, devices.end());
}

TEST(AdvertisingDevicesRegistryTests, RemoveTime5Sec_UpdateAfter4sec_NoDeviceAfter10Sec)
{
	AdvertisingDevicesRegistry registry(-75, std::chrono::seconds(5), std::chrono::seconds(1));
	AdvertisementData data
	{
		"DumbDevice",
		get_device_id(),
		-70,
	{UUID{"11111111-2222-3333-4444-555555555555"}, UUID{"AAAAAAAA-BBBB-CCCC-DDDD-EEEEEEEEEEEE"}},
		std::chrono::steady_clock::now(),
		std::unordered_map<int, std::vector<std::byte>>{ {14, std::vector{std::byte{0x00}}}, {28, std::vector{std::byte{0x11}}}}
	};
	registry.onAdvertisementReceived({ data });
	std::this_thread::sleep_for(std::chrono::seconds(4));
	data.TimeStamp = std::chrono::steady_clock::now();
	registry.onAdvertisementReceived({ data });
	std::this_thread::sleep_for(std::chrono::seconds(6));
	auto devices = registry.advertisingDevices();
	auto result = std::find_if(devices.begin(), devices.end(), [&data](const auto& device_advertisement)
		{
			return data.Identifier == device_advertisement.Identifier;
		});

	ASSERT_EQ(result, devices.end());
}