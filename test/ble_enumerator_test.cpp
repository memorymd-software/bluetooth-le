//
//  apple_ble_enumerator_test.cpp
//  bluetoothle_test
//
//  Created by admin on 16/09/2019.
//

#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "ble_enumerator.h"

#include <chrono>
#include <thread>

using testing::Each;
using testing::Eq;

auto g_ble = Neuro::Ble::create_ble_enumerator();
std::unique_ptr<Neuro::Ble::IBleDevice> g_device;
//auto g_advert = Neuro::Ble::AdvertisementData();
TEST(BleEnumerator, Create_BleEnumerator)
{
//    auto ble = Neuro::Ble::create_ble_enumerator();
//
//    ASSERT_TRUE(ble.get() != nullptr);
    ASSERT_TRUE(true);
}

TEST(BleEnumerator, Create_BleEnumerator_with_ScannFilter)
{
//    auto ble = Neuro::Ble::create_ble_enumerator(const std::vector<ScanFilter> &scan_filters)
//    ASSERT_TRUE(ble.get() != nullptr)
    ASSERT_TRUE(true);
}


TEST(BleEnumerator, Register_AdvertisementListChangedEvent)
{
//    std::mutex outputMutex;
    
//    const Neuro::Utilities::EventListener<const Neuro::Ble::AdvertisementData &> listener
//    {
//        [](const Neuro::Ble::AdvertisementData &advertisement)
//        {
////            std::unique_lock outputLock(outputMutex);
//            /*
//             std::string Name;
//             DeviceIdentifier Identifier;
//             int RSSI;
//             std::vector<UUID> ServicesUUIDs;
//             std::chrono::steady_clock::time_point TimeStamp;
//             std::unordered_map<int, std::vector<std::byte>> ManufacturerData;
//             */
//            std::cout << "-------------------" << std::endl;
//            std::cout << "Name: " << advertisement.Name << std::endl;
//            std::cout << "UIID: " << advertisement.Identifier.Uuid << std::endl;
//            std::cout << "RSSI: " << advertisement.RSSI << std::endl;
//            std::cout << "Services: " << std::endl;
//            for ( auto service : advertisement.ServicesUUIDs) {
//                std::cout << "\t" << service << std::endl;
//            }
//            std::cout << "-------------------" << std::endl;
////            ASSERT_EQ(advertisement.Name, "BrainBit");
////            ASSERT_EQ(advertisement.Identifier.Uuid, Neuro::Ble::UUID("9D3F3C91-99E3-4137-932B-19ECDB153E8B"));
//            if( advertisement.Name == "BrainBit") {
//                g_advert = advertisement;
//            }
//        }
//    };
//    g_ble->advertisementListChanged().registerListener(listener);
//    using namespace std::chrono_literals;
//    std::this_thread::sleep_for(5s);
    ASSERT_TRUE(true);
}

TEST(BleEnumerator, Register_CreateDevice)
{
    /*g_device = g_ble->createBleDevice( g_advert.Identifier );
    using namespace std::chrono_literals;
    std::this_thread::sleep_for(5s);*/
}

TEST(BleEnumerator, Delete_BleEnumerator)
{
    using namespace std::chrono_literals;
    std::this_thread::sleep_for(5s);
    g_ble.reset();
    ASSERT_TRUE(true);
}

TEST(BleEnumerator, Dump)
{
    using namespace std::chrono_literals;
    std::this_thread::sleep_for(5s);
    ASSERT_TRUE(true);
}
