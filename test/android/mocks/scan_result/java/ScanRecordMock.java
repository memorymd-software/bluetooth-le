package com.memorymd.bluetoothlemocks;

import com.memorymd.bluetoothle.IScanRecord;

import java.util.List;
import android.util.SparseArray;
import android.os.ParcelUuid;

public class ScanRecordMock implements IScanRecord
{
	private final String mDeviceName;
	private final SparseArray<byte[]> mManufacturerData;
	private final List<ParcelUuid> mServiceUuids;

	public ScanRecordMock(String deviceName, SparseArray<byte[]> manuData, List<ParcelUuid> serviceUuids)
	{
		mDeviceName = deviceName;
		mManufacturerData = manuData;
		mServiceUuids = serviceUuids;
	}
	
	@Override
	public SparseArray<byte[]> getManufacturerSpecificData()
	{
		return mManufacturerData;
	}

	@Override
	public String getDeviceName()
	{
		return mDeviceName;
	}

	@Override
	public List<ParcelUuid> getServiceUuids()
	{
		return mServiceUuids;
	}
}