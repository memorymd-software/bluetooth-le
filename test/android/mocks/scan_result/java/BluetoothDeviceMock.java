package com.memorymd.bluetoothlemocks;

import com.memorymd.bluetoothle.IBluetoothDevice;

public class BluetoothDeviceMock implements IBluetoothDevice
{
	private final String mAddress;

	public BluetoothDeviceMock(String address)
	{
		mAddress = address;
	}

	@Override
	public String getAddress()
	{
		return mAddress;
	}
}