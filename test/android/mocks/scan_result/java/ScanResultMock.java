package com.memorymd.bluetoothlemocks;

import com.memorymd.bluetoothle.IScanResult;
import com.memorymd.bluetoothle.IScanRecord;
import com.memorymd.bluetoothle.IBluetoothDevice;

public class ScanResultMock implements IScanResult
{
	private final IBluetoothDevice mDevice;
	private final IScanRecord mScanRecord;
	private final int mRssi;
	private final long mTimestamp;

	public ScanResultMock(IBluetoothDevice device, IScanRecord scanRecord, int rssi, long timestamp)
	{
		mDevice = device;
		mScanRecord = scanRecord;
		mRssi = rssi;
		mTimestamp = timestamp;
	}

	@Override
	public IBluetoothDevice getDevice()
	{
		return mDevice;
	}
	
	@Override
	public IScanRecord getScanRecord()
	{
		return mScanRecord;
	}

	@Override
	public int getRssi()
	{
		return mRssi;
	}
	
	@Override
	public long getTimestampNanos()
	{
		return mTimestamp;
	}
}