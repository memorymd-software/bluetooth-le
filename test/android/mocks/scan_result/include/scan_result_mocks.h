#ifndef SCAN_RESULT_MOCKS
#define SCAN_RESULT_MOCKS

#include "jni.h"
#include "advertisement_data.h"

jobject create_scan_result(JNIEnv *env, const Neuro::Ble::AdvertisementData& template_data);

#endif