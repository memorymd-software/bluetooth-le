#include "scan_result_mocks.h"
#include "jni_object.h"
#include "jni_string.h"
#include "jni_array.h"
#include "ble_device_address.h"
#include "cxx_scan_callback.h"

using Neuro::Ble::Impl::BleScanError;
using Neuro::Ble::AdvertisementData;
using Neuro::Ble::BleDeviceAddress;
using Neuro::Ble::DeviceIdentifier;
using Neuro::Ble::UUID;
using Neuro::Utilities::EventListener;
using Neuro::Ble::Impl::Jni::JavaObject;
using Neuro::Ble::Impl::Jni::JavaArray;
using Neuro::Ble::Impl::Jni::JavaClass;
using Neuro::Ble::Impl::Jni::JavaString;
using Neuro::Ble::Impl::Jni::call_and_handle_java_exceptions;

static jobject create_bluetooth_device(JNIEnv* env, const BleDeviceAddress& address)
{
	JavaString deviceAddressString(env, to_string(address));
	JavaClass bleDeviceClass(env, "com/memorymd/bluetoothlemocks/BluetoothDeviceMock");
	return JavaObject(env, bleDeviceClass, "(Ljava/lang/String;)V", static_cast<jstring>(deviceAddressString));
}

static jobject create_sparse_array(JNIEnv* env, const std::unordered_map<int, std::vector<std::byte>> &manufacturer_data)
{
	JavaClass sparseArrayClass(env, "android/util/SparseArray");
	JavaObject sparseArray(env, sparseArrayClass, "()V");
	for (const auto &idDataPair : manufacturer_data)
	{
		JavaArray<jbyte> data(env, idDataPair.second.begin(), idDataPair.second.size());
		sparseArray.callMethod<void>(env, "append", "(ILjava/lang/Object;)V", idDataPair.first, data.javaArray());
	}

	return sparseArray;
}

static jobject create_uuid_list(JNIEnv* env, const std::vector<UUID>& uuids)
{
	JavaClass arrayListClass(env, "java/util/ArrayList");
	JavaObject arrayList(env, arrayListClass, "()V");
	JavaClass parcelUUIDClass(env, "android/os/ParcelUuid");
	JavaClass uuidClass(env, "java/util/UUID");
	for (const UUID& uuid : uuids)
	{
		JavaString uuidJavaString(env, to_string(uuid));
		auto javaUuid = uuidClass.callStaticMethod<jobject>(env, "fromString", "(Ljava/lang/String;)Ljava/util/UUID;", static_cast<jstring>(uuidJavaString));
		JavaObject parcelUUID(env, parcelUUIDClass, "(Ljava/util/UUID;)V", javaUuid);
		arrayList.callMethod<jboolean>(env, "add", "(Ljava/lang/Object;)Z", static_cast<jobject>(parcelUUID));
	}

	return arrayList;
}

static jobject create_scan_record(JNIEnv* env, const AdvertisementData& template_data)
{
	JavaClass scanRecordClass(env, "com/memorymd/bluetoothlemocks/ScanRecordMock");
	return JavaObject(env, scanRecordClass, "(Ljava/lang/String;Landroid/util/SparseArray;Ljava/util/List;)V",
		static_cast<jstring>(JavaString(env, template_data.Name)),
		create_sparse_array(env, template_data.ManufacturerData),
		create_uuid_list(env, template_data.ServicesUUIDs)
	);
}

jobject create_scan_result(JNIEnv* env, const AdvertisementData& template_data)
{
	JavaClass scanResultClass(env, "com/memorymd/bluetoothlemocks/ScanResultMock");
	JavaObject scanResult(env, scanResultClass, "(Lcom/memorymd/bluetoothle/IBluetoothDevice;Lcom/memorymd/bluetoothle/IScanRecord;IJ)V",
		create_bluetooth_device(env, template_data.Identifier.Address),
		create_scan_record(env, template_data),
		static_cast<jint>(template_data.RSSI),
		std::chrono::duration_cast<std::chrono::nanoseconds>(template_data.TimeStamp.time_since_epoch()).count()
	);

	return Neuro::Ble::Impl::Jni::call_and_handle_java_exceptions(env, &JNIEnv::NewGlobalRef, static_cast<jobject>(scanResult));
}

