#include "android/log.h"
#include "jni_exceptions.h"
#include "gtest/gtest.h"
#include <string>
#include <thread>

using Neuro::Ble::Impl::Jni::call_and_handle_java_exceptions;

static auto initializeRedirect(const std::string& token)
{
	using namespace std;
	__android_log_print(ANDROID_LOG_INFO, token.c_str(), "Setting up STDOUT pipe to adb log");
	int stdoutPipe[2];
	pipe(stdoutPipe);
	dup2(stdoutPipe[1], STDOUT_FILENO);
	return fdopen(stdoutPipe[0], "r");
}

static void redirectStdOutLoop(FILE* exitEndFd, std::string token)
{
	using namespace std;
	stringstream outStm;
	// It is okay to keep it running like this.
	while (true) {
		auto c = fgetc(exitEndFd);
		if (c == '\n' || c == EOF) {
			__android_log_print(ANDROID_LOG_INFO, token.c_str(), "%s", outStm.str().c_str());
			outStm.str("");
		}
		else {
			outStm << (char)c;
		}
		if (c == EOF) {
			break;
		}
	}
}

void redirectTestOutputToLog(const std::string &token)
{
	auto pipeHandle = initializeRedirect(token);
	std::thread redirectThread(&redirectStdOutLoop, pipeHandle, token);
	redirectThread.detach();
}

std::string extractToken(JNIEnv* env, jstring tokenString)
{
	try
	{
		const char* tokenChars = call_and_handle_java_exceptions(env, &JNIEnv::GetStringUTFChars, tokenString, static_cast<jboolean *>(nullptr));
		std::string token(tokenChars);
		call_and_handle_java_exceptions(env, &JNIEnv::ReleaseStringUTFChars, tokenString, tokenChars);
		return token;
	}
	catch (std::exception &e)
	{
		throw std::runtime_error(std::string("Unable to extract session token: ") + e.what());
	}
	catch (...)
	{
		throw std::runtime_error("Unable to extract session token");
	}
}

JNIEnv* GlobalEnv = nullptr;

extern "C"
{
	JNIEXPORT void JNICALL Java_com_memorymd_unittestapp_TestActivity_performTests
	(
		JNIEnv* env,
		jobject activity,
		jstring tokenString,
		jobject context
	)
	{
		try
		{
			GlobalEnv = env;
			auto token = extractToken(env, tokenString);
			redirectTestOutputToLog(token);
			auto result = RUN_ALL_TESTS();
			std::this_thread::sleep_for(std::chrono::milliseconds(500));
			__android_log_print(ANDROID_LOG_ERROR, token.c_str(), "%d", result);
		}
		catch (std::exception & e)
		{
			__android_log_print(ANDROID_LOG_ERROR, "UnitTestApp", "TestApplicationFailed: %s", e.what());
		}
		catch (...)
		{
			__android_log_print(ANDROID_LOG_ERROR, "UnitTestApp", "TestApplicationFailed");
		}
	}
}