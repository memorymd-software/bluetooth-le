package com.memorymd.unittestapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.util.List;

public class TestActivity extends Activity
{
	@Override
    protected void onStart() {
        super.onStart();
		try
		{			
			Intent intent = getIntent();
			String token = intent.getStringExtra("token");
			System.loadLibrary("android_unit_tests");
			Log.i(token, "Test application started");
			performTests(token, this);
			finish();
		}
		catch(Exception e)
		{
			Log.e("UnitTestApp", String.format("Test application failed: %s", e.getMessage()));
		}
    }

	private native void performTests(String token, Context context);
}