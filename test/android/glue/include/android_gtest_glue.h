#ifndef ANDROID_GTEST_GLUE_H
#define ANDROID_GTEST_GLUE_H

#include "jni.h"
#include <memory>

namespace TestGlue
{
	struct TestEnvironment final
	{
		template<typename Callable>
		static auto execute(Callable func) {
			TestEnvironment env;
			return func(static_cast<JNIEnv *>(env));
		}		

	private:
		TestEnvironment();
		operator JNIEnv* () const noexcept;
		std::unique_ptr<JNIEnv, void(*)(JNIEnv*)> mEnvironment;
	};
}

#endif
