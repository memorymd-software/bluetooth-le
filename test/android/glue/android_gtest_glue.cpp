#include <android_native_app_glue.h>
#include "android/log.h"
#include "jni_object.h"
#include <iostream>
#include <iomanip>
#include <sys/types.h>
#include <unistd.h>
#include <sstream>
#include <thread>
#include "gtest/gtest.h"
#include "android_gtest_glue.h"


namespace TestGlue
{
	namespace Impl
	{
		auto initializeRedirect(std::string token)
		{
			using namespace std;
			__android_log_print(ANDROID_LOG_INFO, token.c_str(), "Setting up STDOUT pipe to adb log");
			int stdoutPipe[2];
			pipe(stdoutPipe);
			dup2(stdoutPipe[1], STDOUT_FILENO);
			return fdopen(stdoutPipe[0], "r");
		}

		void redirectStdOutLoop(FILE* exitEndFd, std::string token)
		{
			using namespace std;
			stringstream outStm;
			// It is okay to keep it running like this.
			while (true) {
				auto c = fgetc(exitEndFd);
				if (c == '\n' || c == EOF) {
					__android_log_print(ANDROID_LOG_INFO, token.c_str(), "%s", outStm.str().c_str());
					outStm.str("");
				}
				else {
					outStm << (char)c;
				}
				if (c == EOF) {
					break;
				}
			}
		}

		static android_app * GlobalState = nullptr;
		static jobject GlobalClassLoader = nullptr;

		std::string get_session_token(android_app* state)
		{
			return TestEnvironment::execute
			(
				[state](auto env)
				{
					jobject me = state->activity->clazz;

					jclass acl = env->GetObjectClass(me); //class pointer of NativeActivity
					jmethodID giid = env->GetMethodID(acl, "getIntent", "()Landroid/content/Intent;");
					jobject intent = env->CallObjectMethod(me, giid); //Got our intent

					jclass icl = env->GetObjectClass(intent); //class pointer of Intent
					jmethodID gseid = env->GetMethodID(icl, "getStringExtra", "(Ljava/lang/String;)Ljava/lang/String;");

					jstring jsParam1 = static_cast<jstring>(env->CallObjectMethod(intent, gseid, env->NewStringUTF("token")));
					const char* Param1 = env->GetStringUTFChars(jsParam1, 0);
					std::string result(Param1);
					env->ReleaseStringUTFChars(jsParam1, Param1);
					return result;
				}
			);
		}

		void initialize_global_class_loader()
		{
			using Neuro::Ble::Impl::Jni::JavaObject;
			using Neuro::Ble::Impl::Jni::JavaClass;
			using Neuro::Ble::Impl::Jni::call_and_handle_java_exceptions;

			JNIEnv* env;
			auto getEnvStat = GlobalState->activity->vm->GetEnv((void**)&env, JNI_VERSION_1_6);
			if (getEnvStat == JNI_EDETACHED) {
				if (GlobalState->activity->vm->AttachCurrentThread(&env, nullptr) != 0) {
					throw std::runtime_error("Unable to attach current thread to JVM");
				}
			}
			JavaObject activity(GlobalState->activity->clazz);
			JavaObject classLoader(activity.callMethod<jobject>(env, "getClassLoader", "()Ljava/lang/ClassLoader;"));
			GlobalClassLoader = env->NewGlobalRef(classLoader);
		}

		void set_native_class_loader(JNIEnv* env)
		{
			using Neuro::Ble::Impl::Jni::JavaObject;
			using Neuro::Ble::Impl::Jni::JavaClass;
			using Neuro::Ble::Impl::Jni::call_and_handle_java_exceptions;

			JavaClass threadClass(env, "java/lang/Thread");
			JavaObject currentThread(threadClass.callStaticMethod<jobject>(env, "currentThread", "()Ljava/lang/Thread;"));
			currentThread.callMethod<jobject>(env, "setContextClassLoader", "(Ljava/lang/ClassLoader;)V", GlobalClassLoader);
		}

		void do_nothing_detatcher(JNIEnv *) noexcept
		{ 
			/*do nothing*/ 
		}

		void thread_detatcher(JNIEnv* env) noexcept 
		{
			GlobalState->activity->vm->DetachCurrentThread(); 
		}

		std::unique_ptr<JNIEnv, void(*)(JNIEnv*)> create_environment_ptr() 
		{
			if (GlobalState == nullptr)
			{
				throw std::runtime_error("Global appkication state pointer is not initialized");
			}

			JNIEnv* env;
			auto getEnvStat = GlobalState->activity->vm->GetEnv((void**)&env, JNI_VERSION_1_6);
			if (getEnvStat == JNI_EDETACHED) {
				if (GlobalState->activity->vm->AttachCurrentThread(&env, nullptr) != 0) {
					throw std::runtime_error("Unable to attach current thread to JVM");
				}
				set_native_class_loader(env);
				return std::unique_ptr<JNIEnv, void(*)(JNIEnv*)>(env, &thread_detatcher);
			}
			set_native_class_loader(env);
			return std::unique_ptr<JNIEnv, void(*)(JNIEnv*)>(env, &do_nothing_detatcher);
		}
	}

	TestEnvironment::TestEnvironment():
		mEnvironment(Impl::create_environment_ptr())
	{}

	TestEnvironment::operator JNIEnv*() const noexcept
	{
		return mEnvironment.get();
	}

	std::string initialize(android_app* state)
	{
		Impl::GlobalState = state;
		Impl::initialize_global_class_loader();
		auto token = Impl::get_session_token(state);
		auto pipeHandle = Impl::initializeRedirect(token);
		std::thread redirectThread(&Impl::redirectStdOutLoop, pipeHandle, token);
		redirectThread.detach();
		return token;
	}
}

void android_main(android_app* state)
{
	try
	{
		auto token = TestGlue::initialize(state);
		__android_log_print(ANDROID_LOG_INFO, token.c_str(), "Starting test application");				
		auto result = RUN_ALL_TESTS();
		__android_log_print(ANDROID_LOG_ERROR, token.c_str(), "%d", result);
		ANativeActivity_finish(state->activity);
	}
	catch (std::exception & e)
	{
		__android_log_print(ANDROID_LOG_ERROR, "AndroidUnitTesting", "Testing application failed: %s", e.what());
	}
}