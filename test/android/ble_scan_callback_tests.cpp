#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "ble_scan_callback.h"
#include "scan_result_mocks.h"
#include "jni_object.h"
#include "jni_string.h"

using Neuro::Ble::Impl::BleScanCallback;
using Neuro::Ble::Impl::BleScanError;
using Neuro::Ble::AdvertisementData;
using Neuro::Ble::BleDeviceAddress;
using Neuro::Ble::DeviceIdentifier;
using Neuro::Ble::UUID;
using Neuro::Utilities::EventListener;
using Neuro::Ble::Impl::Jni::JavaObject;
using Neuro::Ble::Impl::Jni::JavaClass;
using Neuro::Ble::Impl::Jni::JavaString;

constexpr jint CALLBACK_TYPE_MATCH_LOST = 4;
constexpr jint CALLBACK_TYPE_ALL_MATCHES = 1;
constexpr jint CALLBACK_TYPE_FIRST_MATCH = 2;

extern JNIEnv* GlobalEnv;

TEST(BleScanCallbackTests, Create_NullEnv_InvalidArgumentException)
{
	using Neuro::Ble::Impl::BleScanCallback;
	ASSERT_THROW({ BleScanCallback(nullptr); }, std::invalid_argument);
}

TEST(BleScanCallbackTests, Create_ValidEnv_NoException)
{
	using Neuro::Ble::Impl::BleScanCallback;
	ASSERT_NO_THROW({  BleScanCallback{  GlobalEnv }; });
}

TEST(BleScanCallbackTests, JavaCallback_onScanFailed_ValidErrorCodes_AdvertisementReceived_NoNotificationReceived)
{
	BleScanCallback callback(GlobalEnv, "com/memorymd/bluetoothle/BleScanCallback");
	bool advertisementReceived{ false };
	EventListener<const std::vector<AdvertisementData>&> advertisementReceivedListener
	{
		[&advertisementReceived](const auto&)
		{
			advertisementReceived = true;
		}
	};
	callback.advertisementsReceived().registerListener(advertisementReceivedListener);
	auto javaCallbackObject = callback.javaCallback().javaObject();

	javaCallbackObject.callMethod<void>(GlobalEnv, "onScanFailed", "(I)V", static_cast<jint>(BleScanError::ALREADY_STARTED));
	javaCallbackObject.callMethod<void>(GlobalEnv, "onScanFailed", "(I)V", static_cast<jint>(BleScanError::APPLICATION_REGISTRATION_FAILED));
	javaCallbackObject.callMethod<void>(GlobalEnv, "onScanFailed", "(I)V", static_cast<jint>(BleScanError::FEATURE_UNSUPPORTED));
	javaCallbackObject.callMethod<void>(GlobalEnv, "onScanFailed", "(I)V", static_cast<jint>(BleScanError::INTERNAL_ERROR));

	ASSERT_FALSE(advertisementReceived);
}

TEST(BleScanCallbackTests, JavaCallback_onScanFailed_InvalidErrorCodes_AdvertisementReceived_NoNotificationReceived)
{
	BleScanCallback callback(GlobalEnv, "com/memorymd/bluetoothle/BleScanCallback");
	bool advertisementReceived{ false };
	EventListener<const std::vector<AdvertisementData>&> advertisementReceivedListener
	{
		[&advertisementReceived](const auto&)
		{
			advertisementReceived = true;
		}
	};
	callback.advertisementsReceived().registerListener(advertisementReceivedListener);
	auto javaCallbackObject = callback.javaCallback().javaObject();

	javaCallbackObject.callMethod<void>(GlobalEnv, "onScanFailed", "(I)V", -1);
	javaCallbackObject.callMethod<void>(GlobalEnv, "onScanFailed", "(I)V", -200);
	javaCallbackObject.callMethod<void>(GlobalEnv, "onScanFailed", "(I)V", 6);
	javaCallbackObject.callMethod<void>(GlobalEnv, "onScanFailed", "(I)V", 5);
	javaCallbackObject.callMethod<void>(GlobalEnv, "onScanFailed", "(I)V", 15);

	ASSERT_FALSE(advertisementReceived);
}

TEST(BleScanCallbackTests, JavaCallback_onScanFailed_InvalidErrorCodes_ScanErrorOccured_NoNotificationReceived)
{
	BleScanCallback callback(GlobalEnv, "com/memorymd/bluetoothle/BleScanCallback");
	bool scanErrorOccuredReceived{ false };
	const EventListener<BleScanError> scanErrorOccuredListener
	{
		[&scanErrorOccuredReceived](auto)
		{
			scanErrorOccuredReceived = true;
		}
	};
	callback.scanErrorOccured().registerListener(scanErrorOccuredListener);
	auto javaCallbackObject = callback.javaCallback().javaObject();

	javaCallbackObject.callMethod<void>(GlobalEnv, "onScanFailed", "(I)V", -1);
	javaCallbackObject.callMethod<void>(GlobalEnv, "onScanFailed", "(I)V", -200);
	javaCallbackObject.callMethod<void>(GlobalEnv, "onScanFailed", "(I)V", 6);
	javaCallbackObject.callMethod<void>(GlobalEnv, "onScanFailed", "(I)V", 5);
	javaCallbackObject.callMethod<void>(GlobalEnv, "onScanFailed", "(I)V", 15);

	ASSERT_FALSE(scanErrorOccuredReceived);
}

TEST(BleScanCallbackTests, JavaCallback_onScanFailed_AlreadyStartedError_ScanErrorOccured_ValidErrorValueReceived)
{
	BleScanCallback callback(GlobalEnv, "com/memorymd/bluetoothle/BleScanCallback");
	bool scanErrorOccuredReceived{ false };
	const EventListener<BleScanError> scanErrorOccuredListener
	{
		[&scanErrorOccuredReceived](auto scanError)
		{
			scanErrorOccuredReceived = true;
			ASSERT_EQ(scanError, BleScanError::ALREADY_STARTED);
		}
	};
	callback.scanErrorOccured().registerListener(scanErrorOccuredListener);
	auto javaCallbackObject = callback.javaCallback().javaObject();

	javaCallbackObject.callMethod<void>(GlobalEnv, "onScanFailed", "(I)V", static_cast<jint>(BleScanError::ALREADY_STARTED));

	ASSERT_TRUE(scanErrorOccuredReceived);
}

TEST(BleScanCallbackTests, JavaCallback_onScanFailed_ApplicationRegError_ScanErrorOccured_ValidErrorValueReceived)
{
	BleScanCallback callback(GlobalEnv, "com/memorymd/bluetoothle/BleScanCallback");
	bool scanErrorOccuredReceived{ false };
	const EventListener<BleScanError> scanErrorOccuredListener
	{
		[&scanErrorOccuredReceived](auto scanError)
		{
			scanErrorOccuredReceived = true;
			ASSERT_EQ(scanError, BleScanError::APPLICATION_REGISTRATION_FAILED);
		}
	};
	callback.scanErrorOccured().registerListener(scanErrorOccuredListener);
	auto javaCallbackObject = callback.javaCallback().javaObject();

	javaCallbackObject.callMethod<void>(GlobalEnv, "onScanFailed", "(I)V", static_cast<jint>(BleScanError::APPLICATION_REGISTRATION_FAILED));

	ASSERT_TRUE(scanErrorOccuredReceived);
}

TEST(BleScanCallbackTests, JavaCallback_onScanFailed_FeatureUnsupported_ScanErrorOccured_ValidErrorValueReceived)
{
	BleScanCallback callback(GlobalEnv, "com/memorymd/bluetoothle/BleScanCallback");
	bool scanErrorOccuredReceived{ false };
	const EventListener<BleScanError> scanErrorOccuredListener
	{
		[&scanErrorOccuredReceived](auto scanError)
		{
			scanErrorOccuredReceived = true;
			ASSERT_EQ(scanError, BleScanError::FEATURE_UNSUPPORTED);
		}
	};
	callback.scanErrorOccured().registerListener(scanErrorOccuredListener);
	auto javaCallbackObject = callback.javaCallback().javaObject();

	javaCallbackObject.callMethod<void>(GlobalEnv, "onScanFailed", "(I)V", static_cast<jint>(BleScanError::FEATURE_UNSUPPORTED));

	ASSERT_TRUE(scanErrorOccuredReceived);
}

TEST(BleScanCallbackTests, JavaCallback_onScanFailed_InternalError_ScanErrorOccured_ValidErrorValueReceived)
{
	BleScanCallback callback(GlobalEnv, "com/memorymd/bluetoothle/BleScanCallback");
	bool scanErrorOccuredReceived{ false };
	const EventListener<BleScanError> scanErrorOccuredListener
	{
		[&scanErrorOccuredReceived](auto scanError)
		{
			scanErrorOccuredReceived = true;
			ASSERT_EQ(scanError, BleScanError::INTERNAL_ERROR);
		}
	};
	callback.scanErrorOccured().registerListener(scanErrorOccuredListener);
	auto javaCallbackObject = callback.javaCallback().javaObject();

	javaCallbackObject.callMethod<void>(GlobalEnv, "onScanFailed", "(I)V", static_cast<jint>(BleScanError::INTERNAL_ERROR));

	ASSERT_TRUE(scanErrorOccuredReceived);
}

TEST(BleScanCallbackTests, JavaCallback_onScanResult_MatchLost_NullScanResult_AdvertisementReceived_NoException_NoNotification)
{	
	BleScanCallback callback(GlobalEnv, "com/memorymd/bluetoothle/BleScanCallback");
	bool advertisementReceived{ false };
	EventListener<const std::vector<AdvertisementData>&> advertisementReceivedListener
	{
		[&advertisementReceived](const auto&)
		{
			advertisementReceived = true;
		}
	};
	callback.advertisementsReceived().registerListener(advertisementReceivedListener);
	auto javaCallbackObject = callback.javaCallback().javaObject();

	javaCallbackObject.callMethod<void>(GlobalEnv, "onScanResult", "(ILcom/memorymd/bluetoothle/IScanResult;)V", CALLBACK_TYPE_MATCH_LOST, nullptr);
	ASSERT_FALSE(advertisementReceived);
}

TEST(BleScanCallbackTests, JavaCallback_onScanResult_AllMatches_NullScanResult_AdvertisementReceived_NoException_NoNotification)
{
	BleScanCallback callback(GlobalEnv, "com/memorymd/bluetoothle/BleScanCallback");
	bool advertisementReceived{ false };
	EventListener<const std::vector<AdvertisementData>&> advertisementReceivedListener
	{
		[&advertisementReceived](const auto&)
		{
			advertisementReceived = true;
		}
	};
	callback.advertisementsReceived().registerListener(advertisementReceivedListener);
	auto javaCallbackObject = callback.javaCallback().javaObject();

	javaCallbackObject.callMethod<void>(GlobalEnv, "onScanResult", "(ILcom/memorymd/bluetoothle/IScanResult;)V", CALLBACK_TYPE_ALL_MATCHES, nullptr);
	ASSERT_FALSE(advertisementReceived);
}

TEST(BleScanCallbackTests, JavaCallback_onScanResult_FirstMatch_NullScanResult_AdvertisementReceived_NoException_NoNotification)
{
	BleScanCallback callback(GlobalEnv, "com/memorymd/bluetoothle/BleScanCallback");
	bool advertisementReceived{ false };
	EventListener<const std::vector<AdvertisementData>&> advertisementReceivedListener
	{
		[&advertisementReceived](const auto&)
		{
			advertisementReceived = true;
		}
	};
	callback.advertisementsReceived().registerListener(advertisementReceivedListener);
	auto javaCallbackObject = callback.javaCallback().javaObject();

	javaCallbackObject.callMethod<void>(GlobalEnv, "onScanResult", "(ILcom/memorymd/bluetoothle/IScanResult;)V", CALLBACK_TYPE_FIRST_MATCH, nullptr);
	ASSERT_FALSE(advertisementReceived);
}

TEST(BleScanCallbackTests, JavaCallback_onScanResult_FirstMatch_ValidScanResult_OneAdvertisementReceived_ValidAdvertisementData)
{
	BleScanCallback callback(GlobalEnv, "com/memorymd/bluetoothle/BleScanCallback");
	bool advertisementReceived{ false };	
	const AdvertisementData patternAdvertisement
	{
		"DumbDevice",
		DeviceIdentifier { BleDeviceAddress { "11:22:33:44:55:66" } },
		-70,
	{UUID{"11111111-2222-3333-4444-555555555555"}, UUID{"AAAAAAAA-BBBB-CCCC-DDDD-EEEEEEEEEEEE"}},
		std::chrono::steady_clock::time_point{ std::chrono::seconds{ 1488 } },
		std::unordered_map<int, std::vector<std::byte>>{{14, std::vector{std::byte{0x00}}}, {28, std::vector{std::byte{0x11}}}}
	};
	EventListener<const std::vector<AdvertisementData>&> advertisementReceivedListener
	{
		[&advertisementReceived, &patternAdvertisement](const std::vector<AdvertisementData>& advertisement_data)
		{
			advertisementReceived = true;
			ASSERT_EQ(advertisement_data.size(), 1);
			ASSERT_EQ(advertisement_data[0].Name, patternAdvertisement.Name);
			ASSERT_EQ(advertisement_data[0].Identifier, patternAdvertisement.Identifier);
			ASSERT_EQ(advertisement_data[0].RSSI, patternAdvertisement.RSSI);
			for(size_t i = 0; i < advertisement_data[0].ServicesUUIDs.size(); ++i)
			{
				ASSERT_EQ(advertisement_data[0].ServicesUUIDs[i], patternAdvertisement.ServicesUUIDs[i]);
			}
		}
	};
	callback.advertisementsReceived().registerListener(advertisementReceivedListener);
	auto javaCallbackObject = callback.javaCallback().javaObject();
	
	
	javaCallbackObject.callMethod<void>(GlobalEnv, "onScanResult", "(ILcom/memorymd/bluetoothle/IScanResult;)V", CALLBACK_TYPE_FIRST_MATCH, create_scan_result(GlobalEnv, patternAdvertisement));
	
	ASSERT_TRUE(advertisementReceived);
}