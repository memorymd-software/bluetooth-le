#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "ble_device_address.h"

using testing::Each;
using testing::Eq;

TEST(BleDeviceAddress, Create_EmptyString_InvalidArgumentException)
{
    using Neuro::Ble::BleDeviceAddress;
    ASSERT_THROW({ BleDeviceAddress(""); }, std::invalid_argument);
}

TEST(BleDeviceAddress, Create_InvalidFormatSymbolsLengthString_InvalidArgumentException)
{
    using Neuro::Ble::BleDeviceAddress;
    ASSERT_THROW({ BleDeviceAddress("2w:45::abe8:4f:60:"); }, std::invalid_argument);
}

TEST(BleDeviceAddress, Create_InvalidFormatLengthString_InvalidArgumentException)
{
    using Neuro::Ble::BleDeviceAddress;
    ASSERT_THROW({ BleDeviceAddress("AABBCCDDEEFF"); }, std::invalid_argument);
}

TEST(BleDeviceAddress, Create_InvalidSymbolsString_InvalidArgumentException)
{
    using Neuro::Ble::BleDeviceAddress;
    ASSERT_THROW({ BleDeviceAddress("14:t6:7g:89:12:7a"); }, std::invalid_argument);
}


TEST(BleDeviceAddress, Create_ZeroBleDeviceAddressString_AllBytesAreZero)
{
    using Neuro::Ble::BleDeviceAddress;
    auto zeroBleDeviceAddress = BleDeviceAddress("00:00:00:00:00:00");
    ASSERT_THAT(zeroBleDeviceAddress, Each(Eq(std::byte(0x00))));
}

TEST(BleDeviceAddress, Create_FFsBleDeviceAddressString_AllBytesAreFF)
{
    using Neuro::Ble::BleDeviceAddress;
    auto ffsBleDeviceAddress = BleDeviceAddress("FF:FF:FF:FF:FF:FF");
    ASSERT_THAT(ffsBleDeviceAddress, Each(Eq(std::byte(0xFF))));
}

TEST(BleDeviceAddress, Create_DifferentNumbersForSectionsBleDeviceAddressString_ValidSectionValues)
{
    using Neuro::Ble::BleDeviceAddress;
    auto digitsBleDeviceAddress = BleDeviceAddress("11:22:33:44:55:66");
    ASSERT_EQ(digitsBleDeviceAddress[0], std::byte(0x11));
    ASSERT_EQ(digitsBleDeviceAddress[1], std::byte(0x22));
    ASSERT_EQ(digitsBleDeviceAddress[2], std::byte(0x33));
    ASSERT_EQ(digitsBleDeviceAddress[3], std::byte(0x44));
    ASSERT_EQ(digitsBleDeviceAddress[4], std::byte(0x55));
    ASSERT_EQ(digitsBleDeviceAddress[5], std::byte(0x66));
}

TEST(BleDeviceAddress, Create_DifferentCapitalLettersForSectionsBleDeviceAddressString_ValidSectionValues)
{
    using Neuro::Ble::BleDeviceAddress;
    auto lettersBleDeviceAddress = BleDeviceAddress("AA:BB:CC:DD:EE:FF");
    ASSERT_EQ(lettersBleDeviceAddress[0], std::byte(0xAA));
    ASSERT_EQ(lettersBleDeviceAddress[1], std::byte(0xBB));
    ASSERT_EQ(lettersBleDeviceAddress[2], std::byte(0xCC));
    ASSERT_EQ(lettersBleDeviceAddress[3], std::byte(0xDD));
    ASSERT_EQ(lettersBleDeviceAddress[4], std::byte(0xEE));
    ASSERT_EQ(lettersBleDeviceAddress[5], std::byte(0xFF));
}

TEST(BleDeviceAddress, Create_DifferentLowercaseLettersForSectionsBleDeviceAddressString_ValidSectionValues)
{
    using Neuro::Ble::BleDeviceAddress;
    auto lettersBleDeviceAddress = BleDeviceAddress("aa:bb:cc:dd:ee:ff");
    ASSERT_EQ(lettersBleDeviceAddress[0], std::byte(0xAA));
    ASSERT_EQ(lettersBleDeviceAddress[1], std::byte(0xBB));
    ASSERT_EQ(lettersBleDeviceAddress[2], std::byte(0xCC));
    ASSERT_EQ(lettersBleDeviceAddress[3], std::byte(0xDD));
    ASSERT_EQ(lettersBleDeviceAddress[4], std::byte(0xEE));
    ASSERT_EQ(lettersBleDeviceAddress[5], std::byte(0xFF));
}

TEST(BleDeviceAddress, Create_ZeroBleDeviceAddressArray_AllBytesAreZero)
{
    using Neuro::Ble::BleDeviceAddress;
    auto zeroBleDeviceAddress = BleDeviceAddress
        ({
             std::byte(0x00), std::byte(0x00), std::byte(0x00),
             std::byte(0x00), std::byte(0x00), std::byte(0x00)
         });
    ASSERT_THAT(zeroBleDeviceAddress, Each(Eq(std::byte(0x00))));
}

TEST(BleDeviceAddress, Create_FFsBleDeviceAddressArray_AllBytesAreFF)
{
    using Neuro::Ble::BleDeviceAddress;
    auto ffsBleDeviceAddress = BleDeviceAddress
        ({
             std::byte(0xFF), std::byte(0xFF), std::byte(0xFF),
             std::byte(0xFF), std::byte(0xFF), std::byte(0xFF)
         });
    ASSERT_THAT(ffsBleDeviceAddress, Each(Eq(std::byte(0xFF))));
}

TEST(BleDeviceAddress, Create_DifferentNumbersForSectionsBleDeviceAddressArray_ValidSectionValues)
{
    using Neuro::Ble::BleDeviceAddress;
    auto digitsBleDeviceAddress = BleDeviceAddress
        ({
             std::byte(0x11), std::byte(0x22), std::byte(0x33),
             std::byte(0x44), std::byte(0x55), std::byte(0x66),
         });
    ASSERT_EQ(digitsBleDeviceAddress[0], std::byte(0x11));
    ASSERT_EQ(digitsBleDeviceAddress[1], std::byte(0x22));
    ASSERT_EQ(digitsBleDeviceAddress[2], std::byte(0x33));
    ASSERT_EQ(digitsBleDeviceAddress[3], std::byte(0x44));
    ASSERT_EQ(digitsBleDeviceAddress[4], std::byte(0x55));
    ASSERT_EQ(digitsBleDeviceAddress[5], std::byte(0x66));
}

TEST(BleDeviceAddress, Create_DifferentLettersForSectionsBleDeviceAddressArray_ValidSectionValues)
{
    using Neuro::Ble::BleDeviceAddress;
    auto lettersBleDeviceAddress = BleDeviceAddress
        ({
             std::byte(0xAA), std::byte(0xBB), std::byte(0xCC),
             std::byte(0xDD), std::byte(0xEE), std::byte(0xFF)
         });
    ASSERT_EQ(lettersBleDeviceAddress[0], std::byte(0xAA));
    ASSERT_EQ(lettersBleDeviceAddress[1], std::byte(0xBB));
    ASSERT_EQ(lettersBleDeviceAddress[2], std::byte(0xCC));
    ASSERT_EQ(lettersBleDeviceAddress[3], std::byte(0xDD));
    ASSERT_EQ(lettersBleDeviceAddress[4], std::byte(0xEE));
    ASSERT_EQ(lettersBleDeviceAddress[5], std::byte(0xFF));
}

TEST(BleDeviceAddress, EqualityOperator_2ConstructorsEqualValues_Equal)
{
    using Neuro::Ble::BleDeviceAddress;
    auto arrayBleDeviceAddress = BleDeviceAddress
        ({
             std::byte(0xAA), std::byte(0xBB), std::byte(0xCC),
             std::byte(0xDD), std::byte(0xEE), std::byte(0xFF)
         });
    auto stringBleDeviceAddress = BleDeviceAddress("AA:BB:CC:DD:EE:FF");

    ASSERT_EQ(arrayBleDeviceAddress, stringBleDeviceAddress);
}

TEST(BleDeviceAddress, ToString1_ConstructorFromString_StringsEqual)
{
    using Neuro::Ble::BleDeviceAddress;
    const auto addressString = "AA:BB:CC:DD:EE:FF";
    auto address = BleDeviceAddress(addressString);

    ASSERT_EQ(addressString, to_string(address));
}
TEST(BleDeviceAddress, ToString2_ConstructorFromString_StringsEqual)
{
    //AE025DC0-820B-7DAF-364C-E60FD052F3CB
    using Neuro::Ble::BleDeviceAddress;
    const auto addressString = "AE:02:C0:0B:52:0F";
    auto address = BleDeviceAddress(addressString);

    ASSERT_EQ(addressString, to_string(address));
}

TEST(BleDeviceAddress, LeftShiftOperator_ConstructorFromStringStringStream_StringsEqual)
{
    using Neuro::Ble::BleDeviceAddress;
    const auto addressString = "AA:BB:CC:DD:EE:FF";
    auto address = BleDeviceAddress(addressString);
    std::stringstream testStream;

    testStream << address;

    ASSERT_EQ(addressString, testStream.str());
}
