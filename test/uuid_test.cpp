#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "uuid.h"

using testing::Each;
using testing::Eq;

TEST(UUID, Create_EmptyString_InvalidArgumentException)
{
    using Neuro::Ble::UUID;
    ASSERT_THROW({ UUID(""); }, std::invalid_argument);
}

TEST(UUID, Create_InvalidFormatSymbolsLengthString_InvalidArgumentException)
{
    using Neuro::Ble::UUID;
    ASSERT_THROW({ UUID("2w23-45200--abe8-4f60-90c8-0d43c5gf6c0f6"); }, std::invalid_argument);
}

TEST(UUID, Create_InvalidFormatLengthString_InvalidArgumentException)
{
    using Neuro::Ble::UUID;
    ASSERT_THROW({ UUID("22345200abe84f6090c80d43c5f6c0f6"); }, std::invalid_argument);
}

TEST(UUID, Create_InvalidSymbolsString_InvalidArgumentException)
{
    using Neuro::Ble::UUID;
    ASSERT_THROW({ UUID("22g45200-abe8-4f60-90c8-0d43c5f6c0f6"); }, std::invalid_argument);
}

TEST(UUID, Create_ZeroUUIDString_AllBytesAreZero)
{
    using Neuro::Ble::UUID;
    auto zeroUUID = UUID("00000000-0000-0000-0000-000000000000");
    ASSERT_THAT(zeroUUID.low(), Each(Eq(std::byte(0x00))));
    ASSERT_THAT(zeroUUID.mid(), Each(Eq(std::byte(0x00))));
    ASSERT_THAT(zeroUUID.high(), Each(Eq(std::byte(0x00))));
    ASSERT_THAT(zeroUUID.clock(), Each(Eq(std::byte(0x00))));
    ASSERT_THAT(zeroUUID.node(), Each(Eq(std::byte(0x00))));
}

TEST(UUID, Create_FFsUUIDString_AllBytesAreFF)
{
    using Neuro::Ble::UUID;
    auto ffsUUID = UUID("FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF");
    ASSERT_THAT(ffsUUID.low(), Each(Eq(std::byte(0xFF))));
    ASSERT_THAT(ffsUUID.mid(), Each(Eq(std::byte(0xFF))));
    ASSERT_THAT(ffsUUID.high(), Each(Eq(std::byte(0xFF))));
    ASSERT_THAT(ffsUUID.clock(), Each(Eq(std::byte(0xFF))));
    ASSERT_THAT(ffsUUID.node(), Each(Eq(std::byte(0xFF))));
}

TEST(UUID, Create_DifferentNumbersForSectionsUUIDString_ValidSectionValues)
{
    using Neuro::Ble::UUID;
    auto digitsUUID = UUID("11111111-2222-3333-4444-555555555555");
    ASSERT_THAT(digitsUUID.low(), Each(Eq(std::byte(0x11))));
    ASSERT_THAT(digitsUUID.mid(), Each(Eq(std::byte(0x22))));
    ASSERT_THAT(digitsUUID.high(), Each(Eq(std::byte(0x33))));
    ASSERT_THAT(digitsUUID.clock(), Each(Eq(std::byte(0x44))));
    ASSERT_THAT(digitsUUID.node(), Each(Eq(std::byte(0x55))));
}

TEST(UUID, Create_DifferentCapitalLettersForSectionsUUIDString_ValidSectionValues)
{
    using Neuro::Ble::UUID;
    auto lettersUUID = UUID("AAAAAAAA-BBBB-CCCC-DDDD-EEEEEEEEEEEE");
    ASSERT_THAT(lettersUUID.low(), Each(Eq(std::byte(0xAA))));
    ASSERT_THAT(lettersUUID.mid(), Each(Eq(std::byte(0xBB))));
    ASSERT_THAT(lettersUUID.high(), Each(Eq(std::byte(0xCC))));
    ASSERT_THAT(lettersUUID.clock(), Each(Eq(std::byte(0xDD))));
    ASSERT_THAT(lettersUUID.node(), Each(Eq(std::byte(0xEE))));
}

TEST(UUID, Create_DifferentLowercaseLettersForSectionsUUIDString_ValidSectionValues)
{
    using Neuro::Ble::UUID;
    auto lettersUUID = UUID("aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee");
    ASSERT_THAT(lettersUUID.low(), Each(Eq(std::byte(0xAA))));
    ASSERT_THAT(lettersUUID.mid(), Each(Eq(std::byte(0xBB))));
    ASSERT_THAT(lettersUUID.high(), Each(Eq(std::byte(0xCC))));
    ASSERT_THAT(lettersUUID.clock(), Each(Eq(std::byte(0xDD))));
    ASSERT_THAT(lettersUUID.node(), Each(Eq(std::byte(0xEE))));
}

TEST(UUID, Create_ZeroUUIDArray_AllBytesAreZero)
{
    using Neuro::Ble::UUID;
    auto zeroUUID = UUID
        ({
             std::byte(0x00), std::byte(0x00), std::byte(0x00), std::byte(0x00), std::byte(0x00), std::byte(0x00),
             std::byte(0x00), std::byte(0x00), std::byte(0x00), std::byte(0x00), std::byte(0x00), std::byte(0x00),
             std::byte(0x00), std::byte(0x00), std::byte(0x00), std::byte(0x00)
         });
    ASSERT_THAT(zeroUUID.low(), Each(Eq(std::byte(0x00))));
    ASSERT_THAT(zeroUUID.mid(), Each(Eq(std::byte(0x00))));
    ASSERT_THAT(zeroUUID.high(), Each(Eq(std::byte(0x00))));
    ASSERT_THAT(zeroUUID.clock(), Each(Eq(std::byte(0x00))));
    ASSERT_THAT(zeroUUID.node(), Each(Eq(std::byte(0x00))));
}

TEST(UUID, Create_FFsUUIDArray_AllBytesAreFF)
{
    using Neuro::Ble::UUID;
    auto ffsUUID = UUID
        ({
             std::byte(0xFF), std::byte(0xFF), std::byte(0xFF), std::byte(0xFF), std::byte(0xFF), std::byte(0xFF),
             std::byte(0xFF), std::byte(0xFF), std::byte(0xFF), std::byte(0xFF), std::byte(0xFF), std::byte(0xFF),
             std::byte(0xFF), std::byte(0xFF), std::byte(0xFF), std::byte(0xFF)
         });
    ASSERT_THAT(ffsUUID.low(), Each(Eq(std::byte(0xFF))));
    ASSERT_THAT(ffsUUID.mid(), Each(Eq(std::byte(0xFF))));
    ASSERT_THAT(ffsUUID.high(), Each(Eq(std::byte(0xFF))));
    ASSERT_THAT(ffsUUID.clock(), Each(Eq(std::byte(0xFF))));
    ASSERT_THAT(ffsUUID.node(), Each(Eq(std::byte(0xFF))));
}

TEST(UUID, Create_DifferentNumbersForSectionsUUIDArray_ValidSectionValues)
{
    using Neuro::Ble::UUID;
    auto digitsUUID = UUID
        ({
             std::byte(0x11), std::byte(0x11), std::byte(0x11), std::byte(0x11),
             std::byte(0x22), std::byte(0x22),
             std::byte(0x33), std::byte(0x33),
             std::byte(0x44), std::byte(0x44),
             std::byte(0x55), std::byte(0x55), std::byte(0x55), std::byte(0x55), std::byte(0x55), std::byte(0x55)
         });
    ASSERT_THAT(digitsUUID.low(), Each(Eq(std::byte(0x11))));
    ASSERT_THAT(digitsUUID.mid(), Each(Eq(std::byte(0x22))));
    ASSERT_THAT(digitsUUID.high(), Each(Eq(std::byte(0x33))));
    ASSERT_THAT(digitsUUID.clock(), Each(Eq(std::byte(0x44))));
    ASSERT_THAT(digitsUUID.node(), Each(Eq(std::byte(0x55))));
}

TEST(UUID, Create_DifferentLettersForSectionsUUIDArray_ValidSectionValues)
{
    using Neuro::Ble::UUID;
    auto lettersUUID = UUID
        ({
             std::byte(0xAA), std::byte(0xAA), std::byte(0xAA), std::byte(0xAA),
             std::byte(0xBB), std::byte(0xBB),
             std::byte(0xCC), std::byte(0xCC),
             std::byte(0xDD), std::byte(0xDD),
             std::byte(0xEE), std::byte(0xEE), std::byte(0xEE), std::byte(0xEE), std::byte(0xEE), std::byte(0xEE)
         });
    ASSERT_THAT(lettersUUID.low(), Each(Eq(std::byte(0xAA))));
    ASSERT_THAT(lettersUUID.mid(), Each(Eq(std::byte(0xBB))));
    ASSERT_THAT(lettersUUID.high(), Each(Eq(std::byte(0xCC))));
    ASSERT_THAT(lettersUUID.clock(), Each(Eq(std::byte(0xDD))));
    ASSERT_THAT(lettersUUID.node(), Each(Eq(std::byte(0xEE))));
}

TEST(UUID, Create_ZeroSectionsUUIDSectionsArrays_ValidSectionValues)
{
    using Neuro::Ble::UUID;
    auto zeroUUID = UUID
        (
            { std::byte(0x00), std::byte(0x00), std::byte(0x00), std::byte(0x00)},
            { std::byte(0x00), std::byte(0x00) },
            { std::byte(0x00), std::byte(0x00) },
            { std::byte(0x00), std::byte(0x00) },
            { std::byte(0x00), std::byte(0x00), std::byte(0x00), std::byte(0x00), std::byte(0x00), std::byte(0x00) }
         );
    ASSERT_THAT(zeroUUID.low(), Each(Eq(std::byte(0x00))));
    ASSERT_THAT(zeroUUID.mid(), Each(Eq(std::byte(0x00))));
    ASSERT_THAT(zeroUUID.high(), Each(Eq(std::byte(0x00))));
    ASSERT_THAT(zeroUUID.clock(), Each(Eq(std::byte(0x00))));
    ASSERT_THAT(zeroUUID.node(), Each(Eq(std::byte(0x00))));
}

TEST(UUID, Create_FFsSectionsUUIDSectionsArrays_ValidSectionValues)
{
    using Neuro::Ble::UUID;
    auto ffsUUID = UUID
        (
            { std::byte(0xFF), std::byte(0xFF), std::byte(0xFF), std::byte(0xFF)},
            { std::byte(0xFF), std::byte(0xFF) },
            { std::byte(0xFF), std::byte(0xFF) },
            { std::byte(0xFF), std::byte(0xFF) },
            { std::byte(0xFF), std::byte(0xFF), std::byte(0xFF), std::byte(0xFF), std::byte(0xFF), std::byte(0xFF) }
        );
    ASSERT_THAT(ffsUUID.low(), Each(Eq(std::byte(0xFF))));
    ASSERT_THAT(ffsUUID.mid(), Each(Eq(std::byte(0xFF))));
    ASSERT_THAT(ffsUUID.high(), Each(Eq(std::byte(0xFF))));
    ASSERT_THAT(ffsUUID.clock(), Each(Eq(std::byte(0xFF))));
    ASSERT_THAT(ffsUUID.node(), Each(Eq(std::byte(0xFF))));
}

TEST(UUID, Create_DifferentNumbersForSectionsUUIDSectionsArrays_ValidSectionValues)
{
    using Neuro::Ble::UUID;
    auto digitsUUID = UUID
        (
            { std::byte(0x11), std::byte(0x11), std::byte(0x11), std::byte(0x11)},
            { std::byte(0x22), std::byte(0x22) },
            { std::byte(0x33), std::byte(0x33) },
            { std::byte(0x44), std::byte(0x44) },
            { std::byte(0x55), std::byte(0x55), std::byte(0x55), std::byte(0x55), std::byte(0x55), std::byte(0x55) }
        );
    ASSERT_THAT(digitsUUID.low(), Each(Eq(std::byte(0x11))));
    ASSERT_THAT(digitsUUID.mid(), Each(Eq(std::byte(0x22))));
    ASSERT_THAT(digitsUUID.high(), Each(Eq(std::byte(0x33))));
    ASSERT_THAT(digitsUUID.clock(), Each(Eq(std::byte(0x44))));
    ASSERT_THAT(digitsUUID.node(), Each(Eq(std::byte(0x55))));
}

TEST(UUID, Create_DifferentLettersForSectionsUUIDSectionsArrays_ValidSectionValues)
{
    using Neuro::Ble::UUID;
    auto lettersUUID = UUID
        (
            { std::byte(0xAA), std::byte(0xAA), std::byte(0xAA), std::byte(0xAA)},
            { std::byte(0xBB), std::byte(0xBB) },
            { std::byte(0xCC), std::byte(0xCC) },
            { std::byte(0xDD), std::byte(0xDD) },
            { std::byte(0xEE), std::byte(0xEE), std::byte(0xEE), std::byte(0xEE), std::byte(0xEE), std::byte(0xEE) }
        );
    ASSERT_THAT(lettersUUID.low(), Each(Eq(std::byte(0xAA))));
    ASSERT_THAT(lettersUUID.mid(), Each(Eq(std::byte(0xBB))));
    ASSERT_THAT(lettersUUID.high(), Each(Eq(std::byte(0xCC))));
    ASSERT_THAT(lettersUUID.clock(), Each(Eq(std::byte(0xDD))));
    ASSERT_THAT(lettersUUID.node(), Each(Eq(std::byte(0xEE))));
}

TEST(UUID, EqualityOperator_3ConstructorsEqualValues_All3Equal)
{
    using Neuro::Ble::UUID;
    auto sectionsUUID = UUID
        (
            { std::byte(0xAA), std::byte(0xAA), std::byte(0xAA), std::byte(0xAA)},
            { std::byte(0xBB), std::byte(0xBB) },
            { std::byte(0xCC), std::byte(0xCC) },
            { std::byte(0xDD), std::byte(0xDD) },
            { std::byte(0xEE), std::byte(0xEE), std::byte(0xEE), std::byte(0xEE), std::byte(0xEE), std::byte(0xEE) }
        );
    auto arrayUUID = UUID
        ({
             std::byte(0xAA), std::byte(0xAA), std::byte(0xAA), std::byte(0xAA),
             std::byte(0xBB), std::byte(0xBB),
             std::byte(0xCC), std::byte(0xCC),
             std::byte(0xDD), std::byte(0xDD),
             std::byte(0xEE), std::byte(0xEE), std::byte(0xEE), std::byte(0xEE), std::byte(0xEE), std::byte(0xEE)
         });
    auto stringUUID = UUID("AAAAAAAA-BBBB-CCCC-DDDD-EEEEEEEEEEEE");

    ASSERT_EQ(sectionsUUID, arrayUUID);
    ASSERT_EQ(sectionsUUID, stringUUID);
    ASSERT_EQ(arrayUUID, stringUUID);
}

TEST(UUID, ToString1_ConstructorFromString_StringsEqual)
{
    using Neuro::Ble::UUID;
    const auto uuidString = "AAAAAAAA-BBBB-CCCC-DDDD-EEEEEEEEEEEE";
    auto uuid = UUID(uuidString);

    ASSERT_EQ(uuidString, to_string(uuid));
}

TEST(UUID, ToString2_ConstructorFromString_StringsEqual)
{
    using Neuro::Ble::UUID;
    const auto uuidString = "AE025DC0-820B-7DAF-364C-E60FD052F3CB";
    auto uuid = UUID(uuidString);

    ASSERT_EQ(uuidString, to_string(uuid));
}

TEST(UUID, LeftShiftOperator_ConstructorFromStringStringStream_StringsEqual)
{
    using Neuro::Ble::UUID;
    const auto uuidString = "AAAAAAAA-BBBB-CCCC-DDDD-EEEEEEEEEEEE";
    auto uuid = UUID(uuidString);
    std::stringstream testStream;

    testStream << uuid;

    ASSERT_EQ(uuidString, testStream.str());
}
