#include "ble_enumerator.h"
#include <iostream>
#include <iomanip>

using Neuro::Utilities::EventListener;
using Neuro::Ble::IBleEnumerator;
using Neuro::Ble::IBleDevice;
using Neuro::Ble::IBleService;
using Neuro::Ble::DeviceIdentifier;
using Neuro::Ble::UUID;



int main()
{
    std::cout << "Start scanning\n";
    std::unique_ptr<IBleEnumerator> Enumerator = Neuro::Ble::create_ble_enumerator();
    std::unique_ptr<IBleDevice> Device;
    std::future<std::vector<std::shared_ptr<IBleService>>> Services;
    std::mutex m;
    std::condition_variable cv;
    std::atomic_bool find_one = false;
    {
        EventListener<> AdvertisementListener
        {
            [&]()
            {
                std::unique_lock<std::mutex> lock{m};
                for (const auto &advertisement : Enumerator->advertisingDevices())
                {
                    std::cout << "=========================" << std::endl;
                    std::cout << advertisement.Name << " [" << advertisement.Identifier << "]" << std::endl;
                    std::cout << "RSSI: " << advertisement.RSSI << std::endl;
                    for (const auto &serviceUuid : advertisement.ServicesUUIDs)
                    {
                        std::cout << " -" << serviceUuid << std::endl;
                    }
                    for (const auto& manuData : advertisement.ManufacturerData)
                    {
                        std::cout << "Manufacturer data [" << manuData.first << "]:" << std::endl;
                        std::ios_base::fmtflags previousFormat(std::cout.flags());
                        std::cout << std::hex << std::uppercase;
                        for (const auto &data : manuData.second)
                        {
                            std::cout << std::setfill('0') << std::setw(2) << static_cast<unsigned short>(data);
                        }
                        std::cout.flags(previousFormat);
                        std::cout << std::endl;
                    }
                    if(!find_one && advertisement.Name == "BrainBit" ) {
                      std::cout << "created one\n";
                        Device = Enumerator->createBleDevice(advertisement.Identifier);
                        find_one = true;
                        cv.notify_one();
                    }
                }
            }
        };
        Enumerator->advertisementListChanged().registerListener(AdvertisementListener);
        std::unique_lock<std::mutex> lock{m};
        cv.wait(lock);
    }
    
	std::string value;
  std::atomic_bool isGetIt = false;
	do 
	{
        // "6E400001-B534-f393-68a9-e50e24dcca9e"
        // "6E400002-B534-f393-68a9-e50e24dcca9e"
        // "6E400003-B534-f393-68a9-e50e24dcca9e"
        try {
        if(!isGetIt) {
            std::cout << "getting services\n";
            auto serviceS = Device->getServicesByUuid({UUID("6E400001-B534-f393-68a9-e50e24dcca9e")}).get();
            
            std::cout << "serviceS: " << serviceS.size() << "\n";
            isGetIt = true;
        }
        } catch (...) {
            std::cerr << "error\n";
            return -1;
        }
		std::getline(std::cin, value);
	} while (value != "exit" && value != "q");
	return 0;
}
