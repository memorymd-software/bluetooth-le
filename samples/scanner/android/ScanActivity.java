package com.memorymd.blescanapp;

import android.app.Activity;
import android.os.Bundle;
import android.content.Context;
import android.widget.EditText;
import android.util.Log;

import java.util.List;

public class ScanActivity extends Activity
{
	@Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_info);
    }

	@Override
    protected void onStart() {
        super.onStart();
		try
		{				
			System.loadLibrary("android_scan_sample");
			startScan(this);
		}
		catch(Exception e)
		{
			Log.e("BleScanner", String.format("Unable to start scan: %s", e.getMessage()));
		}
    }

	@Override
    protected void onStop() {
        super.onStart();
		try
		{			
			stopScan();
		}
		catch(Exception e)
		{
			Log.e("BleScanner", String.format("Unable to stop scan: %s", e.getMessage()));
		}
    }

	private void deviceListChanged(final String deviceInfoString)
	{
		runOnUiThread(new Runnable()
        {
			@Override
            public void run()
            {				
				EditText deviceListText = findViewById(R.id.listText);
				deviceListText.getText().clear();
				deviceListText.append(deviceInfoString);
            }
        });
	}

	private native void startScan(Context context);
	private native void stopScan();
}