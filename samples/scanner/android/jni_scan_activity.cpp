#include "android/log.h"
#include "jni_global_ref.h"
#include "jni_string.h"
#include "ble_enumerator.h"
#include <sstream>
#include <iomanip>

using Neuro::Ble::IBleEnumerator;
using Neuro::Ble::DeviceIdentifier;
using Neuro::Ble::ScanFilter;
using Neuro::Ble::UUID;
using Neuro::Ble::AdvertisementData;
using Neuro::Utilities::EventListener;
using namespace Neuro::Ble::Impl;

std::unique_ptr<IBleEnumerator> Enumerator;
std::unique_ptr<EventListener<>> Listener;

extern "C"
{
	JNIEXPORT void JNICALL Java_com_memorymd_blescanapp_ScanActivity_startScan
	(
		JNIEnv* env,
		jobject activity,
		jobject context
	)
	{
		try
		{
			Enumerator = Neuro::Ble::create_ble_enumerator(env, context,
			{
				ScanFilter
				{
					std::optional<DeviceIdentifier>{},
					std::optional<std::string>{},
					std::optional<UUID>{UUID{"3D2F0001-D6B9-11E4-88CF-0002A5D5C51B"}}
				},
				ScanFilter
				{
					std::optional<DeviceIdentifier>{},
					std::optional<std::string>{},
					std::optional<UUID>{UUID{"67CF0001-FA71-11E5-80B7-0002A5D5C51B"}}
				},
				ScanFilter
				{
					std::optional<DeviceIdentifier>{},
					std::optional<std::string>{},
					std::optional<UUID>{UUID{"77FF0001-FA66-11E5-B501-0002A5D5C51B"}}
				},
				ScanFilter
				{
					std::optional<DeviceIdentifier>{},
					std::optional<std::string>{},
					std::optional<UUID>{UUID{"B9390001-FA71-11E5-A787-0002A5D5C51B"}}
				},
				ScanFilter
				{
					std::optional<DeviceIdentifier>{},
					std::optional<std::string>{},
					std::optional<UUID>{UUID{"6E400001-B534-F393-68A9-E50E24DCCA9E"}}
				},
				ScanFilter
				{
					std::optional<DeviceIdentifier>{},
					std::optional<std::string>{},
					std::optional<UUID>{UUID{"6E400000-B534-F393-68A9-E50E24DCCA9E"}}
				}
			});
			auto jvm = Jni::get_java_vm(env);
			Jni::GlobalRef<Jni::JavaObject> activityObj(env, Jni::JavaObject(activity));
			Listener = std::make_unique<EventListener<>>([activityObj, jvm]()
				{
					Jni::call_in_attached_env(jvm, [activityObj](auto environment)
						{
							if (!Enumerator)
								return;

							std::stringstream outStream;
							auto advertisements = Enumerator->advertisingDevices();
							for (const AdvertisementData &data : advertisements)
							{
								outStream << data.Name << " [" << data.Identifier << "]:"<<std::endl;
								outStream << "RSSI: " << data.RSSI << std::endl;
								outStream << "Services:" << std::endl;
								for (const auto& serviceUuid : data.ServicesUUIDs)
								{
									outStream << " -" << serviceUuid << std::endl;
								}
								for (const auto& manuData : data.ManufacturerData)
								{
									outStream << "Manufacturer data [" << manuData.first << "]:" << std::endl;
									std::ios_base::fmtflags previousFormat(outStream.flags());
									outStream << std::hex << std::uppercase;
									for (const auto& data : manuData.second)
									{
										outStream << std::setfill('0') << std::setw(2) << static_cast<unsigned short>(data);
									}
									outStream.flags(previousFormat);
									outStream << std::endl;
								}
								outStream << "--------------" << std::endl;
							}
							__android_log_print(ANDROID_LOG_INFO, "BleScanner", "Advertisement received: %s", outStream.str().c_str());
							Jni::JavaString advertiseString(environment, outStream.str().c_str());
							activityObj->callMethod<void>(environment, "deviceListChanged", "(Ljava/lang/String;)V", static_cast<jstring>(advertiseString));
						});
				});
			Enumerator->advertisementListChanged().registerListener(*Listener);
		}
		catch (std::exception & e)
		{
			__android_log_print(ANDROID_LOG_ERROR, "BleScanner", "StartScan failed: %s", e.what());
		}
		catch (...)
		{
			__android_log_print(ANDROID_LOG_ERROR, "BleScanner", "StartScan failed");
		}
	}

	JNIEXPORT void JNICALL Java_com_memorymd_blescanapp_ScanActivity_stopScan
	(
		JNIEnv* env,
		jobject activity
	)
	{
		try
		{
			Listener.reset();
			Enumerator.reset();
		}
		catch (std::exception & e)
		{
			__android_log_print(ANDROID_LOG_ERROR, "BleScanner", "StopScan failed: %s", e.what());
		}
		catch (...)
		{
			__android_log_print(ANDROID_LOG_ERROR, "BleScanner", "StopScan failed");
		}
	}
}