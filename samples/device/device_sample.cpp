#include "ble_enumerator.h"
#include "ble_device_address.h"
#include <iostream>
#include <iomanip>

using Neuro::Utilities::EventListener;
using Neuro::Ble::IBleEnumerator;
using Neuro::Ble::DeviceIdentifier;
using Neuro::Ble::BleDeviceAddress;
using Neuro::Ble::UUID;

void show_device_info(const std::string &address_string)
{
	try 
	{
        const auto deviceId = DeviceIdentifier { BleDeviceAddress { address_string } };
		auto enumerator = Neuro::Ble::create_ble_enumerator();
		const auto device = enumerator->createBleDevice(deviceId);
		auto services = device->getServicesByUuid({ UUID{ "6E400001-B534-F393-68A9-E50E24DCCA9E" } }).get();
		auto statusChars = services[0]->getCharacteristicsByUuid({ UUID{ "6E400002-B534-F393-68A9-E50E24DCCA9E" } }).get();
		auto charge = static_cast<unsigned int>(statusChars[0]->value()[2]) & 0b01111111;
		std::cout << "Device " << device->name() << " [" << device->identifier() << "]" << "Battery: " << charge << "%" << std::endl;
	}
	catch (std::exception &e)
	{
		std::cout << "Cannot show info for device [" << address_string << "]: " << e.what() << std::endl;
	}
	catch (...)
	{
		std::cout << "Cannot show info for device [" << address_string << "]" << std::endl;
	}
}

int main()
{
	std::string address;
	std::cout << "Enter device address" << std::endl;
	std::getline(std::cin, address);
	while (address != "exit" && address != "q")
	{		
		show_device_info(address);
		std::cout << "Enter device address" << std::endl;
		std::getline(std::cin, address);
	}
	return 0;
}
