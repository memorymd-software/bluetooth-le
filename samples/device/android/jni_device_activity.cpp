#include "android/log.h"
#include "jni_global_ref.h"
#include "jni_string.h"
#include "ble_enumerator.h"
#include <sstream>
#include <iomanip>

using Neuro::Ble::IBleEnumerator;
using Neuro::Ble::IBleDevice;
using Neuro::Ble::DeviceIdentifier;
using Neuro::Ble::UUID;
using Neuro::Ble::BleDeviceAddress;
using Neuro::Ble::AdvertisementData;
using Neuro::Utilities::EventListener;
using namespace Neuro::Ble::Impl;

std::unique_ptr<IBleDevice> Device;
std::unique_ptr<EventListener<const std::vector<std::byte>&>> StatusCharChanged;

static void print_device_info(JNIEnv *env, jobject activity, const IBleDevice &device, const std::vector<std::byte> &status_bytes)
{
	std::stringstream outStream;
	outStream << device.name() << std::endl;
	outStream << device.identifier() << std::endl;
	auto charge = static_cast<unsigned int>(status_bytes[2]) & 0b01111111;
	outStream << "Battery: " << charge << "%" << std::endl;
	Jni::GlobalRef<Jni::JavaObject> activityObj(env, Jni::JavaObject(activity));
	__android_log_print(ANDROID_LOG_INFO, "BleDevice", "Device info: %s", outStream.str().c_str());
	Jni::JavaString infoString(env, outStream.str().c_str());
	activityObj->callMethod<void>(env, "printInfo", "(Ljava/lang/String;)V", static_cast<jstring>(infoString));
}

extern "C"
{
	JNIEXPORT void JNICALL Java_com_memorymd_bledeviceapp_DeviceActivity_connectDevice
	(
		JNIEnv* env,
		jobject activity,
		jobject context
	)
	{
		try
		{
			auto enumerator = Neuro::Ble::create_ble_enumerator(env, context,
				{
					Neuro::Ble::ScanFilter
					{
						std::optional<DeviceIdentifier>{},
						std::optional<std::string>{"Brainbit"},
						std::optional<UUID>{}
					}
				});

			Device = enumerator->createBleDevice(DeviceIdentifier{ BleDeviceAddress{"FA:47:81:F3:6E:55"} });
			auto service = std::move(Device->getServicesByUuid({ UUID{ "6E400001-B534-F393-68A9-E50E24DCCA9E" } }).get().front());
			auto statusChar = std::move(service->getCharacteristicsByUuid({ UUID{ "6E400002-B534-F393-68A9-E50E24DCCA9E" } }).get().front());
			print_device_info(env, activity, *Device, statusChar->value());
			StatusCharChanged = std::make_unique<EventListener<const std::vector<std::byte>&>>( [env, activity](const auto& value_bytes)
			{
				print_device_info(env, activity, *Device, value_bytes);
			} );
			statusChar->valueChanged().registerListener(*StatusCharChanged);
		}
		catch (std::exception & e)
		{
			__android_log_print(ANDROID_LOG_ERROR, "BleDevice", "Connect failed: %s", e.what());
		}
		catch (...)
		{
			__android_log_print(ANDROID_LOG_ERROR, "BleDevice", "Connect failed");
		}
	}

	JNIEXPORT void JNICALL Java_com_memorymd_bledeviceapp_DeviceActivity_disconnect
	(
		JNIEnv* env,
		jobject activity
	)
	{
		try
		{
			Device.reset();
			StatusCharChanged.reset();
		}
		catch (std::exception & e)
		{
			__android_log_print(ANDROID_LOG_ERROR, "BleDevice", "Disconnect failed: %s", e.what());
		}
		catch (...)
		{
			__android_log_print(ANDROID_LOG_ERROR, "BleDevice", "Disconnect failed");
		}
	}
}