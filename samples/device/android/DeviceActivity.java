package com.memorymd.bledeviceapp;

import android.app.Activity;
import android.os.Bundle;
import android.content.Context;
import android.widget.EditText;
import android.util.Log;

import java.util.List;

public class DeviceActivity extends Activity
{
	@Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_info);
    }

	@Override
    protected void onStart() {
        super.onStart();
		try
		{				
			System.loadLibrary("android_device_sample");
			connectDevice(this);
		}
		catch(Exception e)
		{
			String errorString = String.format("Unable to connect: %s", e.getMessage());
			Log.e("BleDevice", errorString);
			printInfo(errorString);
		}
    }

	@Override
    protected void onStop() {
        super.onStart();
		try
		{			
			disconnect();
		}
		catch(Exception e)
		{
			Log.e("BleDevice", String.format("Unable to stop scan: %s", e.getMessage()));
		}
    }

	private void printInfo(final String infoString)
	{
		runOnUiThread(new Runnable()
        {
			@Override
            public void run()
            {				
				EditText deviceListText = findViewById(R.id.listText);
				deviceListText.getText().clear();
				deviceListText.append(infoString);
            }
        });
	}

	private native void connectDevice(Context context);
	private native void disconnect();
}