#include "event_notifier.h"
#include <iostream>
#include <string>
#include <mutex>

using Neuro::Utilities::EventNotifier;
using Neuro::Utilities::EventListener;

class SampleClass final
{
	int mValue{0};
	EventNotifier<const std::string&, int> ValueChanged;
public:

	EventNotifier<const std::string&, int>& valueChanged()
	{
		return ValueChanged;
	}

	void setValue(int value)
	{
		mValue = value;
		ValueChanged.notify("Block", mValue);
	}

	void setValueAsync(int value)
	{
		mValue = value;
		ValueChanged.notifyAsync("Async", mValue);
	}
};

int main()
{
	SampleClass sampleObj;
	std::mutex outputMutex;

	const EventListener<const std::string &, int> listener
	{
		[&outputMutex](const std::string &message, int value)
		{
			std::unique_lock outputLock(outputMutex);
			std::cout << "Message: " << message;
			std::cout << " Value: " << value << std::endl;
			std::cout << "-------------------" << std::endl;
		}
	};
	sampleObj.valueChanged().registerListener(listener);

	int inputValue;
	while (std::cin >> inputValue)
	{
		for (auto i = 0; i < 5; ++i) 
		{
			sampleObj.setValueAsync(inputValue);
			sampleObj.setValue(inputValue);
		}
	}
}
